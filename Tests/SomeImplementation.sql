/****************************************************
  Given the db implementation in the following script (3 tables),
    what does the Mystery stored proc at the end do?
****************************************************/

IF OBJECT_ID('dbo.Review', 'U') IS NOT NULL DROP TABLE dbo.Review;
IF OBJECT_ID('dbo.Person', 'U') IS NOT NULL DROP TABLE dbo.Person;
IF OBJECT_ID('dbo.Restaurant', 'U') IS NOT NULL DROP TABLE dbo.Restaurant;
GO

CREATE TABLE dbo.Person(
  PersonName varchar(32) NOT NULL,
  CONSTRAINT pk_Person 
	PRIMARY KEY CLUSTERED (PersonName),
  CONSTRAINT ck_Person_PersonName
	CHECK (RTRIM(PersonName) != '')
);
GO

CREATE TABLE dbo.Restaurant(
  RestaurantName varchar(32) NOT NULL,
  CONSTRAINT pk_Restaurant
	PRIMARY KEY CLUSTERED (RestaurantName),
  CONSTRAINT ck_Restaurant_RestaurantName
	CHECK (RTRIM(RestaurantName) != ''),
  CuisineType varchar(16) NOT NULL,
  CONSTRAINT ck_Restaurant_CuisineType
	CHECK (CuisineType IN (
				'Mexican',
				'Pacific Rim',
				'Thai',
				'American'
			) )
);
GO

CREATE TABLE dbo.Review(
  PersonName varchar(32) NOT NULL,
  RestaurantName varchar(32) NOT NULL,
  CONSTRAINT pk_Review
	PRIMARY KEY CLUSTERED(PersonName, RestaurantName),
  Rating float NOT NULL,
  CONSTRAINT ck_Review_Rating
	CHECK (Rating BETWEEN 0 AND 5)
);
GO

ALTER TABLE dbo.Review ADD
  CONSTRAINT fk_Review_to_Person
	FOREIGN KEY (PersonName)
	REFERENCES dbo.Person(PersonName)
, CONSTRAINT fk_Review_to_Restaurant
	FOREIGN KEY (RestaurantName)
	REFERENCES dbo.Restaurant(RestaurantName)
;
GO

INSERT INTO dbo.Person(PersonName)
SELECT 'Mary' UNION ALL
SELECT 'Joe' UNION ALL
SELECT 'Betty' UNION ALL
SELECT 'Fred' UNION ALL
SELECT 'Alice' UNION ALL
SELECT 'Bob';
GO

INSERT INTO dbo.Restaurant(RestaurantName, CuisineType)
SELECT 'Cholo''s', 'Mexican' UNION ALL
SELECT 'Arceo''s', 'Mexican' UNION ALL
SELECT 'The Dolphin', 'Pacific Rim' UNION ALL
SELECT 'Turtle Bay Cafe', 'Pacific Rim' UNION ALL
SELECT 'Satyama', 'Pacific Rim' UNION ALL
SELECT 'Bubba''s', 'American' UNION ALL
SELECT 'McDonald''s', 'American' UNION ALL
SELECT 'Honolulu Thai', 'Thai';
GO

INSERT INTO dbo.Review(PersonName, RestaurantName, Rating)
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Mary' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 4 [Rating] UNION ALL
	SELECT 'Arceo''s', 4 UNION ALL
	SELECT 'The Dolphin', 5 UNION ALL
	SELECT 'Turtle Bay Cafe', 3 UNION ALL
	SELECT 'Satyama', 5 UNION ALL
	SELECT 'Bubba''s', 3 UNION ALL
	SELECT 'McDonald''s', 3 UNION ALL
	SELECT 'Honolulu Thai', 4
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Joe' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 1 [Rating] UNION ALL
	SELECT 'Arceo''s', 1 UNION ALL
	SELECT 'The Dolphin', 2 UNION ALL
	SELECT 'Turtle Bay Cafe', 0 UNION ALL
	SELECT 'Satyama', 2 UNION ALL
	SELECT 'Bubba''s', 0 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 1
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Betty' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 5 [Rating] UNION ALL
	SELECT 'The Dolphin', 4 UNION ALL
	SELECT 'Satyama', 5 UNION ALL
	SELECT 'Bubba''s', 4 UNION ALL
	SELECT 'Honolulu Thai', 5
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Fred' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Arceo''s' [RestaurantName], 1 [Rating] UNION ALL
	SELECT 'The Dolphin', 2 UNION ALL
	SELECT 'Satyama', 1 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 2
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Alice' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 5 [Rating] UNION ALL
	SELECT 'The Dolphin', 3 UNION ALL
	SELECT 'Turtle Bay Cafe', 4 UNION ALL
	SELECT 'Satyama', 3 UNION ALL
	SELECT 'Bubba''s', 5
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Bob' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 4 [Rating] UNION ALL
	SELECT 'Turtle Bay Cafe', 3 UNION ALL
	SELECT 'Bubba''s', 1 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 3
) dY
;
GO

IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'MysteryProc' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.MysteryProc AS SELECT 1');
END;
GO

/**************************************************************************************************
  What does this proc do, and how does it do it?
**************************************************************************************************/
ALTER PROC dbo.MysteryProc
  @PersonName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Do something interesting...
SELECT TOP 1 PersonName, SomeValue, SomeOtherValue
FROM
(
	SELECT 
		r2.PersonName,

		[SomeValue] =
			(
				SUM(r1.Rating * r2.Rating) - SUM(r1.Rating) * SUM(r2.Rating) / CAST(COUNT(*) AS float)) 
				/ 
				(COUNT(*) * STDEVP(r1.Rating) * STDEVP(r2.Rating) 
			), 

		[SomeOtherValue] = COUNT(*) 

	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
) dX
ORDER BY ABS(dX.SomeValue) DESC;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.MysteryProc @PersonName = 'Joe';
GO
