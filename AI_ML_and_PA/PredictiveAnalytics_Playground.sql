/****************************************************
  This T-SQL script by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

IF OBJECT_ID('dbo.Review', 'U') IS NOT NULL DROP TABLE dbo.Review;
IF OBJECT_ID('dbo.Person', 'U') IS NOT NULL DROP TABLE dbo.Person;
IF OBJECT_ID('dbo.Restaurant', 'U') IS NOT NULL DROP TABLE dbo.Restaurant;
GO

CREATE TABLE dbo.Person(
  PersonName varchar(32) NOT NULL,
  CONSTRAINT pk_Person 
	PRIMARY KEY CLUSTERED (PersonName),
  CONSTRAINT ck_Person_PersonName
	CHECK (RTRIM(PersonName) != '')
);
GO

CREATE TABLE dbo.Restaurant(
  RestaurantName varchar(32) NOT NULL,
  CONSTRAINT pk_Restaurant
	PRIMARY KEY CLUSTERED (RestaurantName),
  CONSTRAINT ck_Restaurant_RestaurantName
	CHECK (RTRIM(RestaurantName) != ''),
  CuisineType varchar(16) NOT NULL,
  CONSTRAINT ck_Restaurant_CuisineType
	CHECK (CuisineType IN (
				'Mexican',
				'Pacific Rim',
				'Thai',
				'American'
			) )
);
GO

CREATE TABLE dbo.Review(
  PersonName varchar(32) NOT NULL,
  RestaurantName varchar(32) NOT NULL,
  CONSTRAINT pk_Review
	PRIMARY KEY CLUSTERED(PersonName, RestaurantName),
  Rating float NOT NULL,
  CONSTRAINT ck_Review_Rating
	CHECK (Rating BETWEEN 0 AND 5)
);
GO

ALTER TABLE dbo.Review ADD
  CONSTRAINT fk_Review_to_Person
	FOREIGN KEY (PersonName)
	REFERENCES dbo.Person(PersonName)
, CONSTRAINT fk_Review_to_Restaurant
	FOREIGN KEY (RestaurantName)
	REFERENCES dbo.Restaurant(RestaurantName)
;
GO

INSERT INTO dbo.Person(PersonName)
SELECT 'Mary' UNION ALL
SELECT 'Joe' UNION ALL
SELECT 'Betty' UNION ALL
SELECT 'Fred' UNION ALL
SELECT 'Alice' UNION ALL
SELECT 'Bob';
GO

INSERT INTO dbo.Restaurant(RestaurantName, CuisineType)
SELECT 'Cholo''s', 'Mexican' UNION ALL
SELECT 'Arceo''s', 'Mexican' UNION ALL
SELECT 'The Dolphin', 'Pacific Rim' UNION ALL
SELECT 'Turtle Bay Cafe', 'Pacific Rim' UNION ALL
SELECT 'Satyama', 'Pacific Rim' UNION ALL
SELECT 'Bubba''s', 'American' UNION ALL
SELECT 'McDonald''s', 'American' UNION ALL
SELECT 'Honolulu Thai', 'Thai';
GO

INSERT INTO dbo.Review(PersonName, RestaurantName, Rating)
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Mary' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 4 [Rating] UNION ALL
	SELECT 'Arceo''s', 4 UNION ALL
	SELECT 'The Dolphin', 5 UNION ALL
	SELECT 'Turtle Bay Cafe', 3 UNION ALL
	SELECT 'Satyama', 5 UNION ALL
	SELECT 'Bubba''s', 3 UNION ALL
	SELECT 'McDonald''s', 3 UNION ALL
	SELECT 'Honolulu Thai', 4
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Joe' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 1 [Rating] UNION ALL
	SELECT 'Arceo''s', 1 UNION ALL
	SELECT 'The Dolphin', 2 UNION ALL
	SELECT 'Turtle Bay Cafe', 0 UNION ALL
	SELECT 'Satyama', 2 UNION ALL
	SELECT 'Bubba''s', 0 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 1
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Betty' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 5 [Rating] UNION ALL
	SELECT 'The Dolphin', 4 UNION ALL
	SELECT 'Satyama', 5 UNION ALL
	SELECT 'Bubba''s', 4 UNION ALL
	SELECT 'Honolulu Thai', 5
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Fred' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Arceo''s' [RestaurantName], 1 [Rating] UNION ALL
	SELECT 'The Dolphin', 2 UNION ALL
	SELECT 'Satyama', 1 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 2
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Alice' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 5 [Rating] UNION ALL
	SELECT 'The Dolphin', 3 UNION ALL
	SELECT 'Turtle Bay Cafe', 4 UNION ALL
	SELECT 'Satyama', 3 UNION ALL
	SELECT 'Bubba''s', 5
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Bob' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 4 [Rating] UNION ALL
	SELECT 'Turtle Bay Cafe', 3 UNION ALL
	SELECT 'Bubba''s', 1 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 3
) dY
;
GO


IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'MostSimilarReviewer_Euclidean' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.MostSimilarReviewer_Euclidean AS SELECT 1');
END;
GO

/**************************************************************************************************
  Use Euclidean distance to find the single best-matching reviewer
**************************************************************************************************/
ALTER PROC dbo.MostSimilarReviewer_Euclidean
  @PersonName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Find closest reviewer
DECLARE @Scale float = 5.0 - 0.0;

SELECT TOP 1 PersonName, InvertedEuclid, ElementMatches
FROM
(
	SELECT r2.PersonName
	, (SQRT(COUNT(*)) * @Scale /* MaxPossEucl */ - SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating)))) /* RawEucl */
	  / (SQRT(COUNT(*)) * @Scale) [InvertedEuclid] -- Transforms to linear scaling, higher is better match
	, COUNT(*) [ElementMatches]
	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
) dX
-- Could filter on minimum number of ElementMatches
--  or minimum similarity (or both) here
ORDER BY dX.InvertedEuclid DESC;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.MostSimilarReviewer_Euclidean @PersonName = 'Joe';
GO

IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'NextRestaurant_Euclidean' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.NextRestaurant_Euclidean AS SELECT 1');
END;
GO

/**************************************************************************************************
  Use Euclidean distance to find the single best-matching reviewer,
    and then return the highest-rated restaurant that the user has not yet visited
**************************************************************************************************/
ALTER PROC dbo.NextRestaurant_Euclidean
  @PersonName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Perform operation in steps to more easily see the logic

-- Find set of "eligible" reviewers: those that 
-- have reviewed a restaurant that the user hasn't yet
DECLARE @PossibleReviewers TABLE(PersonName varchar(32) NOT NULL PRIMARY KEY);
INSERT INTO @PossibleReviewers(PersonName)
SELECT DISTINCT r.PersonName
FROM dbo.Review r
WHERE NOT EXISTS(
	SELECT 1
	FROM dbo.Review
	WHERE PersonName = @PersonName
	  AND RestaurantName = r.RestaurantName
);

-- Find closest reviewer
DECLARE @BestMatch varchar(32)
	-- Next 2 variables are handy for sanity 
	-- checking, further refinement
	,	@Similarity float
	,	@ElementMatches int;

DECLARE @Scale float = 5.0 - 0.0;

SELECT TOP 1 @BestMatch = PersonName
			, @Similarity = InvertedEuclid
			, @ElementMatches = ElementMatches
FROM
(
	SELECT r2.PersonName
	, (SQRT(COUNT(*)) * @Scale /* MaxPossEucl */ - SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating)))) /* RawEucl */
	  / (SQRT(COUNT(*)) * @Scale) [InvertedEuclid] -- Transforms to linear scaling, higher is better match
	, COUNT(*) [ElementMatches]
	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	INNER JOIN @PossibleReviewers pr
		ON r2.PersonName = pr.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
) dX
ORDER BY dX.InvertedEuclid DESC;

-- Find highest-rated restaurant that the user hasn't yet visited
SELECT TOP 1 rRecommended.RestaurantName, rRecommended.Rating
	, @BestMatch [RecommendedBy], @Similarity [TasteSimilarity]
	, @ElementMatches [NbrCommonRestaurants]
FROM dbo.Review rRecommended
LEFT JOIN dbo.Review rUser
	ON rUser.PersonName = @PersonName
   AND rRecommended.RestaurantName = rUser.RestaurantName
WHERE rRecommended.PersonName = @BestMatch
  AND rUser.RestaurantName IS NULL
ORDER BY rRecommended.Rating DESC;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.NextRestaurant_Euclidean @PersonName = 'Joe';
GO

IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'HowGoodIsRestaurant_Euclidean' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.HowGoodIsRestaurant_Euclidean AS SELECT 1');
END;
GO

/**************************************************************************************************
  Use Euclidean distance to weight the opinion of all reviewers that have
	reviewed the restaurant of interest, and then use those weightings
    in a calculation predicting this user's hypothetical rating for the same restaurant
**************************************************************************************************/
ALTER PROC dbo.HowGoodIsRestaurant_Euclidean
  @PersonName varchar(32),
  @RestaurantName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Perform operation in steps to more easily see the logic

-- Find set of "eligible" reviewers: those 
-- that have reviewed the restaurant of interest
DECLARE @PossibleReviewers TABLE(PersonName varchar(32) NOT NULL PRIMARY KEY);
INSERT INTO @PossibleReviewers(PersonName)
SELECT r.PersonName
FROM dbo.Review r
WHERE r.RestaurantName = @RestaurantName;

-- Use the similarity to weight the value of the reviewer's opinion
DECLARE @Scale float = 5.0 - 0.0;

-- CTE calculates the euclidean distance and
-- the rating for the restaurant of interest
WITH PreCalc
AS
(
	SELECT r2.PersonName
	, (SQRT(COUNT(*)) * @Scale /* MaxPossEucl */ - SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating)))) /* RawEucl */
	  / (SQRT(COUNT(*)) * @Scale) [InvertedEuclid] -- Transforms to linear scaling, higher is better match
	, COUNT(*) [ElementMatches]
	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	INNER JOIN @PossibleReviewers pr
		ON r2.PersonName = pr.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
)
SELECT 
	--PreCalc.*, r.Rating
	SUM(PreCalc.InvertedEuclid * r.Rating) /* Total of weighted ratings */
	/
	SUM(PreCalc.InvertedEuclid) /* Total of weights */
FROM PreCalc
INNER JOIN dbo.Review r
	ON PreCalc.PersonName = r.PersonName
WHERE r.RestaurantName = @RestaurantName;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.HowGoodIsRestaurant_Euclidean @PersonName = 'Joe', @RestaurantName = 'McDonald''s';
GO

IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'MostIndicativeReviewer_Pearson' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.MostIndicativeReviewer_Pearson AS SELECT 1');
END;
GO

/**************************************************************************************************
  Use Pearson's r to find the single reviewer that
    most strongly predicts user's tastes (note that
    this may be via correlation in the negative
    direction)
**************************************************************************************************/
ALTER PROC dbo.MostIndicativeReviewer_Pearson
  @PersonName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Find closest reviewer
SELECT TOP 1 PersonName, Pearson, ElementMatches
FROM
(
	SELECT r2.PersonName
	, (
		SUM(r1.Rating * r2.Rating) - SUM(r1.Rating) * SUM(r2.Rating) / CAST(COUNT(*) AS float)) 
		/ 
		(COUNT(*) * STDEVP(r1.Rating) * STDEVP(r2.Rating) 
	) [Pearson]

	, COUNT(*) [ElementMatches]

	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
) dX
-- Could filter on minimum number of ElementMatches
--  or minimum similarity (or both) here
ORDER BY ABS(dX.Pearson) DESC;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.MostIndicativeReviewer_Pearson @PersonName = 'Joe';
GO
