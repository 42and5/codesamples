/****************************************************
  This T-SQL script by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'HowGoodIsRestaurant_Euclidean' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.HowGoodIsRestaurant_Euclidean AS SELECT 1');
END;
GO

/**************************************************************************************************
  Use Euclidean distance to weight the opinion of all reviewers that have
	reviewed the restaurant of interest, and then use those weightings
    in a calculation predicting this user's hypothetical rating for the same restaurant
**************************************************************************************************/
ALTER PROC dbo.HowGoodIsRestaurant_Euclidean
  @PersonName varchar(32),
  @RestaurantName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Perform operation in steps to more easily see the logic

-- Find set of "eligible" reviewers: those 
-- that have reviewed the restaurant of interest
DECLARE @PossibleReviewers TABLE(PersonName varchar(32) NOT NULL PRIMARY KEY);
INSERT INTO @PossibleReviewers(PersonName)
SELECT r.PersonName
FROM dbo.Review r
WHERE r.RestaurantName = @RestaurantName;

-- Use the similarity to weight the value of the reviewer's opinion
DECLARE @Scale float = 5.0 - 0.0;

-- CTE calculates the euclidean distance and
-- the rating for the restaurant of interest
WITH PreCalc
AS
(
	SELECT r2.PersonName
	, (SQRT(COUNT(*)) * @Scale /* MaxPossEucl */ - SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating)))) /* RawEucl */
	  / (SQRT(COUNT(*)) * @Scale) [InvertedEuclid] -- Transforms to linear scaling, higher is better match
	, COUNT(*) [ElementMatches]
	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	INNER JOIN @PossibleReviewers pr
		ON r2.PersonName = pr.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
)
SELECT 
	--PreCalc.*, r.Rating
	SUM(PreCalc.InvertedEuclid * r.Rating) /* Total of weighted ratings */
	/
	SUM(PreCalc.InvertedEuclid) /* Total of weights */
FROM PreCalc
INNER JOIN dbo.Review r
	ON PreCalc.PersonName = r.PersonName
WHERE r.RestaurantName = @RestaurantName;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.HowGoodIsRestaurant_Euclidean @PersonName = 'Joe', @RestaurantName = 'McDonald''s';
GO
