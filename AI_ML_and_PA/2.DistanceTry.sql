/****************************************************
  This T-SQL script by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

-- Euclidean Distance, Initial cut
SELECT r1.PersonName, r2.PersonName
, SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating))) [RawEuclid]
, COUNT(*) [ElementMatches]
FROM dbo.Review r1
INNER JOIN dbo.Review r2
	ON r1.RestaurantName = r2.RestaurantName
   --AND r1.PersonName != r2.PersonName
WHERE r1.PersonName = 'Joe'
GROUP BY r1.PersonName, r2.PersonName;
GO

-- Euclidean distance, scaled linearly [0.0, 1.0]
DECLARE @Scale float = 5.0 - 0.0;

SELECT r1.PersonName, r2.PersonName
, (SQRT(COUNT(*)) * @Scale /* MaxPossEucl */ - SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating)))) /* CalculatedEucl */
  / (SQRT(COUNT(*)) * @Scale) [InvertedEuclid] -- Transforms to linear scaling, higher is better match
, COUNT(*) [ElementMatches]
FROM dbo.Review r1
INNER JOIN dbo.Review r2
	ON r1.RestaurantName = r2.RestaurantName
   --AND r1.PersonName != r2.PersonName
WHERE r1.PersonName = 'Joe'
GROUP BY r1.PersonName, r2.PersonName;
GO

-- Euclidean distance, sorted from "most similar" to "least similar"
DECLARE @Scale float = 5.0 - 0.0;

SELECT r1.PersonName, r2.PersonName
, (SQRT(COUNT(*)) * @Scale /* MaxPossEucl */ - SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating)))) /* CalculatedEucl */
  / (SQRT(COUNT(*)) * @Scale) [InvertedEuclid] -- Transforms to linear scaling, higher is better match
, COUNT(*) [ElementMatches]
FROM dbo.Review r1
INNER JOIN dbo.Review r2
	ON r1.RestaurantName = r2.RestaurantName
   --AND r1.PersonName != r2.PersonName
WHERE r1.PersonName = 'Joe'
GROUP BY r1.PersonName, r2.PersonName
ORDER BY 3 /* InvertedEuclid */ DESC;
GO
