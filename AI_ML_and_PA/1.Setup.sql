/****************************************************
  This T-SQL script by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

IF OBJECT_ID('dbo.Review', 'U') IS NOT NULL DROP TABLE dbo.Review;
IF OBJECT_ID('dbo.Person', 'U') IS NOT NULL DROP TABLE dbo.Person;
IF OBJECT_ID('dbo.Restaurant', 'U') IS NOT NULL DROP TABLE dbo.Restaurant;
GO

CREATE TABLE dbo.Person(
  PersonName varchar(32) NOT NULL,
  CONSTRAINT pk_Person 
	PRIMARY KEY CLUSTERED (PersonName),
  CONSTRAINT ck_Person_PersonName
	CHECK (RTRIM(PersonName) != '')
);
GO

CREATE TABLE dbo.Restaurant(
  RestaurantName varchar(32) NOT NULL,
  CONSTRAINT pk_Restaurant
	PRIMARY KEY CLUSTERED (RestaurantName),
  CONSTRAINT ck_Restaurant_RestaurantName
	CHECK (RTRIM(RestaurantName) != ''),
  CuisineType varchar(16) NOT NULL,
  CONSTRAINT ck_Restaurant_CuisineType
	CHECK (CuisineType IN (
				'Mexican',
				'Pacific Rim',
				'Thai',
				'American'
			) )
);
GO

CREATE TABLE dbo.Review(
  PersonName varchar(32) NOT NULL,
  RestaurantName varchar(32) NOT NULL,
  CONSTRAINT pk_Review
	PRIMARY KEY CLUSTERED(PersonName, RestaurantName),
  Rating float NOT NULL,
  CONSTRAINT ck_Review_Rating
	CHECK (Rating BETWEEN 0 AND 5)
);
GO

ALTER TABLE dbo.Review ADD
  CONSTRAINT fk_Review_to_Person
	FOREIGN KEY (PersonName)
	REFERENCES dbo.Person(PersonName)
, CONSTRAINT fk_Review_to_Restaurant
	FOREIGN KEY (RestaurantName)
	REFERENCES dbo.Restaurant(RestaurantName)
;
GO

INSERT INTO dbo.Person(PersonName)
SELECT 'Mary' UNION ALL
SELECT 'Joe' UNION ALL
SELECT 'Betty' UNION ALL
SELECT 'Fred' UNION ALL
SELECT 'Alice' UNION ALL
SELECT 'Bob';
GO

INSERT INTO dbo.Restaurant(RestaurantName, CuisineType)
SELECT 'Cholo''s', 'Mexican' UNION ALL
SELECT 'Arceo''s', 'Mexican' UNION ALL
SELECT 'The Dolphin', 'Pacific Rim' UNION ALL
SELECT 'Turtle Bay Cafe', 'Pacific Rim' UNION ALL
SELECT 'Satyama', 'Pacific Rim' UNION ALL
SELECT 'Bubba''s', 'American' UNION ALL
SELECT 'McDonald''s', 'American' UNION ALL
SELECT 'Honolulu Thai', 'Thai';
GO

INSERT INTO dbo.Review(PersonName, RestaurantName, Rating)
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Mary' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 4 [Rating] UNION ALL
	SELECT 'Arceo''s', 4 UNION ALL
	SELECT 'The Dolphin', 5 UNION ALL
	SELECT 'Turtle Bay Cafe', 3 UNION ALL
	SELECT 'Satyama', 5 UNION ALL
	SELECT 'Bubba''s', 3 UNION ALL
	SELECT 'McDonald''s', 3 UNION ALL
	SELECT 'Honolulu Thai', 4
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Joe' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 1 [Rating] UNION ALL
	SELECT 'Arceo''s', 1 UNION ALL
	SELECT 'The Dolphin', 2 UNION ALL
	SELECT 'Turtle Bay Cafe', 0 UNION ALL
	SELECT 'Satyama', 2 UNION ALL
	SELECT 'Bubba''s', 0 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 1
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Betty' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 5 [Rating] UNION ALL
	SELECT 'The Dolphin', 4 UNION ALL
	SELECT 'Satyama', 5 UNION ALL
	SELECT 'Bubba''s', 4 UNION ALL
	SELECT 'Honolulu Thai', 5
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Fred' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Arceo''s' [RestaurantName], 1 [Rating] UNION ALL
	SELECT 'The Dolphin', 2 UNION ALL
	SELECT 'Satyama', 1 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 2
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Alice' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 5 [Rating] UNION ALL
	SELECT 'The Dolphin', 3 UNION ALL
	SELECT 'Turtle Bay Cafe', 4 UNION ALL
	SELECT 'Satyama', 3 UNION ALL
	SELECT 'Bubba''s', 5
) dY

UNION ALL
SELECT PersonName, RestaurantName, Rating
FROM
(
	SELECT 'Bob' [PersonName]
) dX
CROSS JOIN
(
	SELECT 'Cholo''s' [RestaurantName], 4 [Rating] UNION ALL
	SELECT 'Turtle Bay Cafe', 3 UNION ALL
	SELECT 'Bubba''s', 1 UNION ALL
	SELECT 'McDonald''s', 1 UNION ALL
	SELECT 'Honolulu Thai', 3
) dY
;
GO
