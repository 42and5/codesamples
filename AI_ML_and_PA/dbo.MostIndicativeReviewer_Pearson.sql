/****************************************************
  This T-SQL script by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'MostIndicativeReviewer_Pearson' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.MostIndicativeReviewer_Pearson AS SELECT 1');
END;
GO

/**************************************************************************************************
  Use Pearson's r to find the single reviewer that
    most strongly predicts user's tastes (note that
    this may be via correlation in the negative
    direction)
**************************************************************************************************/
ALTER PROC dbo.MostIndicativeReviewer_Pearson
  @PersonName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Find closest reviewer
SELECT TOP 1 PersonName, Pearson, ElementMatches
FROM
(
	SELECT r2.PersonName
	, (
		SUM(r1.Rating * r2.Rating) - SUM(r1.Rating) * SUM(r2.Rating) / CAST(COUNT(*) AS float)) 
		/ 
		(COUNT(*) * STDEVP(r1.Rating) * STDEVP(r2.Rating) 
	) [Pearson]

	, COUNT(*) [ElementMatches]

	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
) dX
-- Could filter on minimum number of ElementMatches
--  or minimum similarity (or both) here
ORDER BY ABS(dX.Pearson) DESC;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.MostIndicativeReviewer_Pearson @PersonName = 'Joe';
GO
