/****************************************************
  This T-SQL script by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'MostSimilarReviewer_Euclidean' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.MostSimilarReviewer_Euclidean AS SELECT 1');
END;
GO

/**************************************************************************************************
  Use Euclidean distance to find the single best-matching reviewer
**************************************************************************************************/
ALTER PROC dbo.MostSimilarReviewer_Euclidean
  @PersonName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Find closest reviewer
DECLARE @Scale float = 5.0 - 0.0;

SELECT TOP 1 PersonName, InvertedEuclid, ElementMatches
FROM
(
	SELECT r2.PersonName
	, (SQRT(COUNT(*)) * @Scale /* MaxPossEucl */ - SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating)))) /* RawEucl */
	  / (SQRT(COUNT(*)) * @Scale) [InvertedEuclid] -- Transforms to linear scaling, higher is better match
	, COUNT(*) [ElementMatches]
	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
) dX
-- Could filter on minimum number of ElementMatches
--  or minimum similarity (or both) here
ORDER BY dX.InvertedEuclid DESC;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.MostSimilarReviewer_Euclidean @PersonName = 'Joe';
GO
