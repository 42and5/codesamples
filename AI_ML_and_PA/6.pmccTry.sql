/****************************************************
  This T-SQL script by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

-- Pearson product-moment correlation coefficient based on given formula
SELECT r2.PersonName
, (COUNT(*) * SUM(r1.Rating * r2.Rating) - SUM(r1.Rating) * SUM(r2.Rating))
	/
  (
	SQRT(COUNT(*) * SUM(r1.Rating * r1.Rating) - SUM(r1.Rating) * SUM(r1.Rating))
	*
	SQRT(COUNT(*) * SUM(r2.Rating * r2.Rating) - SUM(r2.Rating) * SUM(r2.Rating))
  ) [PearsonFormula]

,	/* num */(SUM(r1.Rating * r2.Rating) - SUM(r1.Rating) * SUM(r2.Rating) / COUNT(*))
			/
	/* den */SQRT((SUM(r1.Rating * r1.Rating)-(SUM(r1.Rating) * SUM(r1.Rating)) / COUNT(*)) 
					*
					(SUM(r2.Rating * r2.Rating)-(SUM(r2.Rating) * SUM(r2.Rating)) / COUNT(*)))
  [PearsonComplex]
, COUNT(*) [ElementMatches]
FROM dbo.Review r1
INNER JOIN dbo.Review r2
	ON r1.RestaurantName = r2.RestaurantName
   --AND r1.PersonName != r2.PersonName
WHERE r1.PersonName = 'Joe'
GROUP BY r1.PersonName, r2.PersonName
ORDER BY PearsonComplex DESC;
GO



-- Pearson product-moment correlation coefficient, 
-- expression simplified by leveraging standard deviation
SELECT r2.PersonName,
	/* num */(SUM(r1.Rating * r2.Rating) - SUM(r1.Rating) * SUM(r2.Rating) / COUNT(*))
			/
	/* den */SQRT((SUM(r1.Rating * r1.Rating)-(SUM(r1.Rating) * SUM(r1.Rating)) / COUNT(*)) 
					*
					(SUM(r2.Rating * r2.Rating)-(SUM(r2.Rating) * SUM(r2.Rating)) / COUNT(*)))
  [PearsonComplex]
,
(
	SUM(r1.Rating * r2.Rating) - SUM(r1.Rating) * SUM(r2.Rating) / CAST(COUNT(*) AS float) ) 
	/ 
	(COUNT(*) * STDEVP(r1.Rating) * STDEVP(r2.Rating) 
) [PearsonSimpler]

, COUNT(*) [ElementMatches]
FROM dbo.Review r1
INNER JOIN dbo.Review r2
	ON r1.RestaurantName = r2.RestaurantName
   --AND r1.PersonName != r2.PersonName
WHERE r1.PersonName = 'Joe'
GROUP BY r1.PersonName, r2.PersonName
ORDER BY PearsonSimpler DESC;
GO
