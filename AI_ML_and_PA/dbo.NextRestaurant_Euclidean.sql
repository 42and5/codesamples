/****************************************************
  This T-SQL script by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

IF NOT EXISTS(
		SELECT 1 
		FROM INFORMATION_SCHEMA.ROUTINES
		WHERE ROUTINE_NAME = N'NextRestaurant_Euclidean' --<< enclose in single quotes
		  AND ROUTINE_SCHEMA = N'dbo'
		  AND ROUTINE_TYPE = N'PROCEDURE'
)
BEGIN
	EXEC ('CREATE PROC dbo.NextRestaurant_Euclidean AS SELECT 1');
END;
GO

/**************************************************************************************************
  Use Euclidean distance to find the single best-matching reviewer,
    and then return the highest-rated restaurant that the user has not yet visited
**************************************************************************************************/
ALTER PROC dbo.NextRestaurant_Euclidean
  @PersonName varchar(32)
AS
SET NOCOUNT ON;

DECLARE @retVal int;
SET @retVal = 0; -- Assume all OK

-- Perform operation in steps to more easily see the logic

-- Find set of "eligible" reviewers: those that 
-- have reviewed a restaurant that the user hasn't yet
DECLARE @PossibleReviewers TABLE(PersonName varchar(32) NOT NULL PRIMARY KEY);
INSERT INTO @PossibleReviewers(PersonName)
SELECT DISTINCT r.PersonName
FROM dbo.Review r
WHERE NOT EXISTS(
	SELECT 1
	FROM dbo.Review
	WHERE PersonName = @PersonName
	  AND RestaurantName = r.RestaurantName
);

-- Find closest reviewer
DECLARE @BestMatch varchar(32)
	-- Next 2 variables are handy for sanity 
	-- checking, further refinement
	,	@Similarity float
	,	@ElementMatches int;

DECLARE @Scale float = 5.0 - 0.0;

SELECT TOP 1 @BestMatch = PersonName
			, @Similarity = InvertedEuclid
			, @ElementMatches = ElementMatches
FROM
(
	SELECT r2.PersonName
	, (SQRT(COUNT(*)) * @Scale /* MaxPossEucl */ - SQRT(SUM((r1.Rating - r2.Rating) * (r1.Rating - r2.Rating)))) /* RawEucl */
	  / (SQRT(COUNT(*)) * @Scale) [InvertedEuclid] -- Transforms to linear scaling, higher is better match
	, COUNT(*) [ElementMatches]
	FROM dbo.Review r1
	INNER JOIN dbo.Review r2
		ON r1.RestaurantName = r2.RestaurantName
	   AND r1.PersonName != r2.PersonName
	INNER JOIN @PossibleReviewers pr
		ON r2.PersonName = pr.PersonName
	WHERE r1.PersonName = @PersonName
	GROUP BY r1.PersonName, r2.PersonName
) dX
-- Thresholds on element matches could be
-- incorporated here as additional predicates
ORDER BY dX.InvertedEuclid DESC;

-- Find highest-rated restaurant that the user hasn't yet visited
SELECT TOP 1 rRecommended.RestaurantName, rRecommended.Rating
	, @BestMatch [RecommendedBy], @Similarity [TasteSimilarity]
	, @ElementMatches [NbrCommonRestaurants]
FROM dbo.Review rRecommended
LEFT JOIN dbo.Review rUser
	ON rUser.PersonName = @PersonName
   AND rRecommended.RestaurantName = rUser.RestaurantName
WHERE rRecommended.PersonName = @BestMatch
  AND rUser.RestaurantName IS NULL
ORDER BY rRecommended.Rating DESC;

RETURN @retVal;
GO

SELECT 'Sanity Check';
EXEC dbo.NextRestaurant_Euclidean @PersonName = 'Joe';
GO
