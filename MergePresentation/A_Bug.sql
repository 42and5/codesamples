CREATE TABLE dbo.Repro(
  i int NOT NULL,
  j int NOT NULL,
  CONSTRAINT pk_Repro
	PRIMARY KEY NONCLUSTERED (i, j),
  SomeString varchar(128) NOT NULL,
  Flotsam AS 666 PERSISTED NOT NULL
);
GO
CREATE CLUSTERED INDEX cdx_Repro_SomeString
	ON dbo.Repro(SomeString);
GO

DECLARE @ToMerge TABLE(i int, j int, SomeString varchar(128), PRIMARY KEY (i, j));

INSERT INTO @ToMerge(i, j, SomeString)
SELECT 1, 100, 'NewString'
;

-- Exercise the "INSERT" portion of the MERGE statement...
MERGE INTO dbo.Repro WITH(HOLDLOCK) AS target
USING @ToMerge AS source
	ON target.i = source.i
   AND target.j = source.j
WHEN MATCHED THEN
	UPDATE SET
		SomeString = source.SomeString
WHEN NOT MATCHED THEN
	INSERT (i, j, SomeString)
	VALUES(source.i, source.j, source.SomeString)
;
GO

DROP TABLE dbo.Repro;
GO
