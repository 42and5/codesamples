-- BEFORE
SELECT * FROM dbo.MOrderDetail;

DECLARE @OrderTVP TABLE (OrderID int, CustomerID int, OrderPlacedDate datetime);
INSERT INTO @OrderTVP(OrderID, CustomerID, OrderPlacedDate)
SELECT 1, 1001, '2010-09-15'; 

-- Desired End Value for the Order Details of order # 1
DECLARE @OrderDetailTVP TABLE (OrderID int, OrderLineNbr int, ProductID int, Qty int)
INSERT INTO @OrderDetailTVP(OrderID, OrderLineNbr, ProductID, Qty)
SELECT 1, 1, 123, 7 UNION ALL -- Wish to change line # 1's qty to 7
-- Note omission of Line #2 - for some reason we want that gone for this order
SELECT 1, 3, 888, 10;

-- Attempt to effect the changes as a unit of work:
-- Use the "UPSERT" version so as to preserve orders we're not actively changing
MERGE INTO dbo.MOrder AS target
USING @OrderTVP AS source
	ON target.OrderID = source.OrderID
WHEN MATCHED THEN
	UPDATE SET
		-- Note: ignoring any potential changes to CustomerID for this example
		OrderPlacedDate = source.OrderPlacedDate
WHEN NOT MATCHED THEN
	INSERT (OrderID, CustomerID, OrderPlacedDate)
	VALUES (source.OrderID, source.CustomerID, source.OrderPlacedDate)
;

-- Use the "UPSERT" + "DELETE" version to replace the entire set of details for the order
MERGE INTO dbo.MOrderDetail AS target
USING @OrderDetailTVP AS source
	ON target.OrderID = source.OrderID
   AND target.OrderLineNbr = source.OrderLineNbr
WHEN MATCHED THEN
	UPDATE SET
		Qty = source.Qty
WHEN NOT MATCHED BY target THEN
	INSERT (OrderID, OrderLineNbr, ProductID, Qty)
	VALUES (source.OrderID, source.OrderLineNbr, source.ProductID, source.Qty)
WHEN NOT MATCHED BY source 
	-- our adjustment: only delete if this was an order we are modifying...
	AND target.OrderID IN (SELECT OrderID FROM @OrderTVP) THEN
	DELETE;
GO

SELECT * FROM dbo.MOrderDetail;
GO