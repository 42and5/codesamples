-- Use a CTE as the source...
WITH NewValues
AS
(
	SELECT 1 [i], 999 [j] UNION ALL
	SELECT 2, 888
)

-- Only act on the "UPDATE" condition
MERGE INTO dbo.MTarget AS target
USING NewValues AS source
	ON target.i = source.i
WHEN MATCHED THEN
	UPDATE SET
		j = source.j
;

SELECT *
FROM dbo.MTarget;
