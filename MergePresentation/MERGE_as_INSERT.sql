-- Using a table variable as the source...
DECLARE @Source TABLE (i int, j int);
INSERT INTO @Source(i, j)
SELECT 1, 123 UNION ALL
SELECT 2, 456;

-- Only act on the "INSERT" condition
MERGE INTO dbo.MTarget AS target
USING @Source AS source
	ON target.i = source.i
WHEN NOT MATCHED /* BY target */ THEN
	INSERT (i, j)
	VALUES(source.i, source.j)
;


SELECT *
FROM dbo.MTarget;
