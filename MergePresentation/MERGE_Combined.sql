-- Setup with a "regular" Truncate/INSERT...
TRUNCATE TABLE dbo.MTarget;
INSERT INTO dbo.MTarget(i, j)
SELECT 1, 123 UNION ALL
SELECT 2, 456;
GO

-- BEFORE MERGE
SELECT * FROM dbo.MTarget;

WITH DesiredEndValue
AS
(
	-- NOTE: No "i = 1" row
	SELECT 2 [i], 999 [j] UNION ALL -- Should update "i = 2" row
	SELECT 3, 987			-- Should add an "i = 3" row
)

MERGE INTO dbo.MTarget AS target
USING DesiredEndValue AS source
	ON target.i = source.i
WHEN MATCHED THEN 
	UPDATE SET
		j = source.j
WHEN NOT MATCHED BY target THEN
	INSERT (i, j)
	VALUES (source.i, source.j)
WHEN NOT MATCHED BY source THEN
	DELETE
;

-- AFTER MERGE
SELECT * FROM dbo.MTarget;
