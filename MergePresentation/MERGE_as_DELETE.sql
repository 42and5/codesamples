-- Using a subquery as the source...

-- Only act on the "DELETE" condition
MERGE INTO dbo.MTarget AS target
USING (SELECT -1, -666 WHERE 1 = 0) AS source(i, j)
	ON target.i = source.i
WHEN NOT MATCHED BY source THEN
	DELETE
;

SELECT *
FROM dbo.MTarget;