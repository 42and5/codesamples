-- Using a table variable as the target...
DECLARE @Target TABLE (i int, j int);

-- Setup with "regular" INSERT
INSERT INTO @Target(i, j)
VALUES(1, 123), (2, 456);

WITH DesiredEndValue
AS
(
	-- NOTE: No "i = 1" row
	SELECT 2 [Attr1], 999 [Attr2], 'Hmmmm' [Attr3] UNION ALL
	SELECT 3, 987, 'WellNow'
)

MERGE INTO @Target AS target
USING (
		SELECT Attr1, Attr2, Attr3 
		FROM DesiredEndValue
	) AS source(i, j, Fred)
	ON target.i = source.i
WHEN MATCHED THEN 
	UPDATE SET
		j = source.j
WHEN NOT MATCHED BY target THEN
	INSERT (i, j)
	VALUES (source.i, source.j)
WHEN NOT MATCHED BY source THEN
	DELETE
OUTPUT inserted.*
;
