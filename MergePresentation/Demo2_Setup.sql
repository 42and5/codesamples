IF OBJECT_ID('dbo.MOrderDetail') IS NOT NULL DROP TABLE dbo.MOrderDetail;
IF OBJECT_ID('dbo.MOrder') IS NOT NULL DROP TABLE dbo.MOrder;
GO

CREATE TABLE dbo.MOrder(
  OrderID int NOT NULL PRIMARY KEY,
  CustomerID int NOT NULL,
  OrderPlacedDate datetime NOT NULL
);
GO

CREATE TABLE dbo.MOrderDetail(
  OrderID int NOT NULL REFERENCES dbo.MOrder(OrderID),
  OrderLineNbr int NOT NULL,
  CONSTRAINT pk_MOrderDetail
	PRIMARY KEY CLUSTERED (OrderID, OrderLineNbr),
  ProductID int NOT NULL,
  Qty int NOT NULL
);
GO

-- Setup using "regular" INSERTs
INSERT INTO dbo.MOrder(OrderID, CustomerID, OrderPlacedDate)
SELECT 1, 1001, '2010-09-01' UNION ALL
SELECT 2, 2002, '2010-09-02';

INSERT INTO dbo.MOrderDetail(OrderID, OrderLineNbr, ProductID, Qty)
SELECT 1, 1, 123, 5 UNION ALL
SELECT 1, 2, 456, 3 UNION ALL
SELECT 2, 1, 999, 10;
GO
