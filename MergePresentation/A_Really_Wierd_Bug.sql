-- This portion uses the MERGE statement and demonstrates that
-- even though there is a full match on the {SomeID, SomeType}
-- superkey, the Foreign Key cannot be applied.

-- BEGIN MERGE DEMO ---
CREATE TABLE dbo.ReproParentMerge(
  SomeID int NOT NULL,
  SomeType int NOT NULL
);
GO
ALTER TABLE dbo.ReproParentMerge ADD
  CONSTRAINT pk_ReproParentMerge
	PRIMARY KEY CLUSTERED(SomeID)
, CONSTRAINT sk_ReproParentMerge
	UNIQUE NONCLUSTERED (SomeID, SomeType)
;
GO

CREATE TABLE dbo.ReproChildMerge(
  SomeID int NOT NULL,
  SomeType AS CAST(666 AS int) PERSISTED NOT NULL,
  SomeThing varchar(20) NOT NULL
);
GO
ALTER TABLE dbo.ReproChildMerge ADD
  CONSTRAINT pk_ReproChildMerge
	PRIMARY KEY CLUSTERED (SomeID)
;
GO

MERGE INTO dbo.ReproChildMerge AS target
USING (SELECT 1 [SomeID], 'Tester' [SomeThing]) AS source(SomeID, SomeThing)
	ON target.SomeID = source.SomeID
WHEN MATCHED THEN
	UPDATE SET SomeThing = source.SomeThing
WHEN NOT MATCHED THEN
	INSERT (SomeID, SomeThing)
	VALUES(source.SomeID, source.SomeThing)
;
GO

INSERT INTO dbo.ReproParentMerge(SomeID, SomeType)
SELECT 1, 666;
GO

SELECT COUNT(*) [Matches]
FROM dbo.ReproChildMerge rc
INNER JOIN dbo.ReproParentMerge rp
	ON rc.SomeID = rp.SomeID
   AND rc.SomeType = rp.SomeType
;
GO

SELECT COUNT(*) [Anomalies]
FROM dbo.ReproChildMerge
WHERE NOT EXISTS(
	SELECT 1
	FROM dbo.ReproParentMerge
	WHERE SomeID = dbo.ReproChildMerge.SomeID
	  AND SomeType = dbo.ReproChildMerge.SomeType
);
GO

ALTER TABLE dbo.ReproChildMerge ADD
  CONSTRAINT fk_ReproChildMerge_to_ReproParentMerge
	FOREIGN KEY (SomeID, SomeType)
	REFERENCES dbo.ReproParentMerge(SomeID, SomeType)
;
GO

DROP TABLE dbo.ReproChildMerge;
DROP TABLE dbo.ReproParentMerge;
GO
-- END MERGE DEMO ---


-- This script uses an INSERT statement rather than the 
-- MERGE statement, and demonstrates that the Foreign Key
-- can be applied without error

-- BEGIN INSERT DEMO ---
CREATE TABLE dbo.ReproParentInsert(
  SomeID int NOT NULL,
  SomeType int NOT NULL
);
GO
ALTER TABLE dbo.ReproParentInsert ADD
  CONSTRAINT pk_ReproParentInsert
	PRIMARY KEY CLUSTERED(SomeID)
, CONSTRAINT sk_ReproParentInsert
	UNIQUE NONCLUSTERED (SomeID, SomeType)
;
GO

CREATE TABLE dbo.ReproChildInsert(
  SomeID int NOT NULL,
  SomeType AS CAST(666 AS int) PERSISTED NOT NULL,
  SomeThing varchar(20) NOT NULL
);
GO
ALTER TABLE dbo.ReproChildInsert ADD
  CONSTRAINT pk_ReproChildInsert
	PRIMARY KEY CLUSTERED (SomeID)
;
GO

INSERT INTO dbo.ReproChildInsert(SomeID, SomeThing)
SELECT source.SomeID, source.SomeThing
FROM (SELECT 1 [SomeID], 'Tester' [SomeThing]) AS source;
GO

INSERT INTO dbo.ReproParentInsert(SomeID, SomeType)
SELECT 1, 666;
GO

SELECT COUNT(*) [Matches]
FROM dbo.ReproChildInsert rc
INNER JOIN dbo.ReproParentInsert rp
	ON rc.SomeID = rp.SomeID
   AND rc.SomeType = rp.SomeType
;
GO

SELECT COUNT(*) [Anomalies]
FROM dbo.ReproChildInsert
WHERE NOT EXISTS(
	SELECT 1
	FROM dbo.ReproParentInsert
	WHERE SomeID = dbo.ReproChildInsert.SomeID
	  AND SomeType = dbo.ReproChildInsert.SomeType
);
GO

ALTER TABLE dbo.ReproChildInsert ADD
  CONSTRAINT fk_ReproChildInsert_to_ReproParentInsert
	FOREIGN KEY (SomeID, SomeType)
	REFERENCES dbo.ReproParentInsert(SomeID, SomeType)
;
GO

DROP TABLE dbo.ReproChildInsert;
DROP TABLE dbo.ReproParentInsert;
GO
-- END INSERT DEMO ---
