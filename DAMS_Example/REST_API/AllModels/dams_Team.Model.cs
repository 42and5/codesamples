using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dams_Team_Api.Models
{
	
	public class Team
	{
		
		[Required]
		public Guid TeamHUID{ get; set; }
		[Required]
		public String TeamName{ get; set; }
		[Required]
		public Guid OrganizationHUID{ get; set; }
	
	
		public Team()
		{
		}
	}
	
}
