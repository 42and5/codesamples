using Microsoft.EntityFrameworkCore;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace dams_HumanAgentWithTeamsAndRoles_Api.Models
{
    public class HumanAgentWithTeamsAndRoles_Context : DbContext
    {
        public Dictionary<SprocVerb, SqlCommand> cmdSproc;
        public Dictionary<(SprocVerb, SprocParam), SqlParameter> prmSproc;

        public enum SprocVerb{
            GetAll, 
            GetByKey, 
            Insert, 
            Update, 
            Delete
			,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete,
			TeamWithMembers_node_dams_TeamMemberDelete
        };
        public enum SprocParam{
            @ErrorNumber, 
            @ErrorMessage, 
            @retVal,
			@GrantedBy,
			@GrantedOn,
			@HumanAgentHUID,
			@OrganizationHUID,
			@RoleName,
			@TeamHUID, 
			@tvpdamsRoleAssignment,
			@tvpdamsTeamMember
        };

        public HumanAgentWithTeamsAndRoles_Context(DbContextOptions<HumanAgentWithTeamsAndRoles_Context> options)
            : base(options)
        {

            SqlConnection cnx = (SqlConnection)this.Database.GetDbConnection();

            cmdSproc = new Dictionary<SprocVerb, SqlCommand>(){
				{ SprocVerb.GetAll, new SqlCommand("dams.HumanAgentWithTeamsAndRolesGetAll", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.GetByKey, new SqlCommand("dams.HumanAgentWithTeamsAndRolesGetByKey", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Insert, new SqlCommand("dams.HumanAgentWithTeamsAndRolesInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Update, new SqlCommand("dams.HumanAgentWithTeamsAndRolesUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Delete, new SqlCommand("dams.HumanAgentWithTeamsAndRolesDelete", cnx){ CommandType = CommandType.StoredProcedure } }
				,
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } }
            };

            prmSproc = new Dictionary<(SprocVerb, SprocParam), SqlParameter>(){
				
				{ (SprocVerb.GetAll, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.GetByKey, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Insert, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@tvpdamsRoleAssignment), new SqlParameter("@tvpdamsRoleAssignment", SqlDbType.Structured) },
				{ (SprocVerb.Insert, SprocParam.@tvpdamsTeamMember), new SqlParameter("@tvpdamsTeamMember", SqlDbType.Structured) },
				{ (SprocVerb.Insert, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Update, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@tvpdamsRoleAssignment), new SqlParameter("@tvpdamsRoleAssignment", SqlDbType.Structured) },
				{ (SprocVerb.Update, SprocParam.@tvpdamsTeamMember), new SqlParameter("@tvpdamsTeamMember", SqlDbType.Structured) },
				{ (SprocVerb.Update, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Delete, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Delete, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } }

				,
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, SprocParam.@RoleName), new SqlParameter("@RoleName", SqlDbType.VarChar, 128) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, SprocParam.@GrantedOn), new SqlParameter("@GrantedOn", SqlDbType.DateTime2, 7) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, SprocParam.@GrantedBy), new SqlParameter("@GrantedBy", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, SprocParam.@RoleName), new SqlParameter("@RoleName", SqlDbType.VarChar, 128) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, SprocParam.@GrantedOn), new SqlParameter("@GrantedOn", SqlDbType.DateTime2, 7) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, SprocParam.@GrantedBy), new SqlParameter("@GrantedBy", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, SprocParam.@RoleName), new SqlParameter("@RoleName", SqlDbType.VarChar, 128) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, SprocParam.@OrganizationHUID), new SqlParameter("@OrganizationHUID", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, SprocParam.@OrganizationHUID), new SqlParameter("@OrganizationHUID", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } }
            };

            foreach( var p in prmSproc )
            {
                cmdSproc[p.Key.Item1].Parameters.Add(p.Value);
            }
        }

 		public DbSet<HumanAgentWithTeamsAndRoles> HumanAgentWithTeamsAndRoles { get; set; }
		public DbSet<HumanAgentWithTeamsAndRoles_node_dams_RoleAssignment> HumanAgentWithTeamsAndRoles_node_dams_RoleAssignment { get; set; }
		public DbSet<HumanAgentWithTeamsAndRoles_node_dams_TeamMember> HumanAgentWithTeamsAndRoles_node_dams_TeamMember { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			modelBuilder.Entity<HumanAgentWithTeamsAndRoles>().HasKey(c => new{ c.HumanAgentHUID });
			modelBuilder.Entity<HumanAgentWithTeamsAndRoles.dams_RoleAssignment>().HasKey(c => new{ c.RoleName });
			modelBuilder.Entity<HumanAgentWithTeamsAndRoles.dams_TeamMember>().HasKey(c => new{ c.TeamHUID });
			modelBuilder.Entity<HumanAgentWithTeamsAndRoles_node_dams_RoleAssignment>().HasKey(c => new{ c.HumanAgentHUID, c.RoleName });
			modelBuilder.Entity<HumanAgentWithTeamsAndRoles_node_dams_TeamMember>().HasKey(c => new{ c.HumanAgentHUID, c.TeamHUID });
        }
    }
}
