using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dams_TeamWithMembers_Api.Models
{
	
	public class TeamWithMembers
	{
		public class dams_TeamMember
		{
			
			[Required]
			public Guid TeamHUID{ get; set; }
			[Required]
			public Guid HumanAgentHUID{ get; set; }
			[Required]
			public Guid OrganizationHUID{ get; set; }
		
		
			public dams_TeamMember()
			{
			}
		}
		[Required]
		public Guid TeamHUID{ get; set; }
		[Required]
		public String TeamName{ get; set; }
		[Required]
		public Guid OrganizationHUID{ get; set; }
	
		public List<dams_TeamMember> dams_TeamMember_Entries{ get; set; }
	
		public TeamWithMembers()
		{
			dams_TeamMember_Entries = new List<dams_TeamMember>();
		}
	}
	
	
	public class TeamWithMembers_node_dams_TeamMember
	{
		
		[Required]
		public Guid TeamHUID{ get; set; }
		[Required]
		public Guid HumanAgentHUID{ get; set; }
		[Required]
		public Guid OrganizationHUID{ get; set; }
	
	
		public TeamWithMembers_node_dams_TeamMember()
		{
		}
	}
	
}
