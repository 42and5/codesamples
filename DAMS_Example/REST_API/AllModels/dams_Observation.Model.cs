using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dams_Observation_Api.Models
{
	
	public class Observation
	{
		
		[Required]
		public Guid PointCloudHUID{ get; set; }
		[Required]
		public String ObservationTypeCode{ get; set; }
		[Required]
		public DateTime2 ObservationMadeOn{ get; set; }
		[Required]
		public Decimal Value{ get; set; }
		[Required]
		public Guid TeamHUID{ get; set; }
		[Required]
		public Guid ObservedBy{ get; set; }
	
	
		public Observation()
		{
		}
	}
	
}
