using Microsoft.EntityFrameworkCore;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace dams_Team_Api.Models
{
    public class Team_Context : DbContext
    {
        public Dictionary<SprocVerb, SqlCommand> cmdSproc;
        public Dictionary<(SprocVerb, SprocParam), SqlParameter> prmSproc;

        public enum SprocVerb{
            GetAll, 
            GetByKey, 
            Insert, 
            Update, 
            Delete
			,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete,
			TeamWithMembers_node_dams_TeamMemberDelete
        };
        public enum SprocParam{
            @ErrorNumber, 
            @ErrorMessage, 
            @retVal,
			@OrganizationHUID,
			@TeamHUID,
			@TeamName
        };

        public Team_Context(DbContextOptions<Team_Context> options)
            : base(options)
        {

            SqlConnection cnx = (SqlConnection)this.Database.GetDbConnection();

            cmdSproc = new Dictionary<SprocVerb, SqlCommand>(){
				{ SprocVerb.GetAll, new SqlCommand("dams.TeamGetAll", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.GetByKey, new SqlCommand("dams.TeamGetByKey", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Insert, new SqlCommand("dams.TeamInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Update, new SqlCommand("dams.TeamUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Delete, new SqlCommand("dams.TeamDelete", cnx){ CommandType = CommandType.StoredProcedure } }
				,
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } }
            };

            prmSproc = new Dictionary<(SprocVerb, SprocParam), SqlParameter>(){
				
				{ (SprocVerb.GetAll, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.GetByKey, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Insert, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@TeamName), new SqlParameter("@TeamName", SqlDbType.NVarChar, 128) },
				{ (SprocVerb.Insert, SprocParam.@OrganizationHUID), new SqlParameter("@OrganizationHUID", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.Insert, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Update, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@TeamName), new SqlParameter("@TeamName", SqlDbType.NVarChar, 128) },
				{ (SprocVerb.Update, SprocParam.@OrganizationHUID), new SqlParameter("@OrganizationHUID", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.Update, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Delete, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Delete, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } }

            };

            foreach( var p in prmSproc )
            {
                cmdSproc[p.Key.Item1].Parameters.Add(p.Value);
            }
        }

 		public DbSet<Team> Team { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			modelBuilder.Entity<Team>().HasKey(c => new{ c.TeamHUID });
        }
    }
}
