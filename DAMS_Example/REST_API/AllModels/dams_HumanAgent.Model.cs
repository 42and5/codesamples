using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dams_HumanAgent_Api.Models
{
	
	public class HumanAgent
	{
		
		[Required]
		public Guid HumanAgentHUID{ get; set; }
		[Required]
		public Guid OrganizationHUID{ get; set; }
		[Required]
		public String PrimaryEmail{ get; set; }
		public String HumanAgentExternalIdentifier{ get; set; }
		[Required]
		public String GivenName{ get; set; }
		[Required]
		public String Surname{ get; set; }
	
	
		public HumanAgent()
		{
		}
	}
	
}
