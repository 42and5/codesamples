using Microsoft.EntityFrameworkCore;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace dams_PointCloudMetadata_Api.Models
{
    public class PointCloudMetadata_Context : DbContext
    {
        public Dictionary<SprocVerb, SqlCommand> cmdSproc;
        public Dictionary<(SprocVerb, SprocParam), SqlParameter> prmSproc;

        public enum SprocVerb{
            GetAll, 
            GetByKey, 
            Insert, 
            Update, 
            Delete
			,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete,
			TeamWithMembers_node_dams_TeamMemberDelete
        };
        public enum SprocParam{
            @ErrorNumber, 
            @ErrorMessage, 
            @retVal,
			@CapturedBy,
			@CapturedOrDerivedOn,
			@CaptureMetadata,
			@PhysicalDescription,
			@PointCloudHUID,
			@RawDataURI,
			@RepresentationCode,
			@SubspaceOfPointCloud,
			@TeamHUID,
			@Thumbnail
        };

        public PointCloudMetadata_Context(DbContextOptions<PointCloudMetadata_Context> options)
            : base(options)
        {

            SqlConnection cnx = (SqlConnection)this.Database.GetDbConnection();

            cmdSproc = new Dictionary<SprocVerb, SqlCommand>(){
				{ SprocVerb.GetAll, new SqlCommand("dams.PointCloudMetadataGetAll", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.GetByKey, new SqlCommand("dams.PointCloudMetadataGetByKey", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Insert, new SqlCommand("dams.PointCloudMetadataInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Update, new SqlCommand("dams.PointCloudMetadataUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Delete, new SqlCommand("dams.PointCloudMetadataDelete", cnx){ CommandType = CommandType.StoredProcedure } }
				,
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } }
            };

            prmSproc = new Dictionary<(SprocVerb, SprocParam), SqlParameter>(){
				
				{ (SprocVerb.GetAll, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.GetByKey, SprocParam.@PointCloudHUID), new SqlParameter("@PointCloudHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Insert, SprocParam.@PointCloudHUID), new SqlParameter("@PointCloudHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@SubspaceOfPointCloud), new SqlParameter("@SubspaceOfPointCloud", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@Thumbnail), new SqlParameter("@Thumbnail", SqlDbType.VarBinary, MAX) },
				{ (SprocVerb.Insert, SprocParam.@PhysicalDescription), new SqlParameter("@PhysicalDescription", SqlDbType.NVarChar, 1024) },
				{ (SprocVerb.Insert, SprocParam.@RepresentationCode), new SqlParameter("@RepresentationCode", SqlDbType.VarChar, 64) },
				{ (SprocVerb.Insert, SprocParam.@CaptureMetadata), new SqlParameter("@CaptureMetadata", SqlDbType.VarChar, 18) },
				{ (SprocVerb.Insert, SprocParam.@RawDataURI), new SqlParameter("@RawDataURI", SqlDbType.VarChar, 440) },
				{ (SprocVerb.Insert, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@CapturedBy), new SqlParameter("@CapturedBy", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@CapturedOrDerivedOn), new SqlParameter("@CapturedOrDerivedOn", SqlDbType.DateTime2, 7) },/* No TVPs */
				{ (SprocVerb.Insert, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Update, SprocParam.@PointCloudHUID), new SqlParameter("@PointCloudHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@SubspaceOfPointCloud), new SqlParameter("@SubspaceOfPointCloud", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@Thumbnail), new SqlParameter("@Thumbnail", SqlDbType.VarBinary, MAX) },
				{ (SprocVerb.Update, SprocParam.@PhysicalDescription), new SqlParameter("@PhysicalDescription", SqlDbType.NVarChar, 1024) },
				{ (SprocVerb.Update, SprocParam.@RepresentationCode), new SqlParameter("@RepresentationCode", SqlDbType.VarChar, 64) },
				{ (SprocVerb.Update, SprocParam.@CaptureMetadata), new SqlParameter("@CaptureMetadata", SqlDbType.VarChar, 18) },
				{ (SprocVerb.Update, SprocParam.@RawDataURI), new SqlParameter("@RawDataURI", SqlDbType.VarChar, 440) },
				{ (SprocVerb.Update, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@CapturedBy), new SqlParameter("@CapturedBy", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@CapturedOrDerivedOn), new SqlParameter("@CapturedOrDerivedOn", SqlDbType.DateTime2, 7) },/* No TVPs */
				{ (SprocVerb.Update, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Delete, SprocParam.@PointCloudHUID), new SqlParameter("@PointCloudHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Delete, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } }

            };

            foreach( var p in prmSproc )
            {
                cmdSproc[p.Key.Item1].Parameters.Add(p.Value);
            }
        }

 		public DbSet<PointCloudMetadata> PointCloudMetadata { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			modelBuilder.Entity<PointCloudMetadata>().HasKey(c => new{ c.PointCloudHUID });
        }
    }
}
