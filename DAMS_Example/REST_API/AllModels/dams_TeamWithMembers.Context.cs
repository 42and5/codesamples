using Microsoft.EntityFrameworkCore;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace dams_TeamWithMembers_Api.Models
{
    public class TeamWithMembers_Context : DbContext
    {
        public Dictionary<SprocVerb, SqlCommand> cmdSproc;
        public Dictionary<(SprocVerb, SprocParam), SqlParameter> prmSproc;

        public enum SprocVerb{
            GetAll, 
            GetByKey, 
            Insert, 
            Update, 
            Delete
			,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete,
			TeamWithMembers_node_dams_TeamMemberDelete
        };
        public enum SprocParam{
            @ErrorNumber, 
            @ErrorMessage, 
            @retVal,
			@HumanAgentHUID,
			@OrganizationHUID,
			@TeamHUID,
			@TeamName, 
			@tvpdamsTeamMember
        };

        public TeamWithMembers_Context(DbContextOptions<TeamWithMembers_Context> options)
            : base(options)
        {

            SqlConnection cnx = (SqlConnection)this.Database.GetDbConnection();

            cmdSproc = new Dictionary<SprocVerb, SqlCommand>(){
				{ SprocVerb.GetAll, new SqlCommand("dams.TeamWithMembersGetAll", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.GetByKey, new SqlCommand("dams.TeamWithMembersGetByKey", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Insert, new SqlCommand("dams.TeamWithMembersInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Update, new SqlCommand("dams.TeamWithMembersUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Delete, new SqlCommand("dams.TeamWithMembersDelete", cnx){ CommandType = CommandType.StoredProcedure } }
				,
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } }
            };

            prmSproc = new Dictionary<(SprocVerb, SprocParam), SqlParameter>(){
				
				{ (SprocVerb.GetAll, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.GetByKey, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Insert, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@TeamName), new SqlParameter("@TeamName", SqlDbType.NVarChar, 128) },
				{ (SprocVerb.Insert, SprocParam.@OrganizationHUID), new SqlParameter("@OrganizationHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@tvpdamsTeamMember), new SqlParameter("@tvpdamsTeamMember", SqlDbType.Structured) },
				{ (SprocVerb.Insert, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Update, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@TeamName), new SqlParameter("@TeamName", SqlDbType.NVarChar, 128) },
				{ (SprocVerb.Update, SprocParam.@OrganizationHUID), new SqlParameter("@OrganizationHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@tvpdamsTeamMember), new SqlParameter("@tvpdamsTeamMember", SqlDbType.Structured) },
				{ (SprocVerb.Update, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Delete, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Delete, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } }

				,
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, SprocParam.@OrganizationHUID), new SqlParameter("@OrganizationHUID", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, SprocParam.@OrganizationHUID), new SqlParameter("@OrganizationHUID", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, SprocParam.@HumanAgentHUID), new SqlParameter("@HumanAgentHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } }
            };

            foreach( var p in prmSproc )
            {
                cmdSproc[p.Key.Item1].Parameters.Add(p.Value);
            }
        }

 		public DbSet<TeamWithMembers> TeamWithMembers { get; set; }
		public DbSet<TeamWithMembers_node_dams_TeamMember> TeamWithMembers_node_dams_TeamMember { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			modelBuilder.Entity<TeamWithMembers>().HasKey(c => new{ c.TeamHUID });
			modelBuilder.Entity<TeamWithMembers.dams_TeamMember>().HasKey(c => new{ c.HumanAgentHUID });
			modelBuilder.Entity<TeamWithMembers_node_dams_TeamMember>().HasKey(c => new{ c.HumanAgentHUID, c.TeamHUID });
        }
    }
}
