using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dams_HumanAgentWithTeamsAndRoles_Api.Models
{
	
	public class HumanAgentWithTeamsAndRoles
	{
		public class dams_RoleAssignment
		{
			
			[Required]
			public Guid HumanAgentHUID{ get; set; }
			[Required]
			public String RoleName{ get; set; }
			[Required]
			public DateTime2 GrantedOn{ get; set; }
			[Required]
			public Guid GrantedBy{ get; set; }
		
		
			public dams_RoleAssignment()
			{
			}
		}
		public class dams_TeamMember
		{
			
			[Required]
			public Guid TeamHUID{ get; set; }
			[Required]
			public Guid HumanAgentHUID{ get; set; }
			[Required]
			public Guid OrganizationHUID{ get; set; }
		
		
			public dams_TeamMember()
			{
			}
		}
		[Required]
		public Guid HumanAgentHUID{ get; set; }
	
		public List<dams_RoleAssignment> dams_RoleAssignment_Entries{ get; set; }
		public List<dams_TeamMember> dams_TeamMember_Entries{ get; set; }
	
		public HumanAgentWithTeamsAndRoles()
		{
			dams_RoleAssignment_Entries = new List<dams_RoleAssignment>();
			dams_TeamMember_Entries = new List<dams_TeamMember>();
		}
	}
	
	
	public class HumanAgentWithTeamsAndRoles_node_dams_RoleAssignment
	{
		
		[Required]
		public Guid HumanAgentHUID{ get; set; }
		[Required]
		public String RoleName{ get; set; }
		[Required]
		public DateTime2 GrantedOn{ get; set; }
		[Required]
		public Guid GrantedBy{ get; set; }
	
	
		public HumanAgentWithTeamsAndRoles_node_dams_RoleAssignment()
		{
		}
	}
	
	
	public class HumanAgentWithTeamsAndRoles_node_dams_TeamMember
	{
		
		[Required]
		public Guid TeamHUID{ get; set; }
		[Required]
		public Guid HumanAgentHUID{ get; set; }
		[Required]
		public Guid OrganizationHUID{ get; set; }
	
	
		public HumanAgentWithTeamsAndRoles_node_dams_TeamMember()
		{
		}
	}
	
}
