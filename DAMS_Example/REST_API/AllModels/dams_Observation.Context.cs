using Microsoft.EntityFrameworkCore;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace dams_Observation_Api.Models
{
    public class Observation_Context : DbContext
    {
        public Dictionary<SprocVerb, SqlCommand> cmdSproc;
        public Dictionary<(SprocVerb, SprocParam), SqlParameter> prmSproc;

        public enum SprocVerb{
            GetAll, 
            GetByKey, 
            Insert, 
            Update, 
            Delete
			,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberInsert,
			TeamWithMembers_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate,
			HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete,
			TeamWithMembers_node_dams_TeamMemberDelete
        };
        public enum SprocParam{
            @ErrorNumber, 
            @ErrorMessage, 
            @retVal,
			@ObservationMadeOn,
			@ObservationTypeCode,
			@ObservedBy,
			@PointCloudHUID,
			@TeamHUID,
			@Value
        };

        public Observation_Context(DbContextOptions<Observation_Context> options)
            : base(options)
        {

            SqlConnection cnx = (SqlConnection)this.Database.GetDbConnection();

            cmdSproc = new Dictionary<SprocVerb, SqlCommand>(){
				{ SprocVerb.GetAll, new SqlCommand("dams.ObservationGetAll", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.GetByKey, new SqlCommand("dams.ObservationGetByKey", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Insert, new SqlCommand("dams.ObservationInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Update, new SqlCommand("dams.ObservationUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.Delete, new SqlCommand("dams.ObservationDelete", cnx){ CommandType = CommandType.StoredProcedure } }
				,
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberInsert, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberInsert", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberUpdate, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete, new SqlCommand("dams.HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } },
				{ SprocVerb.TeamWithMembers_node_dams_TeamMemberDelete, new SqlCommand("dams.TeamWithMembers_node_dams_TeamMemberDelete", cnx){ CommandType = CommandType.StoredProcedure } }
            };

            prmSproc = new Dictionary<(SprocVerb, SprocParam), SqlParameter>(){
				
				{ (SprocVerb.GetAll, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetAll, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.GetByKey, SprocParam.@PointCloudHUID), new SqlParameter("@PointCloudHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.GetByKey, SprocParam.@ObservationTypeCode), new SqlParameter("@ObservationTypeCode", SqlDbType.VarChar, 64) },
				{ (SprocVerb.GetByKey, SprocParam.@ObservationMadeOn), new SqlParameter("@ObservationMadeOn", SqlDbType.DateTime2, 7) },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.GetByKey, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Insert, SprocParam.@PointCloudHUID), new SqlParameter("@PointCloudHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@ObservationTypeCode), new SqlParameter("@ObservationTypeCode", SqlDbType.VarChar, 64) },
				{ (SprocVerb.Insert, SprocParam.@ObservationMadeOn), new SqlParameter("@ObservationMadeOn", SqlDbType.DateTime2, 7) },
				{ (SprocVerb.Insert, SprocParam.@Value), new SqlParameter("@Value", SqlDbType.Decimal, 10, 0) },
				{ (SprocVerb.Insert, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Insert, SprocParam.@ObservedBy), new SqlParameter("@ObservedBy", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.Insert, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Insert, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Update, SprocParam.@PointCloudHUID), new SqlParameter("@PointCloudHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@ObservationTypeCode), new SqlParameter("@ObservationTypeCode", SqlDbType.VarChar, 64) },
				{ (SprocVerb.Update, SprocParam.@ObservationMadeOn), new SqlParameter("@ObservationMadeOn", SqlDbType.DateTime2, 7) },
				{ (SprocVerb.Update, SprocParam.@Value), new SqlParameter("@Value", SqlDbType.Decimal, 10, 0) },
				{ (SprocVerb.Update, SprocParam.@TeamHUID), new SqlParameter("@TeamHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Update, SprocParam.@ObservedBy), new SqlParameter("@ObservedBy", SqlDbType.UniqueIdentifier) },/* No TVPs */
				{ (SprocVerb.Update, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Update, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } },
				{ (SprocVerb.Delete, SprocParam.@PointCloudHUID), new SqlParameter("@PointCloudHUID", SqlDbType.UniqueIdentifier) },
				{ (SprocVerb.Delete, SprocParam.@ObservationTypeCode), new SqlParameter("@ObservationTypeCode", SqlDbType.VarChar, 64) },
				{ (SprocVerb.Delete, SprocParam.@ObservationMadeOn), new SqlParameter("@ObservationMadeOn", SqlDbType.DateTime2, 7) },
				{ (SprocVerb.Delete, SprocParam.@ErrorNumber), new SqlParameter("@ErrorNumber", SqlDbType.Int){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@ErrorMessage), new SqlParameter("@ErrorMessage", SqlDbType.NVarChar, 4000){ Direction = ParameterDirection.Output } },
				{ (SprocVerb.Delete, SprocParam.@retVal), new SqlParameter("@retVal", SqlDbType.Int){ Direction = ParameterDirection.ReturnValue } }

            };

            foreach( var p in prmSproc )
            {
                cmdSproc[p.Key.Item1].Parameters.Add(p.Value);
            }
        }

 		public DbSet<Observation> Observation { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			modelBuilder.Entity<Observation>().HasKey(c => new{ c.ObservationMadeOn, c.ObservationTypeCode, c.PointCloudHUID });
        }
    }
}
