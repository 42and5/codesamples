using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dams_PointCloudMetadata_Api.Models
{
	
	public class PointCloudMetadata
	{
		
		[Required]
		public Guid PointCloudHUID{ get; set; }
		public Guid SubspaceOfPointCloud{ get; set; }
		[Required]
		public Binary Thumbnail{ get; set; }
		[Required]
		public String PhysicalDescription{ get; set; }
		[Required]
		public String RepresentationCode{ get; set; }
		[Required]
		public String CaptureMetadata{ get; set; }
		[Required]
		public String RawDataURI{ get; set; }
		[Required]
		public Guid TeamHUID{ get; set; }
		[Required]
		public Guid CapturedBy{ get; set; }
		[Required]
		public DateTime2 CapturedOrDerivedOn{ get; set; }
	
	
		public PointCloudMetadata()
		{
		}
	}
	
}
