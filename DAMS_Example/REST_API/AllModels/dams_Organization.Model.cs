using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dams_Organization_Api.Models
{
	
	public class Organization
	{
		
		[Required]
		public Guid OrganizationHUID{ get; set; }
		[Required]
		public String OrganizationName{ get; set; }
		[Required]
		public String PrimaryIndustryCode{ get; set; }
	
	
		public Organization()
		{
		}
	}
	
}
