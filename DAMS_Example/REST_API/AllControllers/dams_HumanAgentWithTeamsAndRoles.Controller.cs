using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using dams_HumanAgentWithTeamsAndRoles_Api.Models;
using static dams_HumanAgentWithTeamsAndRoles_Api.Models.HumanAgentWithTeamsAndRoles_Context;

namespace dams_HumanAgentWithTeamsAndRoles_Api.Controllers
{
    [ApiController]
    public class HumanAgentWithTeamsAndRoles_Controller : ControllerBase
    {
        private readonly HumanAgentWithTeamsAndRoles_Context _context;
		private DataTable _dtdamsRoleAssignment;
		private DataTable _dtdamsTeamMember;

        public HumanAgentWithTeamsAndRoles_Controller(HumanAgentWithTeamsAndRoles_Context context)
        {
            try
            {
                _context = context;
				_dtdamsRoleAssignment = new DataTable();
				_dtdamsRoleAssignment.Columns.Add(new DataColumn("HumanAgentHUID", typeof(Guid)));
				_dtdamsRoleAssignment.Columns.Add(new DataColumn("RoleName", typeof(String)));
				_dtdamsRoleAssignment.Columns.Add(new DataColumn("GrantedOn", typeof(DateTime2)));
				_dtdamsRoleAssignment.Columns.Add(new DataColumn("GrantedBy", typeof(Guid)));
				_dtdamsTeamMember = new DataTable();
				_dtdamsTeamMember.Columns.Add(new DataColumn("TeamHUID", typeof(Guid)));
				_dtdamsTeamMember.Columns.Add(new DataColumn("HumanAgentHUID", typeof(Guid)));
				_dtdamsTeamMember.Columns.Add(new DataColumn("OrganizationHUID", typeof(Guid)));
            }
            catch(Exception e)
            {
                // Log e.Message?
                Debug.WriteLine(e.Message);
            }
        }

        // GET
        [HttpGet]
        [Route("api/humanagentwithteamsandroles")]
        public async Task<ActionResult<IEnumerable<HumanAgentWithTeamsAndRoles>>> GetHumanAgentWithTeamsAndRoles(Guid? HumanAgentHUID)
        {
            List<HumanAgentWithTeamsAndRoles> result = new List<HumanAgentWithTeamsAndRoles>();
            SqlCommand command;

            try
            {   
                if( HumanAgentHUID == null )
				{
                    command = _context.cmdSproc[SprocVerb.GetAll];
                } else {
					command = _context.cmdSproc[SprocVerb.GetByKey];
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@HumanAgentHUID)].Value = HumanAgentHUID;
                }

                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    // First result: HumanAgentWithTeamsAndRoles (base entity)
                    while (await reader.ReadAsync())
                    {
                        result.Add(
                            new HumanAgentWithTeamsAndRoles{
								HumanAgentHUID = (Guid)reader["HumanAgentHUID"]
                            }
                        );
                    }
					// Next result: dams_RoleAssignment
					await reader.NextResultAsync();
					while( await reader.ReadAsync() )
					{
						var relevantResultEntry = result.Find( e => e.HumanAgentHUID == (Guid)reader["HumanAgentHUID"] );
						relevantResultEntry.dams_RoleAssignment_Entries.Add(
							new HumanAgentWithTeamsAndRoles.dams_RoleAssignment(){
								GrantedBy = (Guid)reader["GrantedBy"],
								GrantedOn = (DateTime2)reader["GrantedOn"],
								RoleName = (String)reader["RoleName"]
							}
						);
					}
					// Next result: dams_TeamMember
					await reader.NextResultAsync();
					while( await reader.ReadAsync() )
					{
						var relevantResultEntry = result.Find( e => e.HumanAgentHUID == (Guid)reader["HumanAgentHUID"] );
						relevantResultEntry.dams_TeamMember_Entries.Add(
							new HumanAgentWithTeamsAndRoles.dams_TeamMember(){
								OrganizationHUID = (Guid)reader["OrganizationHUID"]
							}
						);
					}
                }
                var retVal = (int)command.Parameters["@retVal"].Value;
                command.Connection.Close();
                if( retVal != 0 )
                {
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better return for non-zero retVal
                }   
                if (result.Count == 0 && !(HumanAgentHUID == null))
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message. (Contact site administrator)");
            }

            return result;
        }


        /// TODO: Automated docs here? 
        [HttpPost]
        [Route("api/humanagentwithteamsandroles")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<HumanAgentWithTeamsAndRoles>> PostHumanAgentWithTeamsAndRoles(HumanAgentWithTeamsAndRoles humanAgentWithTeamsAndRoles)
        {

            if( !ModelState.IsValid )
            {
                return BadRequest();
            }

            try
            {
                _context.HumanAgentWithTeamsAndRoles.Add(humanAgentWithTeamsAndRoles);
				
				foreach( var dams_RoleAssignment_Entry in humanAgentWithTeamsAndRoles.dams_RoleAssignment_Entries )
				{
					_dtdamsRoleAssignment.Rows.Add(
						humanAgentWithTeamsAndRoles.HumanAgentHUID,
						dams_RoleAssignment_Entry.RoleName,
						dams_RoleAssignment_Entry.GrantedOn,
						dams_RoleAssignment_Entry.GrantedBy
					);
				
				}
				
				foreach( var dams_TeamMember_Entry in humanAgentWithTeamsAndRoles.dams_TeamMember_Entries )
				{
					_dtdamsTeamMember.Rows.Add(
						humanAgentWithTeamsAndRoles.TeamHUID,
						humanAgentWithTeamsAndRoles.HumanAgentHUID,
						dams_TeamMember_Entry.OrganizationHUID
					);
				
				}
				

				_context.prmSproc[(SprocVerb.Insert, SprocParam.@HumanAgentHUID)].Value = humanAgentWithTeamsAndRoles.HumanAgentHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@tvpdamsRoleAssignment)].Value = _dtdamsRoleAssignment;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@tvpdamsTeamMember)].Value = _dtdamsTeamMember;

                _context.cmdSproc[SprocVerb.Insert].Connection.Open();
                await _context.cmdSproc[SprocVerb.Insert].ExecuteNonQueryAsync();
                    
                var retVal = (int)_context.prmSproc[(SprocVerb.Insert, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Insert].Connection.Close();
    
                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Insert];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (POST). (Contact site administrator)");
            }

            // TODO: Better way to get json block of current state (try to avoid full db read)
            return CreatedAtAction("GetHumanAgentWithTeamsAndRoles", new { HumanAgentHUID = humanAgentWithTeamsAndRoles.HumanAgentHUID }, humanAgentWithTeamsAndRoles);
        }

        // PUT
        [HttpPut]//("{ id }")]
        [Route("api/humanagentwithteamsandroles")]
        public async Task<IActionResult> PutHumanAgentWithTeamsAndRoles(Guid HumanAgentHUID, HumanAgentWithTeamsAndRoles humanAgentWithTeamsAndRoles)
        {
            if ( (HumanAgentHUID == null) || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _context.Entry(humanAgentWithTeamsAndRoles).State = EntityState.Modified;

				foreach( var dams_RoleAssignment_Entry in humanAgentWithTeamsAndRoles.dams_RoleAssignment_Entries )
				{
					_dtdamsRoleAssignment.Rows.Add(
						humanAgentWithTeamsAndRoles.HumanAgentHUID,
						dams_RoleAssignment_Entry.RoleName,
						dams_RoleAssignment_Entry.GrantedOn,
						dams_RoleAssignment_Entry.GrantedBy
					);
				
				}
				
				foreach( var dams_TeamMember_Entry in humanAgentWithTeamsAndRoles.dams_TeamMember_Entries )
				{
					_dtdamsTeamMember.Rows.Add(
						humanAgentWithTeamsAndRoles.TeamHUID,
						humanAgentWithTeamsAndRoles.HumanAgentHUID,
						dams_TeamMember_Entry.OrganizationHUID
					);
				
				}
				
				
				_context.prmSproc[(SprocVerb.Update, SprocParam.@HumanAgentHUID)].Value = humanAgentWithTeamsAndRoles.HumanAgentHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@tvpdamsRoleAssignment)].Value = _dtdamsRoleAssignment;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@tvpdamsTeamMember)].Value = _dtdamsTeamMember;

                _context.cmdSproc[SprocVerb.Update].Connection.Open();
                await _context.cmdSproc[SprocVerb.Update].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Update, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Update].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Update];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (PUT). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }

        // DELETE
        [HttpDelete]//("{ id }")]
        [Route("api/humanagentwithteamsandroles")]
        public async Task<ActionResult<HumanAgentWithTeamsAndRoles>> DeleteHumanAgentWithTeamsAndRoles(Guid HumanAgentHUID)
        {
            try
            {
 				_context.prmSproc[(SprocVerb.Delete, SprocParam.@HumanAgentHUID)].Value = HumanAgentHUID;

                _context.cmdSproc[SprocVerb.Delete].Connection.Open();
                await _context.cmdSproc[SprocVerb.Delete].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Delete, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Delete].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Delete];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                 // TODO Better status
                return NotFound("TODO: Appropriate public message (Todo DELETE). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }
        
    }
}

