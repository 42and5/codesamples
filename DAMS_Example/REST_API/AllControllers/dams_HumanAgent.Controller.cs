using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using dams_HumanAgent_Api.Models;
using static dams_HumanAgent_Api.Models.HumanAgent_Context;

namespace dams_HumanAgent_Api.Controllers
{
    [ApiController]
    public class HumanAgent_Controller : ControllerBase
    {
        private readonly HumanAgent_Context _context;
		/* (No Datatables) */

        public HumanAgent_Controller(HumanAgent_Context context)
        {
            try
            {
                _context = context;
				/* (No Datatables) */
            }
            catch(Exception e)
            {
                // Log e.Message?
                Debug.WriteLine(e.Message);
            }
        }

        // GET
        [HttpGet]
        [Route("api/humanagent")]
        public async Task<ActionResult<IEnumerable<HumanAgent>>> GetHumanAgent(Guid? HumanAgentHUID)
        {
            List<HumanAgent> result = new List<HumanAgent>();
            SqlCommand command;

            try
            {   
                if( HumanAgentHUID == null )
				{
                    command = _context.cmdSproc[SprocVerb.GetAll];
                } else {
					command = _context.cmdSproc[SprocVerb.GetByKey];
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@HumanAgentHUID)].Value = HumanAgentHUID;
                }

                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    // First result: HumanAgent (base entity)
                    while (await reader.ReadAsync())
                    {
                        result.Add(
                            new HumanAgent{
								GivenName = (String)reader["GivenName"],
								HumanAgentExternalIdentifier = (String)reader["HumanAgentExternalIdentifier"],
								HumanAgentHUID = (Guid)reader["HumanAgentHUID"],
								OrganizationHUID = (Guid)reader["OrganizationHUID"],
								PrimaryEmail = (String)reader["PrimaryEmail"],
								Surname = (String)reader["Surname"]
                            }
                        );
                    }
					{ChildNodeReaders}
                }
                var retVal = (int)command.Parameters["@retVal"].Value;
                command.Connection.Close();
                if( retVal != 0 )
                {
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better return for non-zero retVal
                }   
                if (result.Count == 0 && !(HumanAgentHUID == null))
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message. (Contact site administrator)");
            }

            return result;
        }


        /// TODO: Automated docs here? 
        [HttpPost]
        [Route("api/humanagent")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<HumanAgent>> PostHumanAgent(HumanAgent humanAgent)
        {

            if( !ModelState.IsValid )
            {
                return BadRequest();
            }

            try
            {
                _context.HumanAgent.Add(humanAgent);
				
				{ChildNodeAssignments}

				_context.prmSproc[(SprocVerb.Insert, SprocParam.@HumanAgentHUID)].Value = humanAgent.HumanAgentHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@OrganizationHUID)].Value = humanAgent.OrganizationHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@PrimaryEmail)].Value = humanAgent.PrimaryEmail;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@HumanAgentExternalIdentifier)].Value = humanAgent.HumanAgentExternalIdentifier;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@GivenName)].Value = humanAgent.GivenName;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@Surname)].Value = humanAgent.Surname;/* No TVPs */

                _context.cmdSproc[SprocVerb.Insert].Connection.Open();
                await _context.cmdSproc[SprocVerb.Insert].ExecuteNonQueryAsync();
                    
                var retVal = (int)_context.prmSproc[(SprocVerb.Insert, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Insert].Connection.Close();
    
                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Insert];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (POST). (Contact site administrator)");
            }

            // TODO: Better way to get json block of current state (try to avoid full db read)
            return CreatedAtAction("GetHumanAgent", new { HumanAgentHUID = humanAgent.HumanAgentHUID }, humanAgent);
        }

        // PUT
        [HttpPut]//("{ id }")]
        [Route("api/humanagent")]
        public async Task<IActionResult> PutHumanAgent(Guid HumanAgentHUID, HumanAgent humanAgent)
        {
            if ( (HumanAgentHUID == null) || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _context.Entry(humanAgent).State = EntityState.Modified;

				{ChildNodeAssignments}
				
				_context.prmSproc[(SprocVerb.Update, SprocParam.@HumanAgentHUID)].Value = humanAgent.HumanAgentHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@OrganizationHUID)].Value = humanAgent.OrganizationHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@PrimaryEmail)].Value = humanAgent.PrimaryEmail;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@HumanAgentExternalIdentifier)].Value = humanAgent.HumanAgentExternalIdentifier;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@GivenName)].Value = humanAgent.GivenName;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@Surname)].Value = humanAgent.Surname;/* No TVPs */

                _context.cmdSproc[SprocVerb.Update].Connection.Open();
                await _context.cmdSproc[SprocVerb.Update].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Update, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Update].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Update];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (PUT). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }

        // DELETE
        [HttpDelete]//("{ id }")]
        [Route("api/humanagent")]
        public async Task<ActionResult<HumanAgent>> DeleteHumanAgent(Guid HumanAgentHUID)
        {
            try
            {
 				_context.prmSproc[(SprocVerb.Delete, SprocParam.@HumanAgentHUID)].Value = HumanAgentHUID;

                _context.cmdSproc[SprocVerb.Delete].Connection.Open();
                await _context.cmdSproc[SprocVerb.Delete].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Delete, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Delete].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Delete];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                 // TODO Better status
                return NotFound("TODO: Appropriate public message (Todo DELETE). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }
        
    }
}

