using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using dams_Team_Api.Models;
using static dams_Team_Api.Models.Team_Context;

namespace dams_Team_Api.Controllers
{
    [ApiController]
    public class Team_Controller : ControllerBase
    {
        private readonly Team_Context _context;
		/* (No Datatables) */

        public Team_Controller(Team_Context context)
        {
            try
            {
                _context = context;
				/* (No Datatables) */
            }
            catch(Exception e)
            {
                // Log e.Message?
                Debug.WriteLine(e.Message);
            }
        }

        // GET
        [HttpGet]
        [Route("api/team")]
        public async Task<ActionResult<IEnumerable<Team>>> GetTeam(Guid? TeamHUID)
        {
            List<Team> result = new List<Team>();
            SqlCommand command;

            try
            {   
                if( TeamHUID == null )
				{
                    command = _context.cmdSproc[SprocVerb.GetAll];
                } else {
					command = _context.cmdSproc[SprocVerb.GetByKey];
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@TeamHUID)].Value = TeamHUID;
                }

                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    // First result: Team (base entity)
                    while (await reader.ReadAsync())
                    {
                        result.Add(
                            new Team{
								OrganizationHUID = (Guid)reader["OrganizationHUID"],
								TeamHUID = (Guid)reader["TeamHUID"],
								TeamName = (String)reader["TeamName"]
                            }
                        );
                    }
					{ChildNodeReaders}
                }
                var retVal = (int)command.Parameters["@retVal"].Value;
                command.Connection.Close();
                if( retVal != 0 )
                {
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better return for non-zero retVal
                }   
                if (result.Count == 0 && !(TeamHUID == null))
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message. (Contact site administrator)");
            }

            return result;
        }


        /// TODO: Automated docs here? 
        [HttpPost]
        [Route("api/team")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<Team>> PostTeam(Team team)
        {

            if( !ModelState.IsValid )
            {
                return BadRequest();
            }

            try
            {
                _context.Team.Add(team);
				
				{ChildNodeAssignments}

				_context.prmSproc[(SprocVerb.Insert, SprocParam.@TeamHUID)].Value = team.TeamHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@TeamName)].Value = team.TeamName;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@OrganizationHUID)].Value = team.OrganizationHUID;/* No TVPs */

                _context.cmdSproc[SprocVerb.Insert].Connection.Open();
                await _context.cmdSproc[SprocVerb.Insert].ExecuteNonQueryAsync();
                    
                var retVal = (int)_context.prmSproc[(SprocVerb.Insert, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Insert].Connection.Close();
    
                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Insert];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (POST). (Contact site administrator)");
            }

            // TODO: Better way to get json block of current state (try to avoid full db read)
            return CreatedAtAction("GetTeam", new { TeamHUID = team.TeamHUID }, team);
        }

        // PUT
        [HttpPut]//("{ id }")]
        [Route("api/team")]
        public async Task<IActionResult> PutTeam(Guid TeamHUID, Team team)
        {
            if ( (TeamHUID == null) || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _context.Entry(team).State = EntityState.Modified;

				{ChildNodeAssignments}
				
				_context.prmSproc[(SprocVerb.Update, SprocParam.@TeamHUID)].Value = team.TeamHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@TeamName)].Value = team.TeamName;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@OrganizationHUID)].Value = team.OrganizationHUID;/* No TVPs */

                _context.cmdSproc[SprocVerb.Update].Connection.Open();
                await _context.cmdSproc[SprocVerb.Update].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Update, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Update].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Update];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (PUT). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }

        // DELETE
        [HttpDelete]//("{ id }")]
        [Route("api/team")]
        public async Task<ActionResult<Team>> DeleteTeam(Guid TeamHUID)
        {
            try
            {
 				_context.prmSproc[(SprocVerb.Delete, SprocParam.@TeamHUID)].Value = TeamHUID;

                _context.cmdSproc[SprocVerb.Delete].Connection.Open();
                await _context.cmdSproc[SprocVerb.Delete].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Delete, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Delete].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Delete];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                 // TODO Better status
                return NotFound("TODO: Appropriate public message (Todo DELETE). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }
        
    }
}

