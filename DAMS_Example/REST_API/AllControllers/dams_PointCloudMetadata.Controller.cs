using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using dams_PointCloudMetadata_Api.Models;
using static dams_PointCloudMetadata_Api.Models.PointCloudMetadata_Context;

namespace dams_PointCloudMetadata_Api.Controllers
{
    [ApiController]
    public class PointCloudMetadata_Controller : ControllerBase
    {
        private readonly PointCloudMetadata_Context _context;
		/* (No Datatables) */

        public PointCloudMetadata_Controller(PointCloudMetadata_Context context)
        {
            try
            {
                _context = context;
				/* (No Datatables) */
            }
            catch(Exception e)
            {
                // Log e.Message?
                Debug.WriteLine(e.Message);
            }
        }

        // GET
        [HttpGet]
        [Route("api/pointcloudmetadata")]
        public async Task<ActionResult<IEnumerable<PointCloudMetadata>>> GetPointCloudMetadata(Guid? PointCloudHUID)
        {
            List<PointCloudMetadata> result = new List<PointCloudMetadata>();
            SqlCommand command;

            try
            {   
                if( PointCloudHUID == null )
				{
                    command = _context.cmdSproc[SprocVerb.GetAll];
                } else {
					command = _context.cmdSproc[SprocVerb.GetByKey];
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@PointCloudHUID)].Value = PointCloudHUID;
                }

                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    // First result: PointCloudMetadata (base entity)
                    while (await reader.ReadAsync())
                    {
                        result.Add(
                            new PointCloudMetadata{
								CapturedBy = (Guid)reader["CapturedBy"],
								CapturedOrDerivedOn = (DateTime2)reader["CapturedOrDerivedOn"],
								CaptureMetadata = (String)reader["CaptureMetadata"],
								PhysicalDescription = (String)reader["PhysicalDescription"],
								PointCloudHUID = (Guid)reader["PointCloudHUID"],
								RawDataURI = (String)reader["RawDataURI"],
								RepresentationCode = (String)reader["RepresentationCode"],
								SubspaceOfPointCloud = (Guid)reader["SubspaceOfPointCloud"],
								TeamHUID = (Guid)reader["TeamHUID"],
								Thumbnail = (Binary)reader["Thumbnail"]
                            }
                        );
                    }
					{ChildNodeReaders}
                }
                var retVal = (int)command.Parameters["@retVal"].Value;
                command.Connection.Close();
                if( retVal != 0 )
                {
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better return for non-zero retVal
                }   
                if (result.Count == 0 && !(PointCloudHUID == null))
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message. (Contact site administrator)");
            }

            return result;
        }


        /// TODO: Automated docs here? 
        [HttpPost]
        [Route("api/pointcloudmetadata")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<PointCloudMetadata>> PostPointCloudMetadata(PointCloudMetadata pointCloudMetadata)
        {

            if( !ModelState.IsValid )
            {
                return BadRequest();
            }

            try
            {
                _context.PointCloudMetadata.Add(pointCloudMetadata);
				
				{ChildNodeAssignments}

				_context.prmSproc[(SprocVerb.Insert, SprocParam.@PointCloudHUID)].Value = pointCloudMetadata.PointCloudHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@SubspaceOfPointCloud)].Value = pointCloudMetadata.SubspaceOfPointCloud;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@Thumbnail)].Value = pointCloudMetadata.Thumbnail;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@PhysicalDescription)].Value = pointCloudMetadata.PhysicalDescription;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@RepresentationCode)].Value = pointCloudMetadata.RepresentationCode;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@CaptureMetadata)].Value = pointCloudMetadata.CaptureMetadata;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@RawDataURI)].Value = pointCloudMetadata.RawDataURI;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@TeamHUID)].Value = pointCloudMetadata.TeamHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@CapturedBy)].Value = pointCloudMetadata.CapturedBy;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@CapturedOrDerivedOn)].Value = pointCloudMetadata.CapturedOrDerivedOn;/* No TVPs */

                _context.cmdSproc[SprocVerb.Insert].Connection.Open();
                await _context.cmdSproc[SprocVerb.Insert].ExecuteNonQueryAsync();
                    
                var retVal = (int)_context.prmSproc[(SprocVerb.Insert, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Insert].Connection.Close();
    
                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Insert];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (POST). (Contact site administrator)");
            }

            // TODO: Better way to get json block of current state (try to avoid full db read)
            return CreatedAtAction("GetPointCloudMetadata", new { PointCloudHUID = pointCloudMetadata.PointCloudHUID }, pointCloudMetadata);
        }

        // PUT
        [HttpPut]//("{ id }")]
        [Route("api/pointcloudmetadata")]
        public async Task<IActionResult> PutPointCloudMetadata(Guid PointCloudHUID, PointCloudMetadata pointCloudMetadata)
        {
            if ( (PointCloudHUID == null) || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _context.Entry(pointCloudMetadata).State = EntityState.Modified;

				{ChildNodeAssignments}
				
				_context.prmSproc[(SprocVerb.Update, SprocParam.@PointCloudHUID)].Value = pointCloudMetadata.PointCloudHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@SubspaceOfPointCloud)].Value = pointCloudMetadata.SubspaceOfPointCloud;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@Thumbnail)].Value = pointCloudMetadata.Thumbnail;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@PhysicalDescription)].Value = pointCloudMetadata.PhysicalDescription;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@RepresentationCode)].Value = pointCloudMetadata.RepresentationCode;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@CaptureMetadata)].Value = pointCloudMetadata.CaptureMetadata;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@RawDataURI)].Value = pointCloudMetadata.RawDataURI;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@TeamHUID)].Value = pointCloudMetadata.TeamHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@CapturedBy)].Value = pointCloudMetadata.CapturedBy;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@CapturedOrDerivedOn)].Value = pointCloudMetadata.CapturedOrDerivedOn;/* No TVPs */

                _context.cmdSproc[SprocVerb.Update].Connection.Open();
                await _context.cmdSproc[SprocVerb.Update].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Update, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Update].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Update];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (PUT). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }

        // DELETE
        [HttpDelete]//("{ id }")]
        [Route("api/pointcloudmetadata")]
        public async Task<ActionResult<PointCloudMetadata>> DeletePointCloudMetadata(Guid PointCloudHUID)
        {
            try
            {
 				_context.prmSproc[(SprocVerb.Delete, SprocParam.@PointCloudHUID)].Value = PointCloudHUID;

                _context.cmdSproc[SprocVerb.Delete].Connection.Open();
                await _context.cmdSproc[SprocVerb.Delete].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Delete, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Delete].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Delete];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                 // TODO Better status
                return NotFound("TODO: Appropriate public message (Todo DELETE). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }
        
    }
}

