using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using dams_Observation_Api.Models;
using static dams_Observation_Api.Models.Observation_Context;

namespace dams_Observation_Api.Controllers
{
    [ApiController]
    public class Observation_Controller : ControllerBase
    {
        private readonly Observation_Context _context;
		/* (No Datatables) */

        public Observation_Controller(Observation_Context context)
        {
            try
            {
                _context = context;
				/* (No Datatables) */
            }
            catch(Exception e)
            {
                // Log e.Message?
                Debug.WriteLine(e.Message);
            }
        }

        // GET
        [HttpGet]
        [Route("api/observation")]
        public async Task<ActionResult<IEnumerable<Observation>>> GetObservation(Guid? PointCloudHUID, String? ObservationTypeCode, DateTime2? ObservationMadeOn)
        {
            List<Observation> result = new List<Observation>();
            SqlCommand command;

            try
            {   
                if( PointCloudHUID == null OR ObservationTypeCode == null OR ObservationMadeOn == null )
				{
                    command = _context.cmdSproc[SprocVerb.GetAll];
                } else {
					command = _context.cmdSproc[SprocVerb.GetByKey];
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@PointCloudHUID)].Value = PointCloudHUID;
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@ObservationTypeCode)].Value = ObservationTypeCode;
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@ObservationMadeOn)].Value = ObservationMadeOn;
                }

                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    // First result: Observation (base entity)
                    while (await reader.ReadAsync())
                    {
                        result.Add(
                            new Observation{
								ObservationMadeOn = (DateTime2)reader["ObservationMadeOn"],
								ObservationTypeCode = (String)reader["ObservationTypeCode"],
								ObservedBy = (Guid)reader["ObservedBy"],
								PointCloudHUID = (Guid)reader["PointCloudHUID"],
								TeamHUID = (Guid)reader["TeamHUID"],
								Value = (Decimal)reader["Value"]
                            }
                        );
                    }
					{ChildNodeReaders}
                }
                var retVal = (int)command.Parameters["@retVal"].Value;
                command.Connection.Close();
                if( retVal != 0 )
                {
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better return for non-zero retVal
                }   
                if (result.Count == 0 && !(PointCloudHUID == null OR ObservationTypeCode == null OR ObservationMadeOn == null))
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message. (Contact site administrator)");
            }

            return result;
        }


        /// TODO: Automated docs here? 
        [HttpPost]
        [Route("api/observation")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<Observation>> PostObservation(Observation observation)
        {

            if( !ModelState.IsValid )
            {
                return BadRequest();
            }

            try
            {
                _context.Observation.Add(observation);
				
				{ChildNodeAssignments}

				_context.prmSproc[(SprocVerb.Insert, SprocParam.@PointCloudHUID)].Value = observation.PointCloudHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@ObservationTypeCode)].Value = observation.ObservationTypeCode;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@ObservationMadeOn)].Value = observation.ObservationMadeOn;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@Value)].Value = observation.Value;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@TeamHUID)].Value = observation.TeamHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@ObservedBy)].Value = observation.ObservedBy;/* No TVPs */

                _context.cmdSproc[SprocVerb.Insert].Connection.Open();
                await _context.cmdSproc[SprocVerb.Insert].ExecuteNonQueryAsync();
                    
                var retVal = (int)_context.prmSproc[(SprocVerb.Insert, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Insert].Connection.Close();
    
                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Insert];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (POST). (Contact site administrator)");
            }

            // TODO: Better way to get json block of current state (try to avoid full db read)
            return CreatedAtAction("GetObservation", new { PointCloudHUID = observation.PointCloudHUID, ObservationTypeCode = observation.ObservationTypeCode, ObservationMadeOn = observation.ObservationMadeOn }, observation);
        }

        // PUT
        [HttpPut]//("{ id }")]
        [Route("api/observation")]
        public async Task<IActionResult> PutObservation(Guid PointCloudHUID, String ObservationTypeCode, DateTime2 ObservationMadeOn, Observation observation)
        {
            if ( (PointCloudHUID == null OR ObservationTypeCode == null OR ObservationMadeOn == null) || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _context.Entry(observation).State = EntityState.Modified;

				{ChildNodeAssignments}
				
				_context.prmSproc[(SprocVerb.Update, SprocParam.@PointCloudHUID)].Value = observation.PointCloudHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@ObservationTypeCode)].Value = observation.ObservationTypeCode;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@ObservationMadeOn)].Value = observation.ObservationMadeOn;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@Value)].Value = observation.Value;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@TeamHUID)].Value = observation.TeamHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@ObservedBy)].Value = observation.ObservedBy;/* No TVPs */

                _context.cmdSproc[SprocVerb.Update].Connection.Open();
                await _context.cmdSproc[SprocVerb.Update].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Update, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Update].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Update];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (PUT). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }

        // DELETE
        [HttpDelete]//("{ id }")]
        [Route("api/observation")]
        public async Task<ActionResult<Observation>> DeleteObservation(Guid PointCloudHUID, String ObservationTypeCode, DateTime2 ObservationMadeOn)
        {
            try
            {
 				_context.prmSproc[(SprocVerb.Delete, SprocParam.@PointCloudHUID)].Value = PointCloudHUID;
				_context.prmSproc[(SprocVerb.Delete, SprocParam.@ObservationTypeCode)].Value = ObservationTypeCode;
				_context.prmSproc[(SprocVerb.Delete, SprocParam.@ObservationMadeOn)].Value = ObservationMadeOn;

                _context.cmdSproc[SprocVerb.Delete].Connection.Open();
                await _context.cmdSproc[SprocVerb.Delete].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Delete, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Delete].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Delete];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                 // TODO Better status
                return NotFound("TODO: Appropriate public message (Todo DELETE). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }
        
    }
}

