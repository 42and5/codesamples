using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using dams_Organization_Api.Models;
using static dams_Organization_Api.Models.Organization_Context;

namespace dams_Organization_Api.Controllers
{
    [ApiController]
    public class Organization_Controller : ControllerBase
    {
        private readonly Organization_Context _context;
		/* (No Datatables) */

        public Organization_Controller(Organization_Context context)
        {
            try
            {
                _context = context;
				/* (No Datatables) */
            }
            catch(Exception e)
            {
                // Log e.Message?
                Debug.WriteLine(e.Message);
            }
        }

        // GET
        [HttpGet]
        [Route("api/organization")]
        public async Task<ActionResult<IEnumerable<Organization>>> GetOrganization(Guid? OrganizationHUID)
        {
            List<Organization> result = new List<Organization>();
            SqlCommand command;

            try
            {   
                if( OrganizationHUID == null )
				{
                    command = _context.cmdSproc[SprocVerb.GetAll];
                } else {
					command = _context.cmdSproc[SprocVerb.GetByKey];
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@OrganizationHUID)].Value = OrganizationHUID;
                }

                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    // First result: Organization (base entity)
                    while (await reader.ReadAsync())
                    {
                        result.Add(
                            new Organization{
								OrganizationHUID = (Guid)reader["OrganizationHUID"],
								OrganizationName = (String)reader["OrganizationName"],
								PrimaryIndustryCode = (String)reader["PrimaryIndustryCode"]
                            }
                        );
                    }
					{ChildNodeReaders}
                }
                var retVal = (int)command.Parameters["@retVal"].Value;
                command.Connection.Close();
                if( retVal != 0 )
                {
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better return for non-zero retVal
                }   
                if (result.Count == 0 && !(OrganizationHUID == null))
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message. (Contact site administrator)");
            }

            return result;
        }


        /// TODO: Automated docs here? 
        [HttpPost]
        [Route("api/organization")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<Organization>> PostOrganization(Organization organization)
        {

            if( !ModelState.IsValid )
            {
                return BadRequest();
            }

            try
            {
                _context.Organization.Add(organization);
				
				{ChildNodeAssignments}

				_context.prmSproc[(SprocVerb.Insert, SprocParam.@OrganizationHUID)].Value = organization.OrganizationHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@OrganizationName)].Value = organization.OrganizationName;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@PrimaryIndustryCode)].Value = organization.PrimaryIndustryCode;/* No TVPs */

                _context.cmdSproc[SprocVerb.Insert].Connection.Open();
                await _context.cmdSproc[SprocVerb.Insert].ExecuteNonQueryAsync();
                    
                var retVal = (int)_context.prmSproc[(SprocVerb.Insert, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Insert].Connection.Close();
    
                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Insert];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (POST). (Contact site administrator)");
            }

            // TODO: Better way to get json block of current state (try to avoid full db read)
            return CreatedAtAction("GetOrganization", new { OrganizationHUID = organization.OrganizationHUID }, organization);
        }

        // PUT
        [HttpPut]//("{ id }")]
        [Route("api/organization")]
        public async Task<IActionResult> PutOrganization(Guid OrganizationHUID, Organization organization)
        {
            if ( (OrganizationHUID == null) || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _context.Entry(organization).State = EntityState.Modified;

				{ChildNodeAssignments}
				
				_context.prmSproc[(SprocVerb.Update, SprocParam.@OrganizationHUID)].Value = organization.OrganizationHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@OrganizationName)].Value = organization.OrganizationName;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@PrimaryIndustryCode)].Value = organization.PrimaryIndustryCode;/* No TVPs */

                _context.cmdSproc[SprocVerb.Update].Connection.Open();
                await _context.cmdSproc[SprocVerb.Update].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Update, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Update].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Update];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (PUT). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }

        // DELETE
        [HttpDelete]//("{ id }")]
        [Route("api/organization")]
        public async Task<ActionResult<Organization>> DeleteOrganization(Guid OrganizationHUID)
        {
            try
            {
 				_context.prmSproc[(SprocVerb.Delete, SprocParam.@OrganizationHUID)].Value = OrganizationHUID;

                _context.cmdSproc[SprocVerb.Delete].Connection.Open();
                await _context.cmdSproc[SprocVerb.Delete].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Delete, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Delete].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Delete];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                 // TODO Better status
                return NotFound("TODO: Appropriate public message (Todo DELETE). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }
        
    }
}

