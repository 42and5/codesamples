using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using dams_TeamWithMembers_Api.Models;
using static dams_TeamWithMembers_Api.Models.TeamWithMembers_Context;

namespace dams_TeamWithMembers_Api.Controllers
{
    [ApiController]
    public class TeamWithMembers_Controller : ControllerBase
    {
        private readonly TeamWithMembers_Context _context;
		private DataTable _dtdamsTeamMember;

        public TeamWithMembers_Controller(TeamWithMembers_Context context)
        {
            try
            {
                _context = context;
				_dtdamsTeamMember = new DataTable();
				_dtdamsTeamMember.Columns.Add(new DataColumn("TeamHUID", typeof(Guid)));
				_dtdamsTeamMember.Columns.Add(new DataColumn("HumanAgentHUID", typeof(Guid)));
				_dtdamsTeamMember.Columns.Add(new DataColumn("OrganizationHUID", typeof(Guid)));
            }
            catch(Exception e)
            {
                // Log e.Message?
                Debug.WriteLine(e.Message);
            }
        }

        // GET
        [HttpGet]
        [Route("api/teamwithmembers")]
        public async Task<ActionResult<IEnumerable<TeamWithMembers>>> GetTeamWithMembers(Guid? TeamHUID)
        {
            List<TeamWithMembers> result = new List<TeamWithMembers>();
            SqlCommand command;

            try
            {   
                if( TeamHUID == null )
				{
                    command = _context.cmdSproc[SprocVerb.GetAll];
                } else {
					command = _context.cmdSproc[SprocVerb.GetByKey];
					_context.prmSproc[(SprocVerb.GetByKey, SprocParam.@TeamHUID)].Value = TeamHUID;
                }

                command.Connection.Open();
                using (var reader = await command.ExecuteReaderAsync())
                {
                    // First result: TeamWithMembers (base entity)
                    while (await reader.ReadAsync())
                    {
                        result.Add(
                            new TeamWithMembers{
								OrganizationHUID = (Guid)reader["OrganizationHUID"],
								TeamHUID = (Guid)reader["TeamHUID"],
								TeamName = (String)reader["TeamName"]
                            }
                        );
                    }
					// Next result: dams_TeamMember
					await reader.NextResultAsync();
					while( await reader.ReadAsync() )
					{
						var relevantResultEntry = result.Find( e => e.TeamHUID == (Guid)reader["TeamHUID"] );
						relevantResultEntry.dams_TeamMember_Entries.Add(
							new TeamWithMembers.dams_TeamMember(){
								OrganizationHUID = (Guid)reader["OrganizationHUID"]
							}
						);
					}
                }
                var retVal = (int)command.Parameters["@retVal"].Value;
                command.Connection.Close();
                if( retVal != 0 )
                {
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better return for non-zero retVal
                }   
                if (result.Count == 0 && !(TeamHUID == null))
                {
                    return NotFound();
                }
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message. (Contact site administrator)");
            }

            return result;
        }


        /// TODO: Automated docs here? 
        [HttpPost]
        [Route("api/teamwithmembers")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<TeamWithMembers>> PostTeamWithMembers(TeamWithMembers teamWithMembers)
        {

            if( !ModelState.IsValid )
            {
                return BadRequest();
            }

            try
            {
                _context.TeamWithMembers.Add(teamWithMembers);
				
				foreach( var dams_TeamMember_Entry in teamWithMembers.dams_TeamMember_Entries )
				{
					_dtdamsTeamMember.Rows.Add(
						teamWithMembers.TeamHUID,
						teamWithMembers.HumanAgentHUID,
						dams_TeamMember_Entry.OrganizationHUID
					);
				
				}
				

				_context.prmSproc[(SprocVerb.Insert, SprocParam.@TeamHUID)].Value = teamWithMembers.TeamHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@TeamName)].Value = teamWithMembers.TeamName;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@OrganizationHUID)].Value = teamWithMembers.OrganizationHUID;
				_context.prmSproc[(SprocVerb.Insert, SprocParam.@tvpdamsTeamMember)].Value = _dtdamsTeamMember;

                _context.cmdSproc[SprocVerb.Insert].Connection.Open();
                await _context.cmdSproc[SprocVerb.Insert].ExecuteNonQueryAsync();
                    
                var retVal = (int)_context.prmSproc[(SprocVerb.Insert, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Insert].Connection.Close();
    
                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Insert];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };
            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (POST). (Contact site administrator)");
            }

            // TODO: Better way to get json block of current state (try to avoid full db read)
            return CreatedAtAction("GetTeamWithMembers", new { TeamHUID = teamWithMembers.TeamHUID }, teamWithMembers);
        }

        // PUT
        [HttpPut]//("{ id }")]
        [Route("api/teamwithmembers")]
        public async Task<IActionResult> PutTeamWithMembers(Guid TeamHUID, TeamWithMembers teamWithMembers)
        {
            if ( (TeamHUID == null) || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                _context.Entry(teamWithMembers).State = EntityState.Modified;

				foreach( var dams_TeamMember_Entry in teamWithMembers.dams_TeamMember_Entries )
				{
					_dtdamsTeamMember.Rows.Add(
						teamWithMembers.TeamHUID,
						teamWithMembers.HumanAgentHUID,
						dams_TeamMember_Entry.OrganizationHUID
					);
				
				}
				
				
				_context.prmSproc[(SprocVerb.Update, SprocParam.@TeamHUID)].Value = teamWithMembers.TeamHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@TeamName)].Value = teamWithMembers.TeamName;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@OrganizationHUID)].Value = teamWithMembers.OrganizationHUID;
				_context.prmSproc[(SprocVerb.Update, SprocParam.@tvpdamsTeamMember)].Value = _dtdamsTeamMember;

                _context.cmdSproc[SprocVerb.Update].Connection.Open();
                await _context.cmdSproc[SprocVerb.Update].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Update, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Update].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Update];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                // TODO Better status
                return NotFound("TODO: Appropriate public message (PUT). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }

        // DELETE
        [HttpDelete]//("{ id }")]
        [Route("api/teamwithmembers")]
        public async Task<ActionResult<TeamWithMembers>> DeleteTeamWithMembers(Guid TeamHUID)
        {
            try
            {
 				_context.prmSproc[(SprocVerb.Delete, SprocParam.@TeamHUID)].Value = TeamHUID;

                _context.cmdSproc[SprocVerb.Delete].Connection.Open();
                await _context.cmdSproc[SprocVerb.Delete].ExecuteNonQueryAsync();
                    
                int retVal = (int)_context.prmSproc[(SprocVerb.Delete, SprocParam.@retVal)].Value;
                _context.cmdSproc[SprocVerb.Delete].Connection.Close();

                if( retVal != 0 )
                {
                    var command = _context.cmdSproc[SprocVerb.Delete];
                    Debug.WriteLine($"Proc {command.CommandText}, ErrorNumber {(int)command.Parameters["@ErrorNumber"].Value}, ErrorMessage {(string)command.Parameters["@ErrorMessage"].Value}");
                    return NotFound(); // TODO: Better error to return
                };

            }
            catch(Exception e)
            {
                // TODO: Log error as appropriate
                Debug.WriteLine(e.Message);
                 // TODO Better status
                return NotFound("TODO: Appropriate public message (Todo DELETE). (Contact site administrator)");
            }
            return NoContent(); // TODO: Return some type of "success" message?
        }
        
    }
}

