/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembersInsert] -- Hybrid
	@TeamHUID uniqueidentifier,
	@TeamName nvarchar(128),
	@OrganizationHUID uniqueidentifier
	,
	@tvpdamsTeamMember AS [dams].[tbTypeTeamWithMembers_TeamMember] READONLY
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[Team](
				[TeamHUID],
				[TeamName],
				[OrganizationHUID]
			)
			SELECT
				[src].[TeamHUID],
				[src].[TeamName],
				[src].[OrganizationHUID]
			FROM 
			(
				VALUES(
					@TeamHUID,
					@TeamName,
					@OrganizationHUID
				)
			) src(
				[TeamHUID],
				[TeamName],
				[OrganizationHUID]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

			INSERT INTO [dams].[TeamMember](
				[TeamHUID],
				[HumanAgentHUID],
				[OrganizationHUID]
			)
			SELECT
				[src].[TeamHUID],
				[src].[HumanAgentHUID],
				[src].[OrganizationHUID]
			FROM @tvpdamsTeamMember src
			;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRolesInsert] -- Hybrid
	@HumanAgentHUID uniqueidentifier
	,
	@tvpdamsRoleAssignment AS [dams].[tbTypeHumanAgentWithTeamsAndRoles_RoleAssignment] READONLY,
	@tvpdamsTeamMember AS [dams].[tbTypeHumanAgentWithTeamsAndRoles_TeamMember] READONLY
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[HumanAgent](
				[HumanAgentHUID]
			)
			SELECT
				[src].[HumanAgentHUID]
			FROM 
			(
				VALUES(
					@HumanAgentHUID
				)
			) src(
				[HumanAgentHUID]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

			INSERT INTO [dams].[RoleAssignment](
				[HumanAgentHUID],
				[RoleName],
				[GrantedOn],
				[GrantedBy]
			)
			SELECT
				[src].[HumanAgentHUID],
				[src].[RoleName],
				[src].[GrantedOn],
				[src].[GrantedBy]
			FROM @tvpdamsRoleAssignment src
			;
			
			INSERT INTO [dams].[TeamMember](
				[TeamHUID],
				[HumanAgentHUID],
				[OrganizationHUID]
			)
			SELECT
				[src].[TeamHUID],
				[src].[HumanAgentHUID],
				[src].[OrganizationHUID]
			FROM @tvpdamsTeamMember src
			;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert] -- Hybrid
	@HumanAgentHUID uniqueidentifier,
	@RoleName varchar(128),
	@GrantedOn datetime2(7),
	@GrantedBy uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[RoleAssignment](
				[HumanAgentHUID],
				[RoleName],
				[GrantedOn],
				[GrantedBy]
			)
			SELECT
				[src].[HumanAgentHUID],
				[src].[RoleName],
				[src].[GrantedOn],
				[src].[GrantedBy]
			FROM 
			(
				VALUES(
					@HumanAgentHUID,
					@RoleName,
					@GrantedOn,
					@GrantedBy
				)
			) src(
				[HumanAgentHUID],
				[RoleName],
				[GrantedOn],
				[GrantedBy]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert] -- Hybrid
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier,
	@OrganizationHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[TeamMember](
				[TeamHUID],
				[HumanAgentHUID],
				[OrganizationHUID]
			)
			SELECT
				[src].[TeamHUID],
				[src].[HumanAgentHUID],
				[src].[OrganizationHUID]
			FROM 
			(
				VALUES(
					@TeamHUID,
					@HumanAgentHUID,
					@OrganizationHUID
				)
			) src(
				[TeamHUID],
				[HumanAgentHUID],
				[OrganizationHUID]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembers_node_dams_TeamMemberInsert] -- Hybrid
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier,
	@OrganizationHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[TeamMember](
				[TeamHUID],
				[HumanAgentHUID],
				[OrganizationHUID]
			)
			SELECT
				[src].[TeamHUID],
				[src].[HumanAgentHUID],
				[src].[OrganizationHUID]
			FROM 
			(
				VALUES(
					@TeamHUID,
					@HumanAgentHUID,
					@OrganizationHUID
				)
			) src(
				[TeamHUID],
				[HumanAgentHUID],
				[OrganizationHUID]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentInsert] -- Hybrid
	@HumanAgentHUID uniqueidentifier,
	@OrganizationHUID uniqueidentifier,
	@PrimaryEmail varchar(256),
	@HumanAgentExternalIdentifier varchar(128),
	@GivenName nvarchar(128),
	@Surname nvarchar(128)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[HumanAgent](
				[HumanAgentHUID],
				[OrganizationHUID],
				[PrimaryEmail],
				[HumanAgentExternalIdentifier],
				[GivenName],
				[Surname]
			)
			SELECT
				[src].[HumanAgentHUID],
				[src].[OrganizationHUID],
				[src].[PrimaryEmail],
				[src].[HumanAgentExternalIdentifier],
				[src].[GivenName],
				[src].[Surname]
			FROM 
			(
				VALUES(
					@HumanAgentHUID,
					@OrganizationHUID,
					@PrimaryEmail,
					@HumanAgentExternalIdentifier,
					@GivenName,
					@Surname
				)
			) src(
				[HumanAgentHUID],
				[OrganizationHUID],
				[PrimaryEmail],
				[HumanAgentExternalIdentifier],
				[GivenName],
				[Surname]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[ObservationInsert] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@ObservationTypeCode varchar(64),
	@ObservationMadeOn datetime2(7),
	@Value numeric(10, 0),
	@TeamHUID uniqueidentifier,
	@ObservedBy uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[Observation](
				[PointCloudHUID],
				[ObservationTypeCode],
				[ObservationMadeOn],
				[Value],
				[TeamHUID],
				[ObservedBy]
			)
			SELECT
				[src].[PointCloudHUID],
				[src].[ObservationTypeCode],
				[src].[ObservationMadeOn],
				[src].[Value],
				[src].[TeamHUID],
				[src].[ObservedBy]
			FROM 
			(
				VALUES(
					@PointCloudHUID,
					@ObservationTypeCode,
					@ObservationMadeOn,
					@Value,
					@TeamHUID,
					@ObservedBy
				)
			) src(
				[PointCloudHUID],
				[ObservationTypeCode],
				[ObservationMadeOn],
				[Value],
				[TeamHUID],
				[ObservedBy]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[OrganizationInsert] -- Hybrid
	@OrganizationHUID uniqueidentifier,
	@OrganizationName nvarchar(128),
	@PrimaryIndustryCode varchar(64)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[Organization](
				[OrganizationHUID],
				[OrganizationName],
				[PrimaryIndustryCode]
			)
			SELECT
				[src].[OrganizationHUID],
				[src].[OrganizationName],
				[src].[PrimaryIndustryCode]
			FROM 
			(
				VALUES(
					@OrganizationHUID,
					@OrganizationName,
					@PrimaryIndustryCode
				)
			) src(
				[OrganizationHUID],
				[OrganizationName],
				[PrimaryIndustryCode]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[PointCloudMetadataInsert] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@SubspaceOfPointCloud uniqueidentifier,
	@Thumbnail varbinary(MAX),
	@PhysicalDescription nvarchar(1024),
	@RepresentationCode varchar(64),
	@CaptureMetadata varchar(18),
	@RawDataURI varchar(440),
	@TeamHUID uniqueidentifier,
	@CapturedBy uniqueidentifier,
	@CapturedOrDerivedOn datetime2(7)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[PointCloudMetadata](
				[PointCloudHUID],
				[SubspaceOfPointCloud],
				[Thumbnail],
				[PhysicalDescription],
				[RepresentationCode],
				[CaptureMetadata],
				[RawDataURI],
				[TeamHUID],
				[CapturedBy],
				[CapturedOrDerivedOn]
			)
			SELECT
				[src].[PointCloudHUID],
				[src].[SubspaceOfPointCloud],
				[src].[Thumbnail],
				[src].[PhysicalDescription],
				[src].[RepresentationCode],
				[src].[CaptureMetadata],
				[src].[RawDataURI],
				[src].[TeamHUID],
				[src].[CapturedBy],
				[src].[CapturedOrDerivedOn]
			FROM 
			(
				VALUES(
					@PointCloudHUID,
					@SubspaceOfPointCloud,
					@Thumbnail,
					@PhysicalDescription,
					@RepresentationCode,
					@CaptureMetadata,
					@RawDataURI,
					@TeamHUID,
					@CapturedBy,
					@CapturedOrDerivedOn
				)
			) src(
				[PointCloudHUID],
				[SubspaceOfPointCloud],
				[Thumbnail],
				[PhysicalDescription],
				[RepresentationCode],
				[CaptureMetadata],
				[RawDataURI],
				[TeamHUID],
				[CapturedBy],
				[CapturedOrDerivedOn]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[TeamInsert] -- Hybrid
	@TeamHUID uniqueidentifier,
	@TeamName nvarchar(128),
	@OrganizationHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[Team](
				[TeamHUID],
				[TeamName],
				[OrganizationHUID]
			)
			SELECT
				[src].[TeamHUID],
				[src].[TeamName],
				[src].[OrganizationHUID]
			FROM 
			(
				VALUES(
					@TeamHUID,
					@TeamName,
					@OrganizationHUID
				)
			) src(
				[TeamHUID],
				[TeamName],
				[OrganizationHUID]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

