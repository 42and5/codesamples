DROP TYPE IF EXISTS [dams].[tbTypeHumanAgentWithTeamsAndRoles_TeamMember];
GO
CREATE TYPE [dams].[tbTypeHumanAgentWithTeamsAndRoles_TeamMember] AS TABLE (
	[TeamHUID] uniqueidentifier NOT NULL ,
	[HumanAgentHUID] uniqueidentifier NOT NULL ,
	[OrganizationHUID] uniqueidentifier NOT NULL 
	, PRIMARY KEY(
		[TeamHUID],
		[HumanAgentHUID]
	)
);
GO

