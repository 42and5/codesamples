DROP TYPE IF EXISTS [dams].[tbTypeTeamWithMembers_TeamMember];
GO
CREATE TYPE [dams].[tbTypeTeamWithMembers_TeamMember] AS TABLE (
	[TeamHUID] uniqueidentifier NOT NULL ,
	[HumanAgentHUID] uniqueidentifier NOT NULL ,
	[OrganizationHUID] uniqueidentifier NOT NULL 
	, PRIMARY KEY(
		[TeamHUID],
		[HumanAgentHUID]
	)
);
GO

