DROP TYPE IF EXISTS [dams].[tbTypeHumanAgentWithTeamsAndRoles_RoleAssignment];
GO
CREATE TYPE [dams].[tbTypeHumanAgentWithTeamsAndRoles_RoleAssignment] AS TABLE (
	[HumanAgentHUID] uniqueidentifier NOT NULL ,
	[RoleName] varchar(128) NOT NULL ,
	[GrantedOn] datetime2(7) NOT NULL ,
	[GrantedBy] uniqueidentifier NOT NULL 
	, PRIMARY KEY(
		[HumanAgentHUID],
		[RoleName]
	)
);
GO

