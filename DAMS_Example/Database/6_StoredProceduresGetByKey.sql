/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembersGetByKey]
	@TeamHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[TeamHUID] = rootTable.[TeamHUID],
			[TeamName] = rootTable.[TeamName]
		FROM [dams].[Team] AS rootTable
		WHERE rootTable.[TeamHUID] = @TeamHUID
		;
		
		SELECT
			[TeamHUID] = t_2.[TeamHUID],
			[HumanAgentHUID] = t_2.[HumanAgentHUID],
			[OrganizationHUID] = t_2.[OrganizationHUID]
		FROM [dams].[TeamMember] t_2
		INNER JOIN [dams].[Team] t_1
			ON t_2.[TeamHUID] = t_1.[TeamHUID]
		WHERE t_1.[TeamHUID] = @TeamHUID
		;
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRolesGetByKey]
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[HumanAgentHUID] = rootTable.[HumanAgentHUID]
		FROM [dams].[HumanAgent] AS rootTable
		WHERE rootTable.[HumanAgentHUID] = @HumanAgentHUID
		;
		
		SELECT
			[HumanAgentHUID] = t_2.[HumanAgentHUID],
			[RoleName] = t_2.[RoleName],
			[GrantedOn] = t_2.[GrantedOn],
			[GrantedBy] = t_2.[GrantedBy]
		FROM [dams].[RoleAssignment] t_2
		INNER JOIN [dams].[HumanAgent] t_1
			ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
		WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
		;
		SELECT
			[TeamHUID] = t_2.[TeamHUID],
			[HumanAgentHUID] = t_2.[HumanAgentHUID],
			[OrganizationHUID] = t_2.[OrganizationHUID]
		FROM [dams].[TeamMember] t_2
		INNER JOIN [dams].[HumanAgent] t_1
			ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
		WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
		;
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentGetByKey]
	@HumanAgentHUID uniqueidentifier,
	@RoleName varchar(128)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[GrantedBy] = rootTable.[GrantedBy],
			[GrantedOn] = rootTable.[GrantedOn],
			[HumanAgentHUID] = rootTable.[HumanAgentHUID],
			[RoleName] = rootTable.[RoleName]
		FROM [dams].[RoleAssignment] AS rootTable
		WHERE rootTable.[HumanAgentHUID] = @HumanAgentHUID
		  AND rootTable.[RoleName] = @RoleName
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberGetByKey]
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[HumanAgentHUID] = rootTable.[HumanAgentHUID],
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[TeamHUID] = rootTable.[TeamHUID]
		FROM [dams].[TeamMember] AS rootTable
		WHERE rootTable.[TeamHUID] = @TeamHUID
		  AND rootTable.[HumanAgentHUID] = @HumanAgentHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembers_node_dams_TeamMemberGetByKey]
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[HumanAgentHUID] = rootTable.[HumanAgentHUID],
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[TeamHUID] = rootTable.[TeamHUID]
		FROM [dams].[TeamMember] AS rootTable
		WHERE rootTable.[TeamHUID] = @TeamHUID
		  AND rootTable.[HumanAgentHUID] = @HumanAgentHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentGetByKey]
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[GivenName] = rootTable.[GivenName],
			[HumanAgentExternalIdentifier] = rootTable.[HumanAgentExternalIdentifier],
			[HumanAgentHUID] = rootTable.[HumanAgentHUID],
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[PrimaryEmail] = rootTable.[PrimaryEmail],
			[Surname] = rootTable.[Surname]
		FROM [dams].[HumanAgent] AS rootTable
		WHERE rootTable.[HumanAgentHUID] = @HumanAgentHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[ObservationGetByKey]
	@PointCloudHUID uniqueidentifier,
	@ObservationTypeCode varchar(64),
	@ObservationMadeOn datetime2(7)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[ObservationMadeOn] = rootTable.[ObservationMadeOn],
			[ObservationTypeCode] = rootTable.[ObservationTypeCode],
			[ObservedBy] = rootTable.[ObservedBy],
			[PointCloudHUID] = rootTable.[PointCloudHUID],
			[TeamHUID] = rootTable.[TeamHUID],
			[Value] = rootTable.[Value]
		FROM [dams].[Observation] AS rootTable
		WHERE rootTable.[PointCloudHUID] = @PointCloudHUID
		  AND rootTable.[ObservationTypeCode] = @ObservationTypeCode
		  AND rootTable.[ObservationMadeOn] = @ObservationMadeOn
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[OrganizationGetByKey]
	@OrganizationHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[OrganizationName] = rootTable.[OrganizationName],
			[PrimaryIndustryCode] = rootTable.[PrimaryIndustryCode]
		FROM [dams].[Organization] AS rootTable
		WHERE rootTable.[OrganizationHUID] = @OrganizationHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[PointCloudMetadataGetByKey]
	@PointCloudHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[CapturedBy] = rootTable.[CapturedBy],
			[CapturedOrDerivedOn] = rootTable.[CapturedOrDerivedOn],
			[CaptureMetadata] = rootTable.[CaptureMetadata],
			[PhysicalDescription] = rootTable.[PhysicalDescription],
			[PointCloudHUID] = rootTable.[PointCloudHUID],
			[RawDataURI] = rootTable.[RawDataURI],
			[RepresentationCode] = rootTable.[RepresentationCode],
			[SubspaceOfPointCloud] = rootTable.[SubspaceOfPointCloud],
			[TeamHUID] = rootTable.[TeamHUID],
			[Thumbnail] = rootTable.[Thumbnail]
		FROM [dams].[PointCloudMetadata] AS rootTable
		WHERE rootTable.[PointCloudHUID] = @PointCloudHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[TeamGetByKey]
	@TeamHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[TeamHUID] = rootTable.[TeamHUID],
			[TeamName] = rootTable.[TeamName]
		FROM [dams].[Team] AS rootTable
		WHERE rootTable.[TeamHUID] = @TeamHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



