/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembersDelete] -- Hybrid
	@TeamHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_3
			FROM [dams].[Observation] AS t_3
			INNER JOIN [dams].[PointCloudMetadata] AS t_2
			    ON t_3.[PointCloudHUID] = t_2.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[CapturedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID;
			
			DELETE t_2
			FROM [dams].[Observation] AS t_2
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[ObservedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID;
			
			DELETE t_2
			FROM [dams].[PointCloudMetadata] AS t_2
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[CapturedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID;
			
			DELETE t_1
			FROM [dams].[TeamMember] AS t_1
			INNER JOIN [dams].[Team] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID;
			
			DELETE t_0
			FROM [dams].[Team] AS t_0
			WHERE t_0.[TeamHUID] = @TeamHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRolesDelete] -- Hybrid
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_3
			FROM [dams].[Observation] AS t_3
			INNER JOIN [dams].[PointCloudMetadata] AS t_2
			    ON t_3.[PointCloudHUID] = t_2.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[CapturedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_2
			FROM [dams].[Observation] AS t_2
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[ObservedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_2
			FROM [dams].[PointCloudMetadata] AS t_2
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[CapturedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[RoleAssignment] AS t_1
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[GrantedBy] = t_0.[HumanAgentHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[RoleAssignment] AS t_1
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[TeamMember] AS t_1
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_0
			FROM [dams].[HumanAgent] AS t_0
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete] -- Hybrid
	@HumanAgentHUID uniqueidentifier,
	@RoleName varchar(128)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_0
			FROM [dams].[RoleAssignment] AS t_0
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID
			  AND t_0.[RoleName] = @RoleName;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete] -- Hybrid
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_2
			FROM [dams].[Observation] AS t_2
			INNER JOIN [dams].[PointCloudMetadata] AS t_1
			    ON t_2.[PointCloudHUID] = t_1.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[CapturedBy] = t_0.[HumanAgentHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID
			  AND t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[Observation] AS t_1
			INNER JOIN [dams].[TeamMember] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[ObservedBy] = t_0.[HumanAgentHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID
			  AND t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[PointCloudMetadata] AS t_1
			INNER JOIN [dams].[TeamMember] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[CapturedBy] = t_0.[HumanAgentHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID
			  AND t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_0
			FROM [dams].[TeamMember] AS t_0
			WHERE t_0.[TeamHUID] = @TeamHUID
			  AND t_0.[HumanAgentHUID] = @HumanAgentHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembers_node_dams_TeamMemberDelete] -- Hybrid
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_2
			FROM [dams].[Observation] AS t_2
			INNER JOIN [dams].[PointCloudMetadata] AS t_1
			    ON t_2.[PointCloudHUID] = t_1.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[CapturedBy] = t_0.[HumanAgentHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID
			  AND t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[Observation] AS t_1
			INNER JOIN [dams].[TeamMember] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[ObservedBy] = t_0.[HumanAgentHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID
			  AND t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[PointCloudMetadata] AS t_1
			INNER JOIN [dams].[TeamMember] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[CapturedBy] = t_0.[HumanAgentHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID
			  AND t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_0
			FROM [dams].[TeamMember] AS t_0
			WHERE t_0.[TeamHUID] = @TeamHUID
			  AND t_0.[HumanAgentHUID] = @HumanAgentHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentDelete] -- Hybrid
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_3
			FROM [dams].[Observation] AS t_3
			INNER JOIN [dams].[PointCloudMetadata] AS t_2
			    ON t_3.[PointCloudHUID] = t_2.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[CapturedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_2
			FROM [dams].[Observation] AS t_2
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[ObservedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_2
			FROM [dams].[PointCloudMetadata] AS t_2
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[CapturedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[RoleAssignment] AS t_1
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[GrantedBy] = t_0.[HumanAgentHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[RoleAssignment] AS t_1
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_1
			FROM [dams].[TeamMember] AS t_1
			INNER JOIN [dams].[HumanAgent] AS t_0
			    ON t_1.[HumanAgentHUID] = t_0.[HumanAgentHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;
			
			DELETE t_0
			FROM [dams].[HumanAgent] AS t_0
			WHERE t_0.[HumanAgentHUID] = @HumanAgentHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[ObservationDelete] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@ObservationTypeCode varchar(64),
	@ObservationMadeOn datetime2(7)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_0
			FROM [dams].[Observation] AS t_0
			WHERE t_0.[PointCloudHUID] = @PointCloudHUID
			  AND t_0.[ObservationTypeCode] = @ObservationTypeCode
			  AND t_0.[ObservationMadeOn] = @ObservationMadeOn;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[OrganizationDelete] -- Hybrid
	@OrganizationHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_4
			FROM [dams].[Observation] AS t_4
			INNER JOIN [dams].[PointCloudMetadata] AS t_3
			    ON t_4.[PointCloudHUID] = t_3.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[CapturedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_4
			FROM [dams].[Observation] AS t_4
			INNER JOIN [dams].[PointCloudMetadata] AS t_3
			    ON t_4.[PointCloudHUID] = t_3.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[CapturedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_3
			FROM [dams].[Observation] AS t_3
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[ObservedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_3
			FROM [dams].[PointCloudMetadata] AS t_3
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[CapturedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_3
			FROM [dams].[Observation] AS t_3
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[ObservedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_3
			FROM [dams].[PointCloudMetadata] AS t_3
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[CapturedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_2
			FROM [dams].[TeamMember] AS t_2
			INNER JOIN [dams].[Team] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_2
			FROM [dams].[RoleAssignment] AS t_2
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[GrantedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_2
			FROM [dams].[RoleAssignment] AS t_2
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_2
			FROM [dams].[TeamMember] AS t_2
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_1
			FROM [dams].[Team] AS t_1
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_1
			FROM [dams].[HumanAgent] AS t_1
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_0
			FROM [dams].[Organization] AS t_0
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[PointCloudMetadataDelete] -- Hybrid
	@PointCloudHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_1
			FROM [dams].[Observation] AS t_1
			INNER JOIN [dams].[PointCloudMetadata] AS t_0
			    ON t_1.[PointCloudHUID] = t_0.[PointCloudHUID]
			WHERE t_0.[PointCloudHUID] = @PointCloudHUID;
			
			DELETE t_0
			FROM [dams].[PointCloudMetadata] AS t_0
			WHERE t_0.[PointCloudHUID] = @PointCloudHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[TeamDelete] -- Hybrid
	@TeamHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_3
			FROM [dams].[Observation] AS t_3
			INNER JOIN [dams].[PointCloudMetadata] AS t_2
			    ON t_3.[PointCloudHUID] = t_2.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[CapturedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID;
			
			DELETE t_2
			FROM [dams].[Observation] AS t_2
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[ObservedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID;
			
			DELETE t_2
			FROM [dams].[PointCloudMetadata] AS t_2
			INNER JOIN [dams].[TeamMember] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[CapturedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID;
			
			DELETE t_1
			FROM [dams].[TeamMember] AS t_1
			INNER JOIN [dams].[Team] AS t_0
			    ON t_1.[TeamHUID] = t_0.[TeamHUID]
			   AND t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[TeamHUID] = @TeamHUID;
			
			DELETE t_0
			FROM [dams].[Team] AS t_0
			WHERE t_0.[TeamHUID] = @TeamHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



