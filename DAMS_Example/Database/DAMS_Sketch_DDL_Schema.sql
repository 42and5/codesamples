/* WARNING: Destructive drop and replace of experimental DAMS schema.
   Run only in a "sandbox" database 
*/
USE [DAMS_Example]; -- Or another sandbox db you may have
GO

IF NOT EXISTS(
    SELECT 1
    FROM sys.schemas
    WHERE name = 'dams'
)
BEGIN;
    EXEC('CREATE SCHEMA dams AUTHORIZATION dbo;');
END;

IF NOT EXISTS(
    SELECT 1
    FROM sys.schemas
    WHERE name = 'rfrnc'
)
BEGIN;
    EXEC('CREATE SCHEMA rfrnc AUTHORIZATION dbo;');
END;

--------------------------------------------------------------------------------------------

DECLARE @ToDropSchema TABLE(SchemaToDropLike sysname);
DECLARE @PreserveSchema TABLE(SchemaToPreserveLike sysname);

INSERT INTO @ToDropSchema(SchemaToDropLike)
VALUES
	(N'dams'),			-- No wildcard degenerates to "="
	(N'rfrnc')	-- Can use a wildcard
;

-- Preserve overrides ToDrop
INSERT INTO @PreserveSchema(SchemaToPreserveLike)
VALUES
	(N'utility%'),
	(N'meta%'),
    (N'dbo') -- Try to preserve typical db schema in case this is run in the wrong db.
;

-- Calculate full list of schemas to drop, based on Preserve and Drop specs...
DECLARE @SchemaFullDrop TABLE(SchemaName sysname);
INSERT INTO @SchemaFullDrop(SchemaName)
SELECT s.name
FROM sys.schemas s
INNER JOIN @ToDropSchema tds
	ON s.name LIKE tds.SchemaToDropLike
WHERE NOT EXISTS(
	SELECT 1
	FROM @PreserveSchema ps
	WHERE s.name LIKE ps.SchemaToPreserveLike
);

-- Re-usable variable...
DECLARE @DropStmt nvarchar(MAX);

-- Drop any view that references the tables that will be dropped...
SET @DropStmt = N'';
SELECT @DropStmt += CONCAT(N'DROP VIEW ', QUOTENAME(s.name), N'.', QUOTENAME(v.name), N';')
FROM sys.views v
INNER JOIN sys.schemas s
	ON v.schema_id = s.schema_id
WHERE EXISTS(
	SELECT 1
	FROM sys.sql_dependencies d
	INNER JOIN sys.tables tRef
		ON d.referenced_major_id = tRef.object_id
	INNER JOIN sys.schemas sRef
		ON tRef.schema_id = sRef.schema_id
	INNER JOIN @SchemaFullDrop sfdRef
		ON sRef.name = sfdRef.SchemaName
	WHERE d.class < 2 -- restricts to table-level references
	  AND d.object_id = v.object_id
)
;
-- DEBUG: SELECT @DropStmt;
EXEC sp_executeSQL @stmt = @DropStmt;

-- Build the ALTER TABLE...DROP CONSTRAINT statements...
SET @DropStmt = N'';
SELECT @DropStmt += CONCAT(N'ALTER TABLE ', QUOTENAME(sChi.name), N'.', QUOTENAME(tChi.name), N' DROP CONSTRAINT ', fk.name, ';')
FROM sys.foreign_keys fk
INNER JOIN sys.tables tPar
	ON fk.referenced_object_id = tPar.object_id
INNER JOIN sys.schemas sPar
	ON tPar.schema_id = sPar.schema_id
INNER JOIN sys.tables tChi
	ON fk.parent_object_id = tChi.object_id
INNER JOIN sys.schemas sChi
	ON tChi.schema_id = sChi.schema_id
INNER JOIN @SchemaFullDrop sfd
	ON sPar.name = sfd.SchemaName
;
-- DEBUG: SELECT @DropStmt;
EXEC sp_executeSQL @stmt = @DropStmt;



-- Build the DROP TABLE statements...
SET @DropStmt = N'';
SELECT @DropStmt += CONCAT(N'DROP TABLE ', QUOTENAME(s.name), N'.', QUOTENAME(t.name), N';')
FROM sys.tables t
INNER JOIN sys.schemas s
	ON t.schema_id = s.schema_id
INNER JOIN @SchemaFullDrop sfd
	ON s.name = sfd.SchemaName
;
-- DEBUG: SELECT @DropStmt;
EXEC sp_executeSQL @stmt = @DropStmt;

--------------------------------------------------------------------------------------------

CREATE TABLE dams.FeatureAccess(
    RoleName       varchar(128)    NOT NULL,
    FeatureCode    varchar(64)     NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.FeatureAccess ADD 
    CONSTRAINT PK_FeatureAccess 
    PRIMARY KEY CLUSTERED (RoleName, FeatureCode)
GO

CREATE TABLE dams.HumanAgent(
    HumanAgentHUID                  uniqueidentifier    NOT NULL,
    OrganizationHUID                uniqueidentifier    NOT NULL,
    PrimaryEmail                    varchar(256)        NOT NULL,
    HumanAgentExternalIdentifier    varchar(128)        NULL,
    GivenName                       nvarchar(128)       NOT NULL,
    Surname                         nvarchar(128)       NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.HumanAgent ADD 
    CONSTRAINT PK_HumanAgent 
    PRIMARY KEY CLUSTERED (HumanAgentHUID)
;
GO
CREATE UNIQUE INDEX AK_HumanAgent_PrimaryEmail 
    ON dams.HumanAgent(PrimaryEmail)
    WITH (DATA_COMPRESSION = PAGE)
;
GO
CREATE UNIQUE INDEX SK_HumanAgent_OrganizationHUID 
    ON dams.HumanAgent(HumanAgentHUID, OrganizationHUID)
    WITH (DATA_COMPRESSION = PAGE)
;
GO

CREATE TABLE dams.Observation(
    PointCloudHUID         uniqueidentifier    NOT NULL,
    ObservationTypeCode    varchar(64)         NOT NULL,
    ObservationMadeOn      datetime2(7)        NOT NULL,
    [Value]                numeric(10, 0)      NOT NULL,
    TeamHUID               uniqueidentifier    NOT NULL,
    ObservedBy             uniqueidentifier    NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.Observation ADD 
    CONSTRAINT PK_Observation 
    PRIMARY KEY CLUSTERED (PointCloudHUID, ObservationTypeCode, ObservationMadeOn)
;
GO

CREATE TABLE dams.Organization(
    OrganizationHUID       uniqueidentifier    NOT NULL,
    OrganizationName       nvarchar(128)       NOT NULL,
    PrimaryIndustryCode    varchar(64)         NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.Organization ADD 
    CONSTRAINT PK_Organization 
    PRIMARY KEY CLUSTERED (OrganizationHUID)
;
GO


CREATE TABLE dams.PointCloudMetadata(
    PointCloudHUID          uniqueidentifier    NOT NULL,
    SubspaceOfPointCloud    uniqueidentifier    NULL,
    Thumbnail               varbinary(max)      NOT NULL,
    PhysicalDescription     nvarchar(1024)      NOT NULL,
    RepresentationCode      varchar(64)         NOT NULL,
    CaptureMetadata         varchar(18)         NOT NULL,
    RawDataURI              varchar(440)        NOT NULL,
    TeamHUID                uniqueidentifier    NOT NULL,
    CapturedBy              uniqueidentifier    NOT NULL,
    CapturedOrDerivedOn     datetime2(7)        NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.PointCloudMetadata ADD 
    CONSTRAINT PK_PointCloudMetadata 
    PRIMARY KEY CLUSTERED (PointCloudHUID)
;
GO
CREATE UNIQUE INDEX AK_PointCloudMetadata_RawDataURI 
    ON dams.PointCloudMetadata(RawDataURI)
    WITH (DATA_COMPRESSION = PAGE)
;
GO

CREATE TABLE dams.[Role](
    RoleName         varchar(128)     NOT NULL,
    Documentation    varchar(1024)    NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.[Role] ADD 
    CONSTRAINT PK_Role 
    PRIMARY KEY CLUSTERED (RoleName)
GO

CREATE TABLE dams.RoleAssignment(
    HumanAgentHUID    uniqueidentifier    NOT NULL,
    RoleName          varchar(128)        NOT NULL,
    GrantedOn         datetime2(7)        NOT NULL,
    GrantedBy         uniqueidentifier    NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.RoleAssignment ADD 
    CONSTRAINT PK_RoleAssignment 
    PRIMARY KEY CLUSTERED (HumanAgentHUID, RoleName)
;
GO

CREATE TABLE dams.Team(
    TeamHUID            uniqueidentifier    NOT NULL,
    TeamName            nvarchar(128)       NOT NULL,
    OrganizationHUID    uniqueidentifier    NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.Team ADD 
    CONSTRAINT PK_Team 
    PRIMARY KEY CLUSTERED (TeamHUID)
;
GO
CREATE UNIQUE INDEX SK_Team_OrganizationHUID 
    ON dams.Team(TeamHUID, OrganizationHUID)
    WITH (DATA_COMPRESSION = PAGE)
;
GO
CREATE TABLE dams.TeamMember(
    TeamHUID            uniqueidentifier    NOT NULL,
    HumanAgentHUID      uniqueidentifier    NOT NULL,
    OrganizationHUID    uniqueidentifier    NOT NULL
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE dams.TeamMember ADD 
    CONSTRAINT PK_TeamMember 
    PRIMARY KEY CLUSTERED (TeamHUID, HumanAgentHUID)
;
GO

-- (BEGIN) Vendor-managed reference data --
CREATE TABLE rfrnc.Feature(
    FeatureCode         varchar(64)    NOT NULL,
    ConceptOrganizer    hierarchyid    NOT NULL,
    ParentOrganizer AS
        CASE WHEN ConceptOrganizer.GetLevel() > 1 
            THEN ConceptOrganizer.GetAncestor(1) 
            ELSE CAST(NULL AS hierarchyid)
        END PERSISTED
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE rfrnc.Feature ADD 
    CONSTRAINT PK_Feature 
    PRIMARY KEY CLUSTERED (FeatureCode)
;
GO
CREATE UNIQUE INDEX AK_Feature 
    ON rfrnc.Feature(ConceptOrganizer)
    WITH (DATA_COMPRESSION = PAGE)
;
GO
ALTER TABLE rfrnc.Feature ADD 
    CONSTRAINT FK_Feature_ImpliedBy_Feature 
    FOREIGN KEY (ParentOrganizer)
    REFERENCES rfrnc.Feature(ConceptOrganizer)
;
GO

INSERT INTO rfrnc.Feature(
    FeatureCode,
    ConceptOrganizer
)
VALUES
    ('Administer', '/1/'),
    ('ArtifactRead', '/2/'),
    (  'ArtifactAdd', '/2/1/'),
    (  'ArtifactChange', '/2/2/'),
    (    'ArtifactRemove', '/2/2/1/'),
    ('Annotate', '/3/'),
    (  'Observe', '/3/1/')
;
GO

CREATE TABLE rfrnc.PointCloudRepresentation(
    RepresentationCode    varchar(64)      NOT NULL,
    ConceptOrganizer      hierarchyid      NOT NULL,
    ParentOrganizer AS
        CASE WHEN ConceptOrganizer.GetLevel() > 1 
            THEN ConceptOrganizer.GetAncestor(1) 
            ELSE CAST(NULL AS hierarchyid)
        END PERSISTED
) WITH (DATA_COMPRESSION = PAGE);
GO
ALTER TABLE rfrnc.PointCloudRepresentation ADD 
    CONSTRAINT PK_PointCloudRepresentation 
    PRIMARY KEY CLUSTERED (RepresentationCode)
;
GO
CREATE UNIQUE INDEX AK_PointCloudRepresentation_ConceptOrganizer 
    ON rfrnc.PointCloudRepresentation(ConceptOrganizer)
    WITH (DATA_COMPRESSION = PAGE)
;
GO
ALTER TABLE rfrnc.PointCloudRepresentation ADD 
    CONSTRAINT FK_PointcloudRepresentation_SpecializationOf_PointCloudRepresentation 
    FOREIGN KEY (ParentOrganizer)
    REFERENCES rfrnc.PointCloudRepresentation(ConceptOrganizer)
;
GO

INSERT INTO rfrnc.PointCloudRepresentation(
    RepresentationCode,
    ConceptOrganizer
)
VALUES
    ('Mesh', '/1/'),
    (  'TriangularMesh', '/1/1/'),
    (  'QuadrilateralMesh', '/1/2/'),
    ('Voxel', '/2/'),
    ('Parametric', '/3/'),
    ('Raster', '/4/'),
    (  'DepthMap', '/4/1/'),
    (    'RGB-D', '/4/1/1/')
;
GO

/* For simplicity, Role and FeatureAccess entries managed by vendor rather than user-configurable */
INSERT INTO dams.[Role](
    RoleName,
    Documentation
)
VALUES
    ('Administrator', 'Can administer the SaaS solution on behalf of organization'),
    ('Viewer', 'Can search for and read Point Cloud Metadata and Raw Data for artifacts owned by any Team on which they are a member'),
    ('Editor', 'Can add, change, or remove Point Clouds to/from the system for any artifacts owned by any Team on which they are a member'),
    ('Analyst', 'Can add observations and annotations')
;

INSERT INTO dams.FeatureAccess(
    RoleName,
    FeatureCode
)
VALUES
    ('Administrator', 'Administer'),
    ('Editor', 'ArtifactRemove'),   -- All higher-level features in the hierarchy are implied
    ('Viewer', 'ArtifactRead'),     -- Read-only
    ('Analyst', 'Observe')          -- All higher-level features in the hierarchy are implied
;
-- (END) Vendor-managed reference data --




ALTER TABLE dams.FeatureAccess ADD 
    CONSTRAINT FK_FeatureAccess_EnabledFor_Role 
    FOREIGN KEY (RoleName)
    REFERENCES dams.[Role](RoleName)
    ,
    CONSTRAINT FK_FeatureAccess_EnablementOf_Feature 
    FOREIGN KEY (FeatureCode)
    REFERENCES rfrnc.Feature(FeatureCode)
;
GO

ALTER TABLE dams.HumanAgent ADD 
    CONSTRAINT FK_HumanAgent_EmployeedBy_Organization 
    FOREIGN KEY (OrganizationHUID)
    REFERENCES dams.Organization(OrganizationHUID)
;
GO


ALTER TABLE dams.Observation ADD 
    CONSTRAINT FK_Observation_MadeAgainst_PointCloud 
    FOREIGN KEY (PointCloudHUID)
    REFERENCES dams.PointCloudMetadata(PointCloudHUID)
    ,
    CONSTRAINT FK_Observation_MadeBy_TeamMember 
    FOREIGN KEY (TeamHUID, ObservedBy)
    REFERENCES dams.TeamMember(TeamHUID, HumanAgentHUID)
;
GO


ALTER TABLE dams.PointCloudMetadata ADD 
    CONSTRAINT FK_PointCloudMetadata_CapturedBy_TeamMember 
    FOREIGN KEY (TeamHUID, CapturedBy)
    REFERENCES dams.TeamMember(TeamHUID, HumanAgentHUID)
    ,
    CONSTRAINT FK_PointCloudMetadata_RepresentedAs_Representation 
    FOREIGN KEY (RepresentationCode)
    REFERENCES rfrnc.PointCloudRepresentation(RepresentationCode)
    ,
    CONSTRAINT FK_PointCloudMetadata_SubspaceOf_PointCloudMetadata 
    FOREIGN KEY (SubspaceOfPointCloud)
    REFERENCES dams.PointCloudMetadata(PointCloudHUID)
GO

ALTER TABLE dams.RoleAssignment ADD 
    CONSTRAINT FK_RoleAssignment_IsOf_HumanAgent 
    FOREIGN KEY (HumanAgentHUID)
    REFERENCES dams.HumanAgent(HumanAgentHUID)
    ,
    CONSTRAINT FK_RoleAssignment_GrantedBy_HumanAgent 
    FOREIGN KEY (GrantedBy)
    REFERENCES dams.HumanAgent(HumanAgentHUID)
    , 
    CONSTRAINT FK_RoleAssignment_IsTo_Role 
    FOREIGN KEY (RoleName)
    REFERENCES dams.[Role](RoleName)
;
GO

ALTER TABLE dams.Team ADD 
    CONSTRAINT FK_Team_DefinedBy_Organization 
    FOREIGN KEY (OrganizationHUID)
    REFERENCES dams.Organization(OrganizationHUID)
;
GO


ALTER TABLE dams.TeamMember ADD 
    CONSTRAINT FK_TeamMember_AssignmentOf_HumanAgent 
    FOREIGN KEY (HumanAgentHUID, OrganizationHUID)
    REFERENCES dams.HumanAgent(HumanAgentHUID, OrganizationHUID)
    ,
    CONSTRAINT FK_TeamMember_AssignmentTo_Team 
    FOREIGN KEY (TeamHUID, OrganizationHUID)
    REFERENCES dams.Team(TeamHUID, OrganizationHUID)
;
GO


