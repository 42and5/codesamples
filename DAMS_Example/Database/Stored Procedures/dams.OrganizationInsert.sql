/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[OrganizationInsert] -- Hybrid
	@OrganizationHUID uniqueidentifier,
	@OrganizationName nvarchar(128),
	@PrimaryIndustryCode varchar(64)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[Organization](
				[OrganizationHUID],
				[OrganizationName],
				[PrimaryIndustryCode]
			)
			SELECT
				[src].[OrganizationHUID],
				[src].[OrganizationName],
				[src].[PrimaryIndustryCode]
			FROM 
			(
				VALUES(
					@OrganizationHUID,
					@OrganizationName,
					@PrimaryIndustryCode
				)
			) src(
				[OrganizationHUID],
				[OrganizationName],
				[PrimaryIndustryCode]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

