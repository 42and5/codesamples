/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentGetByKey]
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[GivenName] = rootTable.[GivenName],
			[HumanAgentExternalIdentifier] = rootTable.[HumanAgentExternalIdentifier],
			[HumanAgentHUID] = rootTable.[HumanAgentHUID],
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[PrimaryEmail] = rootTable.[PrimaryEmail],
			[Surname] = rootTable.[Surname]
		FROM [dams].[HumanAgent] AS rootTable
		WHERE rootTable.[HumanAgentHUID] = @HumanAgentHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



