/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[ObservationDelete] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@ObservationTypeCode varchar(64),
	@ObservationMadeOn datetime2(7)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_0
			FROM [dams].[Observation] AS t_0
			WHERE t_0.[PointCloudHUID] = @PointCloudHUID
			  AND t_0.[ObservationTypeCode] = @ObservationTypeCode
			  AND t_0.[ObservationMadeOn] = @ObservationMadeOn;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



