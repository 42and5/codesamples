/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[OrganizationGetByKey]
	@OrganizationHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[OrganizationName] = rootTable.[OrganizationName],
			[PrimaryIndustryCode] = rootTable.[PrimaryIndustryCode]
		FROM [dams].[Organization] AS rootTable
		WHERE rootTable.[OrganizationHUID] = @OrganizationHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



