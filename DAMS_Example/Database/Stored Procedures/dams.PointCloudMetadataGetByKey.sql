/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[PointCloudMetadataGetByKey]
	@PointCloudHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[CapturedBy] = rootTable.[CapturedBy],
			[CapturedOrDerivedOn] = rootTable.[CapturedOrDerivedOn],
			[CaptureMetadata] = rootTable.[CaptureMetadata],
			[PhysicalDescription] = rootTable.[PhysicalDescription],
			[PointCloudHUID] = rootTable.[PointCloudHUID],
			[RawDataURI] = rootTable.[RawDataURI],
			[RepresentationCode] = rootTable.[RepresentationCode],
			[SubspaceOfPointCloud] = rootTable.[SubspaceOfPointCloud],
			[TeamHUID] = rootTable.[TeamHUID],
			[Thumbnail] = rootTable.[Thumbnail]
		FROM [dams].[PointCloudMetadata] AS rootTable
		WHERE rootTable.[PointCloudHUID] = @PointCloudHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



