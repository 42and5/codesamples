/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRolesUpdate] -- Hybrid
	@HumanAgentHUID uniqueidentifier
	,
	@tvpdamsRoleAssignment AS [dams].[tbTypeHumanAgentWithTeamsAndRoles_RoleAssignment] READONLY,
	@tvpdamsTeamMember AS [dams].[tbTypeHumanAgentWithTeamsAndRoles_TeamMember] READONLY
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[HumanAgentHUID],
					t_2.[RoleName],
					t_2.[GrantedOn],
					t_2.[GrantedBy]
				FROM [dams].[RoleAssignment] t_2
				INNER JOIN [dams].[HumanAgent] t_1
					ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
				WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[HumanAgentHUID],
					[src].[RoleName],
					[src].[GrantedOn],
					[src].[GrantedBy]
				FROM @tvpdamsRoleAssignment src
			) AS src
				ON src.[HumanAgentHUID] = t.[HumanAgentHUID]
				 AND src.[RoleName] = t.[RoleName]
			WHEN NOT MATCHED
				THEN INSERT (
					[HumanAgentHUID],
					[RoleName],
					[GrantedOn],
					[GrantedBy]
				)
				VALUES(
					src.[HumanAgentHUID],
					src.[RoleName],
					src.[GrantedOn],
					src.[GrantedBy]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[GrantedOn]
				EXCEPT
				SELECT 
					t.[GrantedOn]
			)
				THEN UPDATE SET
					t.[GrantedOn] = [src].[GrantedOn]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[TeamHUID],
					t_2.[HumanAgentHUID],
					t_2.[OrganizationHUID]
				FROM [dams].[TeamMember] t_2
				INNER JOIN [dams].[HumanAgent] t_1
					ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
				WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[TeamHUID],
					[src].[HumanAgentHUID],
					[src].[OrganizationHUID]
				FROM @tvpdamsTeamMember src
			) AS src
				ON src.[TeamHUID] = t.[TeamHUID]
				 AND src.[HumanAgentHUID] = t.[HumanAgentHUID]
			WHEN NOT MATCHED
				THEN INSERT (
					[TeamHUID],
					[HumanAgentHUID],
					[OrganizationHUID]
				)
				VALUES(
					src.[TeamHUID],
					src.[HumanAgentHUID],
					src.[OrganizationHUID]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[OrganizationHUID]
				EXCEPT
				SELECT 
					t.[OrganizationHUID]
			)
				THEN UPDATE SET
					t.[OrganizationHUID] = [src].[OrganizationHUID]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[HumanAgentHUID],
					t_2.[RoleName],
					t_2.[GrantedOn],
					t_2.[GrantedBy]
				FROM [dams].[RoleAssignment] t_2
				INNER JOIN [dams].[HumanAgent] t_1
					ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
				WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[HumanAgentHUID],
					[src].[RoleName],
					[src].[GrantedOn],
					[src].[GrantedBy]
				FROM @tvpdamsRoleAssignment src
			) AS src
				ON src.[HumanAgentHUID] = t.[HumanAgentHUID]
				 AND src.[RoleName] = t.[RoleName]
			WHEN NOT MATCHED
				THEN INSERT (
					[HumanAgentHUID],
					[RoleName],
					[GrantedOn],
					[GrantedBy]
				)
				VALUES(
					src.[HumanAgentHUID],
					src.[RoleName],
					src.[GrantedOn],
					src.[GrantedBy]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[GrantedOn]
				EXCEPT
				SELECT 
					t.[GrantedOn]
			)
				THEN UPDATE SET
					t.[GrantedOn] = [src].[GrantedOn]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[TeamHUID],
					t_2.[HumanAgentHUID],
					t_2.[OrganizationHUID]
				FROM [dams].[TeamMember] t_2
				INNER JOIN [dams].[HumanAgent] t_1
					ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
				WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[TeamHUID],
					[src].[HumanAgentHUID],
					[src].[OrganizationHUID]
				FROM @tvpdamsTeamMember src
			) AS src
				ON src.[TeamHUID] = t.[TeamHUID]
				 AND src.[HumanAgentHUID] = t.[HumanAgentHUID]
			WHEN NOT MATCHED
				THEN INSERT (
					[TeamHUID],
					[HumanAgentHUID],
					[OrganizationHUID]
				)
				VALUES(
					src.[TeamHUID],
					src.[HumanAgentHUID],
					src.[OrganizationHUID]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[OrganizationHUID]
				EXCEPT
				SELECT 
					t.[OrganizationHUID]
			)
				THEN UPDATE SET
					t.[OrganizationHUID] = [src].[OrganizationHUID]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



