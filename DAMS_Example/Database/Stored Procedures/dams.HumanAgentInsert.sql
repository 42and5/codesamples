/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentInsert] -- Hybrid
	@HumanAgentHUID uniqueidentifier,
	@OrganizationHUID uniqueidentifier,
	@PrimaryEmail varchar(256),
	@HumanAgentExternalIdentifier varchar(128),
	@GivenName nvarchar(128),
	@Surname nvarchar(128)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[HumanAgent](
				[HumanAgentHUID],
				[OrganizationHUID],
				[PrimaryEmail],
				[HumanAgentExternalIdentifier],
				[GivenName],
				[Surname]
			)
			SELECT
				[src].[HumanAgentHUID],
				[src].[OrganizationHUID],
				[src].[PrimaryEmail],
				[src].[HumanAgentExternalIdentifier],
				[src].[GivenName],
				[src].[Surname]
			FROM 
			(
				VALUES(
					@HumanAgentHUID,
					@OrganizationHUID,
					@PrimaryEmail,
					@HumanAgentExternalIdentifier,
					@GivenName,
					@Surname
				)
			) src(
				[HumanAgentHUID],
				[OrganizationHUID],
				[PrimaryEmail],
				[HumanAgentExternalIdentifier],
				[GivenName],
				[Surname]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

