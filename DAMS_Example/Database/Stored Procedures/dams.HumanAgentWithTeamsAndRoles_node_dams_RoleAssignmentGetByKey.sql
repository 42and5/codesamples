/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentGetByKey]
	@HumanAgentHUID uniqueidentifier,
	@RoleName varchar(128)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[GrantedBy] = rootTable.[GrantedBy],
			[GrantedOn] = rootTable.[GrantedOn],
			[HumanAgentHUID] = rootTable.[HumanAgentHUID],
			[RoleName] = rootTable.[RoleName]
		FROM [dams].[RoleAssignment] AS rootTable
		WHERE rootTable.[HumanAgentHUID] = @HumanAgentHUID
		  AND rootTable.[RoleName] = @RoleName
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



