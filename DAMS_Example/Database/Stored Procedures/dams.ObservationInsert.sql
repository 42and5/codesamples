/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[ObservationInsert] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@ObservationTypeCode varchar(64),
	@ObservationMadeOn datetime2(7),
	@Value numeric(10, 0),
	@TeamHUID uniqueidentifier,
	@ObservedBy uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[Observation](
				[PointCloudHUID],
				[ObservationTypeCode],
				[ObservationMadeOn],
				[Value],
				[TeamHUID],
				[ObservedBy]
			)
			SELECT
				[src].[PointCloudHUID],
				[src].[ObservationTypeCode],
				[src].[ObservationMadeOn],
				[src].[Value],
				[src].[TeamHUID],
				[src].[ObservedBy]
			FROM 
			(
				VALUES(
					@PointCloudHUID,
					@ObservationTypeCode,
					@ObservationMadeOn,
					@Value,
					@TeamHUID,
					@ObservedBy
				)
			) src(
				[PointCloudHUID],
				[ObservationTypeCode],
				[ObservationMadeOn],
				[Value],
				[TeamHUID],
				[ObservedBy]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

