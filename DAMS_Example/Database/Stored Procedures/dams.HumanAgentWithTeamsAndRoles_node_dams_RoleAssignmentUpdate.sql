/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate] -- Hybrid
	@HumanAgentHUID uniqueidentifier,
	@RoleName varchar(128),
	@GrantedOn datetime2(7),
	@GrantedBy uniqueidentifier
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[GrantedOn] = [src].[GrantedOn],
				t.[GrantedBy] = [src].[GrantedBy]
			FROM [dams].[RoleAssignment] t
			INNER JOIN
			(
				VALUES(
					@HumanAgentHUID,
					@RoleName,
					@GrantedOn,
					@GrantedBy
				)
			) src (
				[HumanAgentHUID],
				[RoleName],
				[GrantedOn],
				[GrantedBy]
			)
				ON src.[HumanAgentHUID] = t.[HumanAgentHUID]
				 AND src.[RoleName] = t.[RoleName]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



