/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembers_node_dams_TeamMemberGetByKey]
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[HumanAgentHUID] = rootTable.[HumanAgentHUID],
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[TeamHUID] = rootTable.[TeamHUID]
		FROM [dams].[TeamMember] AS rootTable
		WHERE rootTable.[TeamHUID] = @TeamHUID
		  AND rootTable.[HumanAgentHUID] = @HumanAgentHUID
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



