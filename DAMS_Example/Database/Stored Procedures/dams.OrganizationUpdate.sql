/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[OrganizationUpdate] -- Hybrid
	@OrganizationHUID uniqueidentifier,
	@OrganizationName nvarchar(128),
	@PrimaryIndustryCode varchar(64)
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[OrganizationName] = [src].[OrganizationName],
				t.[PrimaryIndustryCode] = [src].[PrimaryIndustryCode]
			FROM [dams].[Organization] t
			INNER JOIN
			(
				VALUES(
					@OrganizationHUID,
					@OrganizationName,
					@PrimaryIndustryCode
				)
			) src (
				[OrganizationHUID],
				[OrganizationName],
				[PrimaryIndustryCode]
			)
				ON src.[OrganizationHUID] = t.[OrganizationHUID]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



