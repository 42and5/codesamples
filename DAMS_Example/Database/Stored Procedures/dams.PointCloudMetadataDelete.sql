/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[PointCloudMetadataDelete] -- Hybrid
	@PointCloudHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_1
			FROM [dams].[Observation] AS t_1
			INNER JOIN [dams].[PointCloudMetadata] AS t_0
			    ON t_1.[PointCloudHUID] = t_0.[PointCloudHUID]
			WHERE t_0.[PointCloudHUID] = @PointCloudHUID;
			
			DELETE t_0
			FROM [dams].[PointCloudMetadata] AS t_0
			WHERE t_0.[PointCloudHUID] = @PointCloudHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



