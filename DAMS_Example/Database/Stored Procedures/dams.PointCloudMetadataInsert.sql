/**************************************************************************************************

	INSERT operation for the graph
	{GraphDoc}
	
**************************************************************************************************/
CREATE PROC [dams].[PointCloudMetadataInsert] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@SubspaceOfPointCloud uniqueidentifier,
	@Thumbnail varbinary(MAX),
	@PhysicalDescription nvarchar(1024),
	@RepresentationCode varchar(64),
	@CaptureMetadata varchar(18),
	@RawDataURI varchar(440),
	@TeamHUID uniqueidentifier,
	@CapturedBy uniqueidentifier,
	@CapturedOrDerivedOn datetime2(7)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			INSERT INTO [dams].[PointCloudMetadata](
				[PointCloudHUID],
				[SubspaceOfPointCloud],
				[Thumbnail],
				[PhysicalDescription],
				[RepresentationCode],
				[CaptureMetadata],
				[RawDataURI],
				[TeamHUID],
				[CapturedBy],
				[CapturedOrDerivedOn]
			)
			SELECT
				[src].[PointCloudHUID],
				[src].[SubspaceOfPointCloud],
				[src].[Thumbnail],
				[src].[PhysicalDescription],
				[src].[RepresentationCode],
				[src].[CaptureMetadata],
				[src].[RawDataURI],
				[src].[TeamHUID],
				[src].[CapturedBy],
				[src].[CapturedOrDerivedOn]
			FROM 
			(
				VALUES(
					@PointCloudHUID,
					@SubspaceOfPointCloud,
					@Thumbnail,
					@PhysicalDescription,
					@RepresentationCode,
					@CaptureMetadata,
					@RawDataURI,
					@TeamHUID,
					@CapturedBy,
					@CapturedOrDerivedOn
				)
			) src(
				[PointCloudHUID],
				[SubspaceOfPointCloud],
				[Thumbnail],
				[PhysicalDescription],
				[RepresentationCode],
				[CaptureMetadata],
				[RawDataURI],
				[TeamHUID],
				[CapturedBy],
				[CapturedOrDerivedOn]
			)
			;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;


		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO

