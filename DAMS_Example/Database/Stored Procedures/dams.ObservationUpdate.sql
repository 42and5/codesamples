/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[ObservationUpdate] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@ObservationTypeCode varchar(64),
	@ObservationMadeOn datetime2(7),
	@Value numeric(10, 0),
	@TeamHUID uniqueidentifier,
	@ObservedBy uniqueidentifier
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[Value] = [src].[Value],
				t.[TeamHUID] = [src].[TeamHUID],
				t.[ObservedBy] = [src].[ObservedBy]
			FROM [dams].[Observation] t
			INNER JOIN
			(
				VALUES(
					@PointCloudHUID,
					@ObservationTypeCode,
					@ObservationMadeOn,
					@Value,
					@TeamHUID,
					@ObservedBy
				)
			) src (
				[PointCloudHUID],
				[ObservationTypeCode],
				[ObservationMadeOn],
				[Value],
				[TeamHUID],
				[ObservedBy]
			)
				ON src.[PointCloudHUID] = t.[PointCloudHUID]
				 AND src.[ObservationTypeCode] = t.[ObservationTypeCode]
				 AND src.[ObservationMadeOn] = t.[ObservationMadeOn]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



