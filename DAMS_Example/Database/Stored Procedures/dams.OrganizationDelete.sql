/**************************************************************************************************

	DELETE operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[OrganizationDelete] -- Hybrid
	@OrganizationHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;
		IF @EntryTranCount = 0 BEGIN TRAN;
			
			DELETE t_4
			FROM [dams].[Observation] AS t_4
			INNER JOIN [dams].[PointCloudMetadata] AS t_3
			    ON t_4.[PointCloudHUID] = t_3.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[CapturedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_4
			FROM [dams].[Observation] AS t_4
			INNER JOIN [dams].[PointCloudMetadata] AS t_3
			    ON t_4.[PointCloudHUID] = t_3.[PointCloudHUID]
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[CapturedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_3
			FROM [dams].[Observation] AS t_3
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[ObservedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_3
			FROM [dams].[PointCloudMetadata] AS t_3
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[CapturedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[Team] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_3
			FROM [dams].[Observation] AS t_3
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[ObservedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_3
			FROM [dams].[PointCloudMetadata] AS t_3
			INNER JOIN [dams].[TeamMember] AS t_2
			    ON t_3.[TeamHUID] = t_2.[TeamHUID]
			   AND t_3.[CapturedBy] = t_2.[HumanAgentHUID]
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_2
			FROM [dams].[TeamMember] AS t_2
			INNER JOIN [dams].[Team] AS t_1
			    ON t_2.[TeamHUID] = t_1.[TeamHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_2
			FROM [dams].[RoleAssignment] AS t_2
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[GrantedBy] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_2
			FROM [dams].[RoleAssignment] AS t_2
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_2
			FROM [dams].[TeamMember] AS t_2
			INNER JOIN [dams].[HumanAgent] AS t_1
			    ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
			   AND t_2.[OrganizationHUID] = t_1.[OrganizationHUID]
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_1
			FROM [dams].[Team] AS t_1
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_1
			FROM [dams].[HumanAgent] AS t_1
			INNER JOIN [dams].[Organization] AS t_0
			    ON t_1.[OrganizationHUID] = t_0.[OrganizationHUID]
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;
			
			DELETE t_0
			FROM [dams].[Organization] AS t_0
			WHERE t_0.[OrganizationHUID] = @OrganizationHUID;

			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0 ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



