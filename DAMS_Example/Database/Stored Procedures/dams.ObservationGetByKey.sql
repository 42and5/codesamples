/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[ObservationGetByKey]
	@PointCloudHUID uniqueidentifier,
	@ObservationTypeCode varchar(64),
	@ObservationMadeOn datetime2(7)
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[ObservationMadeOn] = rootTable.[ObservationMadeOn],
			[ObservationTypeCode] = rootTable.[ObservationTypeCode],
			[ObservedBy] = rootTable.[ObservedBy],
			[PointCloudHUID] = rootTable.[PointCloudHUID],
			[TeamHUID] = rootTable.[TeamHUID],
			[Value] = rootTable.[Value]
		FROM [dams].[Observation] AS rootTable
		WHERE rootTable.[PointCloudHUID] = @PointCloudHUID
		  AND rootTable.[ObservationTypeCode] = @ObservationTypeCode
		  AND rootTable.[ObservationMadeOn] = @ObservationMadeOn
		;
		
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



