/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembersGetByKey]
	@TeamHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[OrganizationHUID] = rootTable.[OrganizationHUID],
			[TeamHUID] = rootTable.[TeamHUID],
			[TeamName] = rootTable.[TeamName]
		FROM [dams].[Team] AS rootTable
		WHERE rootTable.[TeamHUID] = @TeamHUID
		;
		
		SELECT
			[TeamHUID] = t_2.[TeamHUID],
			[HumanAgentHUID] = t_2.[HumanAgentHUID],
			[OrganizationHUID] = t_2.[OrganizationHUID]
		FROM [dams].[TeamMember] t_2
		INNER JOIN [dams].[Team] t_1
			ON t_2.[TeamHUID] = t_1.[TeamHUID]
		WHERE t_1.[TeamHUID] = @TeamHUID
		;
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



