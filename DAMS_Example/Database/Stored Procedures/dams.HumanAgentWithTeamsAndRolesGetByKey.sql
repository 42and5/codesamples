/**************************************************************************************************

	GetByKey operation for the graph
	{GraphDoc} << TODO: Including full phys graph
	
**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRolesGetByKey]
	@HumanAgentHUID uniqueidentifier
	,
	@ErrorNumber int OUTPUT,
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
	;

	BEGIN TRY;
			
		SELECT
			[HumanAgentHUID] = rootTable.[HumanAgentHUID]
		FROM [dams].[HumanAgent] AS rootTable
		WHERE rootTable.[HumanAgentHUID] = @HumanAgentHUID
		;
		
		SELECT
			[HumanAgentHUID] = t_2.[HumanAgentHUID],
			[RoleName] = t_2.[RoleName],
			[GrantedOn] = t_2.[GrantedOn],
			[GrantedBy] = t_2.[GrantedBy]
		FROM [dams].[RoleAssignment] t_2
		INNER JOIN [dams].[HumanAgent] t_1
			ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
		WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
		;
		SELECT
			[TeamHUID] = t_2.[TeamHUID],
			[HumanAgentHUID] = t_2.[HumanAgentHUID],
			[OrganizationHUID] = t_2.[OrganizationHUID]
		FROM [dams].[TeamMember] t_2
		INNER JOIN [dams].[HumanAgent] t_1
			ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
		WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
		;
		

	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



