DROP PROC IF EXISTS [dams].[TeamWithMembersDelete];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRolesDelete];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentDelete];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberDelete];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembers_node_dams_TeamMemberDelete];
GO

DROP PROC IF EXISTS [dams].[HumanAgentDelete];
GO

DROP PROC IF EXISTS [dams].[ObservationDelete];
GO

DROP PROC IF EXISTS [dams].[OrganizationDelete];
GO

DROP PROC IF EXISTS [dams].[PointCloudMetadataDelete];
GO

DROP PROC IF EXISTS [dams].[TeamDelete];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembersGetAll];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRolesGetAll];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentGetAll];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberGetAll];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembers_node_dams_TeamMemberGetAll];
GO

DROP PROC IF EXISTS [dams].[HumanAgentGetAll];
GO

DROP PROC IF EXISTS [dams].[ObservationGetAll];
GO

DROP PROC IF EXISTS [dams].[OrganizationGetAll];
GO

DROP PROC IF EXISTS [dams].[PointCloudMetadataGetAll];
GO

DROP PROC IF EXISTS [dams].[TeamGetAll];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembersGetByKey];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRolesGetByKey];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentGetByKey];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberGetByKey];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembers_node_dams_TeamMemberGetByKey];
GO

DROP PROC IF EXISTS [dams].[HumanAgentGetByKey];
GO

DROP PROC IF EXISTS [dams].[ObservationGetByKey];
GO

DROP PROC IF EXISTS [dams].[OrganizationGetByKey];
GO

DROP PROC IF EXISTS [dams].[PointCloudMetadataGetByKey];
GO

DROP PROC IF EXISTS [dams].[TeamGetByKey];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembersInsert];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRolesInsert];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentInsert];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberInsert];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembers_node_dams_TeamMemberInsert];
GO

DROP PROC IF EXISTS [dams].[HumanAgentInsert];
GO

DROP PROC IF EXISTS [dams].[ObservationInsert];
GO

DROP PROC IF EXISTS [dams].[OrganizationInsert];
GO

DROP PROC IF EXISTS [dams].[PointCloudMetadataInsert];
GO

DROP PROC IF EXISTS [dams].[TeamInsert];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembersUpdate];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRolesUpdate];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate];
GO

DROP PROC IF EXISTS [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate];
GO

DROP PROC IF EXISTS [dams].[TeamWithMembers_node_dams_TeamMemberUpdate];
GO

DROP PROC IF EXISTS [dams].[HumanAgentUpdate];
GO

DROP PROC IF EXISTS [dams].[ObservationUpdate];
GO

DROP PROC IF EXISTS [dams].[OrganizationUpdate];
GO

DROP PROC IF EXISTS [dams].[PointCloudMetadataUpdate];
GO

DROP PROC IF EXISTS [dams].[TeamUpdate];
GO

