/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembersUpdate] -- Hybrid
	@TeamHUID uniqueidentifier,
	@TeamName nvarchar(128),
	@OrganizationHUID uniqueidentifier
	,
	@tvpdamsTeamMember AS [dams].[tbTypeTeamWithMembers_TeamMember] READONLY
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[TeamName] = [src].[TeamName],
				t.[OrganizationHUID] = [src].[OrganizationHUID]
			FROM [dams].[Team] t
			INNER JOIN
			(
				VALUES(
					@TeamHUID,
					@TeamName,
					@OrganizationHUID
				)
			) src (
				[TeamHUID],
				[TeamName],
				[OrganizationHUID]
			)
				ON src.[TeamHUID] = t.[TeamHUID]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[TeamHUID],
					t_2.[HumanAgentHUID],
					t_2.[OrganizationHUID]
				FROM [dams].[TeamMember] t_2
				INNER JOIN [dams].[Team] t_1
					ON t_2.[TeamHUID] = t_1.[TeamHUID]
				WHERE t_1.[TeamHUID] = @TeamHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[TeamHUID],
					[src].[HumanAgentHUID],
					[src].[OrganizationHUID]
				FROM @tvpdamsTeamMember src
			) AS src
				ON src.[TeamHUID] = t.[TeamHUID]
				 AND src.[HumanAgentHUID] = t.[HumanAgentHUID]
			WHEN NOT MATCHED
				THEN INSERT (
					[TeamHUID],
					[HumanAgentHUID],
					[OrganizationHUID]
				)
				VALUES(
					src.[TeamHUID],
					src.[HumanAgentHUID],
					src.[OrganizationHUID]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[OrganizationHUID]
				EXCEPT
				SELECT 
					t.[OrganizationHUID]
			)
				THEN UPDATE SET
					t.[OrganizationHUID] = [src].[OrganizationHUID]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRolesUpdate] -- Hybrid
	@HumanAgentHUID uniqueidentifier
	,
	@tvpdamsRoleAssignment AS [dams].[tbTypeHumanAgentWithTeamsAndRoles_RoleAssignment] READONLY,
	@tvpdamsTeamMember AS [dams].[tbTypeHumanAgentWithTeamsAndRoles_TeamMember] READONLY
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[HumanAgentHUID],
					t_2.[RoleName],
					t_2.[GrantedOn],
					t_2.[GrantedBy]
				FROM [dams].[RoleAssignment] t_2
				INNER JOIN [dams].[HumanAgent] t_1
					ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
				WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[HumanAgentHUID],
					[src].[RoleName],
					[src].[GrantedOn],
					[src].[GrantedBy]
				FROM @tvpdamsRoleAssignment src
			) AS src
				ON src.[HumanAgentHUID] = t.[HumanAgentHUID]
				 AND src.[RoleName] = t.[RoleName]
			WHEN NOT MATCHED
				THEN INSERT (
					[HumanAgentHUID],
					[RoleName],
					[GrantedOn],
					[GrantedBy]
				)
				VALUES(
					src.[HumanAgentHUID],
					src.[RoleName],
					src.[GrantedOn],
					src.[GrantedBy]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[GrantedOn]
				EXCEPT
				SELECT 
					t.[GrantedOn]
			)
				THEN UPDATE SET
					t.[GrantedOn] = [src].[GrantedOn]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[TeamHUID],
					t_2.[HumanAgentHUID],
					t_2.[OrganizationHUID]
				FROM [dams].[TeamMember] t_2
				INNER JOIN [dams].[HumanAgent] t_1
					ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
				WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[TeamHUID],
					[src].[HumanAgentHUID],
					[src].[OrganizationHUID]
				FROM @tvpdamsTeamMember src
			) AS src
				ON src.[TeamHUID] = t.[TeamHUID]
				 AND src.[HumanAgentHUID] = t.[HumanAgentHUID]
			WHEN NOT MATCHED
				THEN INSERT (
					[TeamHUID],
					[HumanAgentHUID],
					[OrganizationHUID]
				)
				VALUES(
					src.[TeamHUID],
					src.[HumanAgentHUID],
					src.[OrganizationHUID]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[OrganizationHUID]
				EXCEPT
				SELECT 
					t.[OrganizationHUID]
			)
				THEN UPDATE SET
					t.[OrganizationHUID] = [src].[OrganizationHUID]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[HumanAgentHUID],
					t_2.[RoleName],
					t_2.[GrantedOn],
					t_2.[GrantedBy]
				FROM [dams].[RoleAssignment] t_2
				INNER JOIN [dams].[HumanAgent] t_1
					ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
				WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[HumanAgentHUID],
					[src].[RoleName],
					[src].[GrantedOn],
					[src].[GrantedBy]
				FROM @tvpdamsRoleAssignment src
			) AS src
				ON src.[HumanAgentHUID] = t.[HumanAgentHUID]
				 AND src.[RoleName] = t.[RoleName]
			WHEN NOT MATCHED
				THEN INSERT (
					[HumanAgentHUID],
					[RoleName],
					[GrantedOn],
					[GrantedBy]
				)
				VALUES(
					src.[HumanAgentHUID],
					src.[RoleName],
					src.[GrantedOn],
					src.[GrantedBy]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[GrantedOn]
				EXCEPT
				SELECT 
					t.[GrantedOn]
			)
				THEN UPDATE SET
					t.[GrantedOn] = [src].[GrantedOn]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			
			WITH ReducedSet
			AS
			(
				SELECT
					t_2.[TeamHUID],
					t_2.[HumanAgentHUID],
					t_2.[OrganizationHUID]
				FROM [dams].[TeamMember] t_2
				INNER JOIN [dams].[HumanAgent] t_1
					ON t_2.[HumanAgentHUID] = t_1.[HumanAgentHUID]
				WHERE t_1.[HumanAgentHUID] = @HumanAgentHUID
			)
			MERGE INTO ReducedSet AS t
			USING (
				SELECT
					[src].[TeamHUID],
					[src].[HumanAgentHUID],
					[src].[OrganizationHUID]
				FROM @tvpdamsTeamMember src
			) AS src
				ON src.[TeamHUID] = t.[TeamHUID]
				 AND src.[HumanAgentHUID] = t.[HumanAgentHUID]
			WHEN NOT MATCHED
				THEN INSERT (
					[TeamHUID],
					[HumanAgentHUID],
					[OrganizationHUID]
				)
				VALUES(
					src.[TeamHUID],
					src.[HumanAgentHUID],
					src.[OrganizationHUID]
				)
			WHEN MATCHED
			AND EXISTS(
				SELECT 
					src.[OrganizationHUID]
				EXCEPT
				SELECT 
					t.[OrganizationHUID]
			)
				THEN UPDATE SET
					t.[OrganizationHUID] = [src].[OrganizationHUID]
			WHEN NOT MATCHED BY SOURCE
				THEN DELETE
			;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_RoleAssignmentUpdate] -- Hybrid
	@HumanAgentHUID uniqueidentifier,
	@RoleName varchar(128),
	@GrantedOn datetime2(7),
	@GrantedBy uniqueidentifier
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[GrantedOn] = [src].[GrantedOn],
				t.[GrantedBy] = [src].[GrantedBy]
			FROM [dams].[RoleAssignment] t
			INNER JOIN
			(
				VALUES(
					@HumanAgentHUID,
					@RoleName,
					@GrantedOn,
					@GrantedBy
				)
			) src (
				[HumanAgentHUID],
				[RoleName],
				[GrantedOn],
				[GrantedBy]
			)
				ON src.[HumanAgentHUID] = t.[HumanAgentHUID]
				 AND src.[RoleName] = t.[RoleName]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[HumanAgentWithTeamsAndRoles_node_dams_TeamMemberUpdate] -- Hybrid
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier,
	@OrganizationHUID uniqueidentifier
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[OrganizationHUID] = [src].[OrganizationHUID]
			FROM [dams].[TeamMember] t
			INNER JOIN
			(
				VALUES(
					@TeamHUID,
					@HumanAgentHUID,
					@OrganizationHUID
				)
			) src (
				[TeamHUID],
				[HumanAgentHUID],
				[OrganizationHUID]
			)
				ON src.[TeamHUID] = t.[TeamHUID]
				 AND src.[HumanAgentHUID] = t.[HumanAgentHUID]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[TeamWithMembers_node_dams_TeamMemberUpdate] -- Hybrid
	@TeamHUID uniqueidentifier,
	@HumanAgentHUID uniqueidentifier,
	@OrganizationHUID uniqueidentifier
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[OrganizationHUID] = [src].[OrganizationHUID]
			FROM [dams].[TeamMember] t
			INNER JOIN
			(
				VALUES(
					@TeamHUID,
					@HumanAgentHUID,
					@OrganizationHUID
				)
			) src (
				[TeamHUID],
				[HumanAgentHUID],
				[OrganizationHUID]
			)
				ON src.[TeamHUID] = t.[TeamHUID]
				 AND src.[HumanAgentHUID] = t.[HumanAgentHUID]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[HumanAgentUpdate] -- Hybrid
	@HumanAgentHUID uniqueidentifier,
	@OrganizationHUID uniqueidentifier,
	@PrimaryEmail varchar(256),
	@HumanAgentExternalIdentifier varchar(128),
	@GivenName nvarchar(128),
	@Surname nvarchar(128)
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[OrganizationHUID] = [src].[OrganizationHUID],
				t.[PrimaryEmail] = [src].[PrimaryEmail],
				t.[HumanAgentExternalIdentifier] = [src].[HumanAgentExternalIdentifier],
				t.[GivenName] = [src].[GivenName],
				t.[Surname] = [src].[Surname]
			FROM [dams].[HumanAgent] t
			INNER JOIN
			(
				VALUES(
					@HumanAgentHUID,
					@OrganizationHUID,
					@PrimaryEmail,
					@HumanAgentExternalIdentifier,
					@GivenName,
					@Surname
				)
			) src (
				[HumanAgentHUID],
				[OrganizationHUID],
				[PrimaryEmail],
				[HumanAgentExternalIdentifier],
				[GivenName],
				[Surname]
			)
				ON src.[HumanAgentHUID] = t.[HumanAgentHUID]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[ObservationUpdate] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@ObservationTypeCode varchar(64),
	@ObservationMadeOn datetime2(7),
	@Value numeric(10, 0),
	@TeamHUID uniqueidentifier,
	@ObservedBy uniqueidentifier
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[Value] = [src].[Value],
				t.[TeamHUID] = [src].[TeamHUID],
				t.[ObservedBy] = [src].[ObservedBy]
			FROM [dams].[Observation] t
			INNER JOIN
			(
				VALUES(
					@PointCloudHUID,
					@ObservationTypeCode,
					@ObservationMadeOn,
					@Value,
					@TeamHUID,
					@ObservedBy
				)
			) src (
				[PointCloudHUID],
				[ObservationTypeCode],
				[ObservationMadeOn],
				[Value],
				[TeamHUID],
				[ObservedBy]
			)
				ON src.[PointCloudHUID] = t.[PointCloudHUID]
				 AND src.[ObservationTypeCode] = t.[ObservationTypeCode]
				 AND src.[ObservationMadeOn] = t.[ObservationMadeOn]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[OrganizationUpdate] -- Hybrid
	@OrganizationHUID uniqueidentifier,
	@OrganizationName nvarchar(128),
	@PrimaryIndustryCode varchar(64)
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[OrganizationName] = [src].[OrganizationName],
				t.[PrimaryIndustryCode] = [src].[PrimaryIndustryCode]
			FROM [dams].[Organization] t
			INNER JOIN
			(
				VALUES(
					@OrganizationHUID,
					@OrganizationName,
					@PrimaryIndustryCode
				)
			) src (
				[OrganizationHUID],
				[OrganizationName],
				[PrimaryIndustryCode]
			)
				ON src.[OrganizationHUID] = t.[OrganizationHUID]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[PointCloudMetadataUpdate] -- Hybrid
	@PointCloudHUID uniqueidentifier,
	@SubspaceOfPointCloud uniqueidentifier,
	@Thumbnail varbinary(MAX),
	@PhysicalDescription nvarchar(1024),
	@RepresentationCode varchar(64),
	@CaptureMetadata varchar(18),
	@RawDataURI varchar(440),
	@TeamHUID uniqueidentifier,
	@CapturedBy uniqueidentifier,
	@CapturedOrDerivedOn datetime2(7)
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[SubspaceOfPointCloud] = [src].[SubspaceOfPointCloud],
				t.[Thumbnail] = [src].[Thumbnail],
				t.[PhysicalDescription] = [src].[PhysicalDescription],
				t.[RepresentationCode] = [src].[RepresentationCode],
				t.[CaptureMetadata] = [src].[CaptureMetadata],
				t.[RawDataURI] = [src].[RawDataURI],
				t.[TeamHUID] = [src].[TeamHUID],
				t.[CapturedBy] = [src].[CapturedBy],
				t.[CapturedOrDerivedOn] = [src].[CapturedOrDerivedOn]
			FROM [dams].[PointCloudMetadata] t
			INNER JOIN
			(
				VALUES(
					@PointCloudHUID,
					@SubspaceOfPointCloud,
					@Thumbnail,
					@PhysicalDescription,
					@RepresentationCode,
					@CaptureMetadata,
					@RawDataURI,
					@TeamHUID,
					@CapturedBy,
					@CapturedOrDerivedOn
				)
			) src (
				[PointCloudHUID],
				[SubspaceOfPointCloud],
				[Thumbnail],
				[PhysicalDescription],
				[RepresentationCode],
				[CaptureMetadata],
				[RawDataURI],
				[TeamHUID],
				[CapturedBy],
				[CapturedOrDerivedOn]
			)
				ON src.[PointCloudHUID] = t.[PointCloudHUID]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



/**************************************************************************************************

	UPDATE operation for the graph
	{GraphDoc}
	

**************************************************************************************************/
CREATE PROC [dams].[TeamUpdate] -- Hybrid
	@TeamHUID uniqueidentifier,
	@TeamName nvarchar(128),
	@OrganizationHUID uniqueidentifier
	, 
	@ErrorNumber int OUTPUT, 
	@ErrorMessage nvarchar(4000) OUTPUT
AS
BEGIN;
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @retVal int = 0 -- Assume all OK
		  , @EntryTranCount int = @@TRANCOUNT;

	BEGIN TRY;

		IF @EntryTranCount = 0 BEGIN TRAN;
			UPDATE t
			SET
				t.[TeamName] = [src].[TeamName],
				t.[OrganizationHUID] = [src].[OrganizationHUID]
			FROM [dams].[Team] t
			INNER JOIN
			(
				VALUES(
					@TeamHUID,
					@TeamName,
					@OrganizationHUID
				)
			) src (
				[TeamHUID],
				[TeamName],
				[OrganizationHUID]
			)
				ON src.[TeamHUID] = t.[TeamHUID]
			;
			
			IF @@ROWCOUNT != 1
			BEGIN
				RAISERROR(N'Incorrect number of rows were affected by the data change operation.', 16, 1) WITH NOWAIT;
			END;
			

		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			COMMIT TRAN;
	END TRY
	BEGIN CATCH;
		SET @ErrorNumber = ERROR_NUMBER();
		SET @ErrorMessage = ERROR_MESSAGE();
		IF @EntryTranCount = 0 AND @@TRANCOUNT > 0
			ROLLBACK TRAN;
		SET @retVal = -2; -- Problem in DML above
	END CATCH;

	RETURN @retVal;
END;
GO



