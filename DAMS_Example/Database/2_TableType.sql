DROP TYPE IF EXISTS [dams].[tbTypeHumanAgentWithTeamsAndRoles_RoleAssignment];
GO
CREATE TYPE [dams].[tbTypeHumanAgentWithTeamsAndRoles_RoleAssignment] AS TABLE (
	[HumanAgentHUID] uniqueidentifier NOT NULL ,
	[RoleName] varchar(128) NOT NULL ,
	[GrantedOn] datetime2(7) NOT NULL ,
	[GrantedBy] uniqueidentifier NOT NULL 
	, PRIMARY KEY(
		[HumanAgentHUID],
		[RoleName]
	)
);
GO

DROP TYPE IF EXISTS [dams].[tbTypeHumanAgentWithTeamsAndRoles_TeamMember];
GO
CREATE TYPE [dams].[tbTypeHumanAgentWithTeamsAndRoles_TeamMember] AS TABLE (
	[TeamHUID] uniqueidentifier NOT NULL ,
	[HumanAgentHUID] uniqueidentifier NOT NULL ,
	[OrganizationHUID] uniqueidentifier NOT NULL 
	, PRIMARY KEY(
		[TeamHUID],
		[HumanAgentHUID]
	)
);
GO

DROP TYPE IF EXISTS [dams].[tbTypeTeamWithMembers_TeamMember];
GO
CREATE TYPE [dams].[tbTypeTeamWithMembers_TeamMember] AS TABLE (
	[TeamHUID] uniqueidentifier NOT NULL ,
	[HumanAgentHUID] uniqueidentifier NOT NULL ,
	[OrganizationHUID] uniqueidentifier NOT NULL 
	, PRIMARY KEY(
		[TeamHUID],
		[HumanAgentHUID]
	)
);
GO

