# DAMS Digital Artifact Management System

Specialization of general DAMS for management of 3D Point Cloud artifacts.

Backend code only (no UI), presented as REST-based API endpoints.

Full-fidelity Point Cloud data is assumed to be hosted in some external digital asset system such as AWS S3, Dropbox, or the like.

## Stack

The implementation layer is built using C# over SQL Server.

## Simplifications

* Not intended to be fully runnable - code examples only.
* This example focuses on a hypothethical implementation of role-based artifact management.
* Roles and Feature Access are provider-defined rather than customer-extendable.
* Temporal components elided from DB design and implementation.
* Reference data versioning ignored for this example.
* Feature-to-UIElement wiring ignored.