---- The given RootObject references {these}...
DECLARE @RootObject sysname, @maxDepth int;
SET @RootObject = N'<Programmability_Object_Name, sysname, Object_Name>';
SET @maxDepth = <max_depth, int, 32>;

WITH dbObjects(ObjectID, ObjectName, ObjectType, ObjectType_desc, treeLevel, concatName) AS
(
	SELECT  obj.[object_id], obj.[name], obj.type, obj.type_desc, 1 AS [treeLevel], CAST(obj.[name] AS nvarchar(1380)) AS [concatName]
	FROM sys.all_objects obj
	-- Stored Procs, Scalar, Inline Table-valued, and Table-valued Functions, CLR Stored Proc
	WHERE obj.type IN ('P', 'FN', 'IF', 'TF', 'PC')
	  AND obj.[name] = @RootObject

    UNION ALL

	SELECT  dep.referenced_id, objRef.[name], objRef.type, objRef.type_desc, dbObjects.treeLevel + 1, CAST(dbObjects.ConcatName + N'|' + objRef.[name] AS nvarchar(1380))
 	FROM (	SELECT sed_in.referencing_id
			, COALESCE(sed_in.referenced_id,
					(SELECT ao_in.object_id
					FROM sys.all_objects ao_in
					WHERE name = sed_in.referenced_entity_name)
			) [referenced_id] 
		FROM sys.sql_expression_dependencies sed_in
	) dep
	INNER JOIN dbObjects
		ON dep.referencing_id = dbObjects.[ObjectID]
	INNER JOIN sys.all_objects objRef
		ON dep.referenced_id = objRef.[object_id]
	WHERE dep.referenced_id != dep.referencing_id
), dbObjectGenRowNum
AS
(
	SELECT ObjectID, ObjectName, ObjectType, ObjectType_desc, treeLevel, concatName, ROW_NUMBER() OVER(ORDER BY concatName) [rowNum]
	FROM dbObjects
	WHERE treeLevel <= @maxDepth
)
, dbObjectTree
AS
(
	SELECT rowNum, ObjectID, ObjectName, ObjectType, ObjectType_desc, treeLevel, concatName
	, CAST(REPLICATE(N'   ', treeLevel - 1) + N'|--' AS nvarchar(428)) [graphic]
	FROM dbObjectGenRowNum
	WHERE rowNum = (SELECT MAX(rowNum) FROM dbObjectGenRowNum) -- last Rownum

	UNION ALL

	SELECT s.rowNum, s.ObjectID, s.ObjectName, s.ObjectType, s.ObjectType_desc, s.treeLevel, s.concatName
	, CAST(
		REPLACE(
			LEFT(dbObjectTree.graphic, (s.treeLevel - 1) * 3), N'--', N'  '
		) 
		+ REPLICATE(N' ', CASE WHEN s.treeLevel > (dbObjectTree.treeLevel + 1) THEN (s.treeLevel - dbObjectTree.treeLevel - 1) * 3 ELSE 0 END) + N'|--' 
		AS nvarchar(428)
	)
	FROM dbObjectGenRowNum s
	INNER JOIN dbObjectTree
		ON s.rowNum = dbObjectTree.rowNum - 1
)
SELECT ObjectType, CAST(graphic + ObjectName AS nvarchar(256)) [Top Object References These...]
FROM
(
	SELECT /* ObjectType_desc */ ObjectType, graphic, ObjectName, rowNum, 0 [PlusTrig]
	FROM dbObjectTree
	UNION ALL
	SELECT 'TR', ot.graphic + '***', t.Name, ot.rowNum, 1
	FROM sys.triggers t
	INNER JOIN dbObjectTree ot
		ON t.parent_id = OBJECT_ID(ot.ObjectName)
	   AND ot.ObjectType = 'U'
) dX
ORDER BY rowNum, PlusTrig;
GO
