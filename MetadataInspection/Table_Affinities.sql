/****************************************************
  This T-SQL template by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

/***************************************************
  Infer "relatedness" of each combination of two tables
	by inspecting their participation in other object definitions
***************************************************/

DECLARE @ReferencePairs TABLE (Referenced_One int NOT NULL, Referenced_Two int NOT NULL, referencing_id int NOT NULL);

WITH ObjectReferences
AS
(
	SELECT re.referencing_id, OBJECT_ID(TABLE_SCHEMA + N'.' + TABLE_NAME) [referenced_id]
	FROM INFORMATION_SCHEMA.TABLES t
	CROSS APPLY sys.dm_sql_referencing_entities(t.TABLE_SCHEMA + N'.' + t.TABLE_NAME, N'OBJECT') re
	-- TODO: Include or exclude specific tables via addition of WHERE clause here
)
INSERT INTO @ReferencePairs(Referenced_One, Referenced_Two, referencing_id)
SELECT r1.referenced_id [Referenced_One], r2.referenced_id [Referenced_Two], r1.referencing_id
FROM ObjectReferences r1
INNER JOIN ObjectReferences r2
	ON r1.Referenced_ID > r2.Referenced_ID
   AND r1.referencing_id = r2.referencing_id;

SELECT SCHEMA_NAME(ao1.schema_id) + N'.' + ao1.name
	, SCHEMA_NAME(ao2.schema_id) + N'.' + ao2.name
	, COUNT(*) [Affinity]
	, N'{' +
		STUFF(
			(SELECT N', ' + SCHEMA_NAME(ao_in.schema_id) + N'.' + ao_in.name
			FROM @ReferencePairs rp_in
			INNER JOIN sys.all_objects ao_in
				ON rp_in.referencing_id = ao_in.object_id
			WHERE rp_in.Referenced_One = rp.Referenced_One
			  AND rp_in.Referenced_Two = rp.Referenced_Two
			FOR XML PATH('')
			), 1, 2, N''
		) +
	  N'}' [ReferencingObjects]
FROM @ReferencePairs rp
INNER JOIN sys.all_objects ao1
	ON rp.Referenced_One = ao1.object_id
INNER JOIN sys.all_objects ao2
	ON rp.Referenced_Two = ao2.object_id
GROUP BY ao1.schema_id, ao1.name, ao2.schema_id, ao2.name, rp.Referenced_One, rp.Referenced_Two
ORDER BY Affinity DESC;
