/****************************************************
  This T-SQL template by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

/********************************************
   Find tables with IDENTITY columns used as PKs 
     and no other UQ constraints defined
*********************************************/
SELECT tc.TABLE_SCHEMA, tc.TABLE_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY'
  AND EXISTS(
	SELECT 1
	FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
	WHERE CONSTRAINT_SCHEMA = tc.CONSTRAINT_SCHEMA
	  AND CONSTRAINT_NAME = tc.CONSTRAINT_NAME
	  AND COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + N'.' + TABLE_NAME), COLUMN_NAME, 'IsIdentity') = 1
   )
  AND NOT EXISTS(
	SELECT 1
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
	WHERE CONSTRAINT_TYPE = 'UNIQUE'
	  AND TABLE_SCHEMA = tc.TABLE_SCHEMA
	  AND TABLE_NAME = tc.TABLE_NAME
  )
;
