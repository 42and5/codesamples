--- The given LeafObject is referenced by {these}
DECLARE @LeafSchema sysname, @LeafObject sysname, @maxDepth int;
SET @LeafSchema = N'<Object_Schema, sysname, Object_Schema>';
SET @LeafObject = N'<Object_Name, sysname, Object_Name>';
SET @maxDepth = <max_depth, int, 32>;

WITH dbObjects(ObjectID, ObjectName, ObjectType, ObjectType_desc, treeLevel, concatName) AS
(
	SELECT  obj.[object_id], obj.[name], obj.type, obj.type_desc, 1 AS [treeLevel], CAST(obj.[name] AS nvarchar(1380)) AS [concatName]
	FROM sys.all_objects obj
	-- Stored Procs, Scalar, Inline Table-valued, and Table-valued Functions, CLR Stored Proc
	WHERE obj.[name] = @LeafObject
	  AND obj.schema_id = SCHEMA_ID(@LeafSchema)

    UNION ALL

	SELECT  dep.referencing_id, objRef.[name], objRef.type, objRef.type_desc, dbObjects.treeLevel + 1, CAST(dbObjects.ConcatName + N'|' + objRef.[name] AS nvarchar(1380))
	FROM 	(	SELECT sed_in.referencing_id
			, COALESCE(sed_in.referenced_id,
					(SELECT ao_in.object_id
					FROM sys.all_objects ao_in
					WHERE name = sed_in.referenced_entity_name
					  AND schema_id = SCHEMA_ID('dbo'))
			) [referenced_id] 
		FROM sys.sql_expression_dependencies sed_in
	) dep
	INNER JOIN dbObjects
		ON dep.referenced_id = dbObjects.[ObjectID]
	INNER JOIN sys.all_objects objRef
		ON dep.referencing_id = objRef.[object_id]
	WHERE dep.referencing_id != dep.referenced_id
), dbObjectGenRowNum
AS
(
	SELECT ObjectID, ObjectName, ObjectType, ObjectType_desc, treeLevel, concatName, ROW_NUMBER() OVER(ORDER BY concatName) [rowNum]
	FROM dbObjects
	WHERE treeLevel <= @maxDepth
)
, dbObjectTree
AS
(
	SELECT rowNum, ObjectID, ObjectName, ObjectType, ObjectType_desc, treeLevel, concatName
	, CAST(REPLICATE(N'   ', treeLevel - 1) + N'|--' AS nvarchar(428)) [graphic]
	FROM dbObjectGenRowNum
	WHERE rowNum = (SELECT MAX(rowNum) FROM dbObjectGenRowNum) -- last Rownum

	UNION ALL

	SELECT s.rowNum, s.ObjectID, s.ObjectName, s.ObjectType, s.ObjectType_desc, s.treeLevel, s.concatName
	, CAST(
		REPLACE(
			LEFT(dbObjectTree.graphic, (s.treeLevel - 1) * 3), N'--', N'  '
		) 
		+ REPLICATE(N' ', CASE WHEN s.treeLevel > (dbObjectTree.treeLevel + 1) THEN (s.treeLevel - dbObjectTree.treeLevel - 1) * 3 ELSE 0 END) + N'|--' 
		AS nvarchar(428)
	)
	FROM dbObjectGenRowNum s
	INNER JOIN dbObjectTree
		ON s.rowNum = dbObjectTree.rowNum - 1
)
SELECT /* ObjectType_desc */ ObjectType, CAST(graphic + ObjectName AS nvarchar(256)) [Top Object Referenced BY These...]
FROM dbObjectTree
ORDER BY rowNum;
GO