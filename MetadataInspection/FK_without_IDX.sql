/****************************************************
  This T-SQL template by Troy Ketsdever, http://www.42and5.com,	troyk@42and5.com
	is licensed under the Creative Commons Attribution-ShareAlike 3.0 Unported 
	License. To view a copy of this license, 
	visit http://creativecommons.org/licenses/by-sa/3.0/.

  User may modify and/or distribute this work for commercial or
	non-commercial use, provided that the original attribution above 
	is included, and that the work is made publicly available under 
	these same terms.
****************************************************/

/***************************************************
  Find any defined FKs that don't have a supporting
    index on the child table (relational division version)
***************************************************/
WITH FK
AS
(
    SELECT tc1.TABLE_SCHEMA, tc1.TABLE_NAME, rc.CONSTRAINT_NAME, kcu1.COLUMN_NAME, kcu1.ORDINAL_POSITION
    FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
    INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc1
	    ON rc.CONSTRAINT_SCHEMA = tc1.CONSTRAINT_SCHEMA
       AND rc.CONSTRAINT_NAME = tc1.CONSTRAINT_NAME
    INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu1
	    ON tc1.CONSTRAINT_SCHEMA = kcu1.CONSTRAINT_SCHEMA
       AND tc1.CONSTRAINT_NAME = kcu1.CONSTRAINT_NAME
),
IDX
AS
(
    SELECT SCHEMA_NAME(ao.schema_id) [TABLE_SCHEMA], ao.name [TABLE_NAME], ic.index_id, c.name [COLUMN_NAME], ic.key_ordinal [ORDINAL_POSITION]
    FROM sys.index_columns ic
    INNER JOIN sys.columns c
        ON ic.object_id = c.object_id
       AND ic.column_id = c.column_id
    INNER JOIN sys.all_objects ao
        ON c.object_id = ao.object_id
    WHERE ic.key_ordinal > 0 -- Exclude "included" columns
      AND ao.is_ms_shipped = 0 -- Exclude "Microsoft" tables
)
SELECT fk_out.TABLE_SCHEMA + N'.' + fk_out.TABLE_NAME [Table], fk_out.CONSTRAINT_NAME
    , N'{' + 
        STUFF(
        (   SELECT N', ' + COLUMN_NAME
            FROM FK
            WHERE TABLE_SCHEMA = fk_out.TABLE_SCHEMA
              AND TABLE_NAME = fk_out.TABLE_NAME
              AND CONSTRAINT_NAME = fk_out.CONSTRAINT_NAME
            ORDER BY ORDINAL_POSITION
            FOR XML PATH('') -- Use the XML PATH trick to create a comma-separated list
        ), 1, 2, ''
      ) +
      N'}' [FK Columns]
    , CASE WHEN EXISTS (
        -- http://weblogs.sqlteam.com/peterl/archive/2010/07/02/Proper-Relational-Division-With-Sets.aspx, Peter Larsson 
        SELECT 1
        FROM 
        (
            SELECT TABLE_SCHEMA, TABLE_NAME, index_id, COUNT(*) AS Nbr
            FROM IDX
            -- Predicate to correlate to outer query
            WHERE TABLE_SCHEMA = fk_out.TABLE_SCHEMA
              AND TABLE_NAME = fk_out.TABLE_NAME
            GROUP BY TABLE_SCHEMA, TABLE_NAME, index_id
        ) AS NbrIDXColumns
        INNER JOIN
        (
            SELECT COUNT(*) AS Nbr
            FROM FK
            -- Predicate to correlate to outer query
            WHERE FK.TABLE_SCHEMA = fk_out.TABLE_SCHEMA
              AND FK.TABLE_NAME = fk_out.TABLE_NAME
              AND FK.CONSTRAINT_NAME = fk_out.CONSTRAINT_NAME
        ) AS NbrFKColumns
            -- Set of columns in index are superset of columns in FK reference
           ON NbrIDXColumns.Nbr >= NbrFKColumns.Nbr
        INNER JOIN IDX
            ON NbrIDXColumns.TABLE_SCHEMA = IDX.TABLE_SCHEMA
           AND NbrIDXColumns.TABLE_NAME = IDX.TABLE_NAME
           AND NbrIDXColumns.index_id = IDX.index_id
        INNER JOIN FK
            ON IDX.TABLE_SCHEMA = FK.TABLE_SCHEMA
           AND IDX.TABLE_NAME = FK.TABLE_NAME
           AND IDX.COLUMN_NAME = FK.COLUMN_NAME
           AND IDX.ORDINAL_POSITION = FK.ORDINAL_POSITION
        GROUP BY IDX.TABLE_SCHEMA, IDX.TABLE_NAME, IDX.index_id
        HAVING COUNT(*) = MIN(NbrFKColumns.nbr)
    ) THEN '  yes' ELSE '- MISSING -' END [Helping Index]
FROM FK AS fk_out
GROUP BY fk_out.TABLE_SCHEMA, fk_out.TABLE_NAME, fk_out.CONSTRAINT_NAME
;
GO
