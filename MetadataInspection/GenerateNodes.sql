/* Generate Gephi graph nodes (DB Tables) */
/* Emits XML fragment representing all of the db object_id's for the base tables */
SELECT 1    as Tag,
       NULL as Parent,
       ao.object_id AS [node!1!id],
       CAST(ao.object_id AS varchar(14)) AS [node!1!label],
       NULL		AS	[attvalues!2!empty],
       NULL       as [attvalue!3!for],
       NULL       as [attvalue!3!value]
FROM   sys.all_objects ao
WHERE  ao.type = 'U'

UNION ALL
SELECT	2 AS Tag,
		1 AS Parent,
		ao.object_id AS [node!1!id],
		CAST(ao.object_id AS varchar(14)) AS [node!1!label],
		NULL,
		NULL,
		NULL
FROM   sys.all_objects ao
WHERE  ao.type = 'U'
		
UNION ALL
 
SELECT 3 as Tag,
       2 as Parent,
       ao.object_id,
       CAST(ao.object_id AS varchar(14)) AS [node!1!label],
       NULL,
       0, 
       dX.Log10Rows
  
FROM 
(
	SELECT ps.object_id,
	LOG10(SUM (CASE WHEN (index_id < 2) THEN row_count ELSE 0 END) + 1) [Log10Rows]
	FROM sys.dm_db_partition_stats ps
	INNER JOIN sys.all_objects ao
		ON ps.object_id = ao.object_id
	WHERE ao.type = 'U'
	GROUP BY ps.object_id
) dX
INNER JOIN  sys.all_objects ao
	ON dX.object_id = ao.object_id

ORDER BY 3, 2, 4, 5, 6
FOR XML EXPLICIT
;
