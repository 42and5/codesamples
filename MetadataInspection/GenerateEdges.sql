/* Generate Gephi graph edges explicit FKs. and code-based affinity)  */
/*
	Partition notes:
	1 = FK is declared but not leveraged by code
	2 = FK is declared and used by code
	3 = FK is NOT declared, but affinity exists
*/
DECLARE @Edge TABLE (source int, target int, Weight decimal(6,3), Partition int);

/* Obtain FK references */
INSERT INTO @Edge(source, target, Weight, Partition)
SELECT OBJECT_ID(tcFrom.TABLE_SCHEMA + N'.' + tcFrom.TABLE_NAME) source
, OBJECT_ID(tcTo.TABLE_SCHEMA + N'.' + tcTo.TABLE_NAME) target
, 1 [Weight]
, 1 [Partition] -- 1 = declared rel + 
FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS edge
INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tcFrom
	ON edge.CONSTRAINT_SCHEMA = tcFrom.CONSTRAINT_SCHEMA
   AND edge.CONSTRAINT_NAME = tcFrom.CONSTRAINT_NAME
   AND tcFrom.CONSTRAINT_TYPE = 'FOREIGN KEY'
INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tcTo
	ON edge.UNIQUE_CONSTRAINT_SCHEMA = tcTo.CONSTRAINT_SCHEMA
   AND edge.UNIQUE_CONSTRAINT_NAME = tcTo.CONSTRAINT_NAME
   AND tcTo.CONSTRAINT_TYPE IN ('PRIMARY KEY', 'UNIQUE')
GROUP BY 
	  OBJECT_ID(tcFrom.TABLE_SCHEMA + N'.' + tcFrom.TABLE_NAME)
	, OBJECT_ID(tcTo.TABLE_SCHEMA + N'.' + tcTo.TABLE_NAME)
;

DECLARE @ReferencePairs TABLE (Referenced_One int NOT NULL, Referenced_Two int NOT NULL, referencing_id int NOT NULL);

/* Update affinity pairs as implied by participation in db programming objects */
WITH ObjectReferences
AS
(
	SELECT re.referencing_id, OBJECT_ID(TABLE_SCHEMA + N'.' + TABLE_NAME) [referenced_id]
	FROM INFORMATION_SCHEMA.TABLES t
	CROSS APPLY sys.dm_sql_referencing_entities(t.TABLE_SCHEMA + N'.' + t.TABLE_NAME, N'OBJECT') re
	WHERE t.TABLE_TYPE = 'BASE TABLE'
	-- TODO: Include or exclude specific tables via addition of WHERE clause here
)
INSERT INTO @ReferencePairs(Referenced_One, Referenced_Two, referencing_id)
SELECT r1.referenced_id [Referenced_One], r2.referenced_id [Referenced_Two], r1.referencing_id
FROM ObjectReferences r1
INNER JOIN ObjectReferences r2
	ON r1.Referenced_ID > r2.Referenced_ID
   AND r1.referencing_id = r2.referencing_id;

DECLARE @BaseTableEdge TABLE(TableFrom int, TableTo int, Affinity int);
INSERT INTO @BaseTableEdge(TableFrom, TableTo, Affinity)
SELECT rp.Referenced_One
	, rp.Referenced_Two
	, COUNT(*) [Affinity]
FROM @ReferencePairs rp
GROUP BY rp.Referenced_One, rp.Referenced_Two
;

MERGE INTO @Edge AS target
USING @BaseTableEdge AS source
	ON	(target.target = source.TableFrom AND target.source = source.TableTo)
		OR
		(target.target = source.TableTo AND target.source = source.TableFrom)
WHEN MATCHED THEN
	UPDATE
		SET Weight += source.Affinity,
			Partition = 2						/* FK relationship that is exploited */
WHEN NOT MATCHED THEN
	INSERT (source, target, Weight, Partition)
	VALUES (source.TableFrom, source.TableTo, source.Affinity, 3 /* Non-FK rel'ship */)
;

/* Emit XML fragment */
SELECT 1    as Tag,
       NULL as Parent,
       e.source AS [edge!1!source],
       e.target AS [edge!1!target],
       e.Weight AS [edge!1!Weight],
       NULL		AS	[attvalues!2!empty],
       NULL       as [attvalue!3!for],
       NULL       as [attvalue!3!value]
FROM   @Edge e

UNION ALL
SELECT	2 AS Tag,
		1 AS Parent,
		e.source AS [edge!1!source],
		e.target AS [edge!1!target],
		NULL AS [edge!1!Weight],
		NULL,
		NULL,
		NULL
FROM   @Edge e
		
UNION ALL
 
SELECT 3 as Tag,
       2 as Parent,
       e.source AS [edge!1!source],
       e.target AS [edge!1!target],
       NULL AS [edge!1!Weight],
       NULL,
       0, 
       e.Partition
FROM @Edge e

ORDER BY 3,4,2, 5, 6, 7
FOR XML EXPLICIT
;


