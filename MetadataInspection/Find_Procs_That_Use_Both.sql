-- Procs-that-use
DECLARE @Object1 sysname, @Object2 sysname
SET @Object1 = N'<Object_Name1, sysname, Object_Name>';
SET @Object2 = N'<Object_Name2, sysname, Object_Name>';

SELECT dX.[object_id], dX.[name], dX.type, dX.type_desc
FROM 
(
SELECT  obj.[object_id], obj.[name], obj.type, obj.type_desc
FROM dbo.sql_dependencies_defsearch dep
INNER JOIN sys.all_objects obj
	ON dep.[object_id] = obj.[object_id]
INNER JOIN sys.all_objects objRef
	ON dep.referenced_major_id = objRef.[object_id]
-- Stored Procs, Scalar, Inline Table-valued, and Table-valued Functions, CLR Stored Proc
WHERE obj.type IN ('P', 'FN', 'IF', 'TF', 'PC')
  AND objRef.[name] = @Object1
) dX
INNER JOIN 
(
SELECT  obj.[object_id], obj.[name], obj.type, obj.type_desc
FROM dbo.sql_dependencies_defsearch dep
INNER JOIN sys.all_objects obj
	ON dep.[object_id] = obj.[object_id]
INNER JOIN sys.all_objects objRef
	ON dep.referenced_major_id = objRef.[object_id]
-- Stored Procs, Scalar, Inline Table-valued, and Table-valued Functions, CLR Stored Proc
WHERE obj.type IN ('P', 'FN', 'IF', 'TF', 'PC')
  AND objRef.[name] = @Object2
) dY
ON dX.[object_id] = dY.[object_id]
